#include <stdio.h>
#include <stdlib.h>

/* This is defined in util.h as well. But this little UTF8 library
   should not depend on that file, so I define it here as well. */
#ifndef MYALLOC
#define MYALLOC(TYPE, LEN) (TYPE*)malloc((LEN) * sizeof(TYPE))
#endif

/* This code is from Rosetta codes: 
   https://rosettacode.org/wiki/UTF-8_encode_and_decode */
 
typedef struct {
  char mask;       /* char data will be bitwise AND with this */
  char lead;       /* start bytes of current char in utf-8 encoded character */
  unsigned int beg;    /* beginning of codepoint range */
  unsigned int end;    /* end of codepoint range */
  int bits_stored; /* the number of bits from the codepoint that fits in char */
}utf_t;
 
utf_t * utf[] = {
  /*             mask        lead        beg      end       bits */
  [0] = &(utf_t){0b00111111, 0b10000000, 0,       0,        6    },
  [1] = &(utf_t){0b01111111, 0b00000000, 0000,    0177,     7    },
  [2] = &(utf_t){0b00011111, 0b11000000, 0200,    03777,    5    },
  [3] = &(utf_t){0b00001111, 0b11100000, 04000,   0177777,  4    },
  [4] = &(utf_t){0b00000111, 0b11110000, 0200000, 04177777, 3    },
  &(utf_t){0},
};

__attribute__((__const__))
int codepoint_len(const unsigned int cp)
{
  int len = 0;
  for(utf_t **u = utf; *u; ++u) {
    if((cp >= (*u)->beg) && (cp <= (*u)->end)) {
      break;
    }
    ++len;
  }
  /* if(len > 4) /\* Out of bounds *\/
   *   return len; */
 
  return len;
}
 
__attribute__((__const__))
int utf8_len(const char ch)
{
  int len = 0;
  for(utf_t **u = utf; *u; ++u) {
    if((ch & ~(*u)->mask) == (*u)->lead) {
      break;
    }
    ++len;
  }
  /* if(len > 4) { /\* Malformed leading byte *\/
   *   return len;
   * } */
  return len;
}

__attribute__((__pure__))
int utf8_len_back(const char * s)
{
  int len = 1;

  for (;*(s-len+1) && (*(s-len+1) & 0xC0) == 0x80;) {
    len++;
  }

  return len;
}
 
void to_utf8(const unsigned int cp, char *ret)
{
  const int bytes = codepoint_len(cp);
  
  if (bytes>4) return;
 
  int shift = utf[0]->bits_stored * (bytes - 1);
  ret[0] = (cp >> shift & utf[bytes]->mask) | utf[bytes]->lead;
  shift -= utf[0]->bits_stored;
  for(int i = 1; i < bytes; ++i) {
    ret[i] = (cp >> shift & utf[0]->mask) | utf[0]->lead;
    shift -= utf[0]->bits_stored;
  }
  ret[bytes] = 0;
}

__attribute__((__pure__))
unsigned int to_cp(const char *chr, const int bytes)
{
  if (bytes>4) return 0;
  
  /* int bytes = utf8_len(*chr); */
  int shift = utf[0]->bits_stored * (bytes - 1);
  unsigned int codep = (*chr++ & utf[bytes]->mask) << shift;
 
  for(int i = 1; i < bytes; ++i, ++chr) {
    shift -= utf[0]->bits_stored;
    codep |= ((char)*chr & utf[0]->mask) << shift;
  }
 
  return codep;
}
 
#ifdef TEST_UTF8
int main(int argc, char **argv)
{
  const unsigned int *in, input[] = {0x0041, 0x00f6, 0x0416, 0x20ac, 0x1d11e, 0x0133, 0x0};
 
  printf("Character  Unicode  UTF-8 encoding (hex)\n");
  printf("----------------------------------------\n");
 
  char utf8[5];
  unsigned int codepoint;
  int inc = 0;
  
  for(in = input; *in; ++in) {
    to_utf8(*in, utf8);
    inc = utf8_len(*utf8);
    codepoint = to_cp(utf8, inc);
    printf("%s          U+%-7.4x", utf8, codepoint);
 
    for(int i = 0; utf8[i] && i < 4; ++i) {
      printf("%hhx ", utf8[i]);
    }
    printf("\n");
  }
  
  char *test = (char*)malloc (sizeof(char)*39);
  char *pre_test = "試試看這麼厲害！";

  *(test+0) = 0x41;
  *(test+1) = 0xc3;
  *(test+2) = 0xb6;
  *(test+3) = 0xd0;
  *(test+4) = 0x96;
  *(test+5) = 0xe2;
  *(test+6) = 0x82;
  *(test+7) = 0xac;
  *(test+8) = 0xf0;
  *(test+9) = 0x9d;
  *(test+10) = 0x84;
  *(test+11) = 0x9e;
  *(test+12) = 0xc4;
  *(test+13) = 0xb3;

  for (int i = 0; i < 24; i++)
    *(test+14+i) = *(pre_test+i);
  
  int increment = 0;

  for (;*test;test += increment) {
    increment = utf8_len(*test);

    for (int i = 0; i < increment; i++)
      *(utf8+i) = *(test+i);

    *(utf8+increment) = 0;
    
    codepoint = to_cp(utf8, increment);
    printf("%s          U+%-7.4x", utf8, codepoint);
    for(int i = 0; utf8[i] && i < 4; ++i)
      printf("%hhx ", utf8[i]);
    
    printf("\n");
  }

  test--;

  for (int i = 0;*(test+i);i-=increment) {
    increment = utf8_len_back(test+i);
    to_utf8(to_cp(test+i+1-increment, increment), utf8);
    printf("len back = %d, test = %s\n", increment, utf8);
  }
  
  return 0;
}
#endif

