#ifndef UTIL_H
#define UTIL_H

#define READ_AMOUNT 1024*50

/* This is commonly used, so put here for easy access. */
#define MYALLOC(TYPE, LEN) (TYPE*)malloc((LEN) * sizeof(TYPE))

/* Push a node to the singly linked list. */
#define PUSH_NODE(NUM_VAR, POINTER, CUR, VALUE, TYPE) {                 \
    (NUM_VAR)++;                                                        \
    if (NUM_VAR > 1) {                                                  \
      (POINTER) = MYALLOC(Node_t, 1);                                   \
      *(POINTER) = (CUR);                                               \
      (CUR) = (Node_t) { (Node_v) { .TYPE=(VALUE) }, (POINTER) };       \
    } else {                                                            \
      (CUR) = (Node_t) { (Node_v) { .TYPE=(VALUE) }, NULL };            \
    }                                                                   \
  }

/* Copy every node in the singly linked list to an array ASSIGN_TO. */
#define COPY_NODE(NUM_VAR, CUR, ASSIGN_TO, NEXT, TYPE) {        \
    if (NUM_VAR) {                                              \
      if (NUM_VAR > 1) {                                        \
        for (int i = 0; i < NUM_VAR; i++) {                     \
          *(ASSIGN_TO+((NUM_VAR)-i-1)) = (CUR).value.TYPE;      \
          if ((CUR).next) {                                     \
            NEXT = *((CUR).next);                               \
            free((CUR).next);                                   \
            (CUR) = NEXT;                                       \
          }                                                     \
        }                                                       \
      } else {                                                  \
        *(ASSIGN_TO) = (CUR).value.TYPE;                        \
      }                                                         \
    }                                                           \
  }

/* Pop one node out of the singly linked list. */
#define POP_NODE(NUM_VAR, X, CUR, NEXT, TYPE) { \
    (NUM_VAR)--;                                \
    X = CUR.value.TYPE;                         \
    if (CUR.next) {                             \
      NEXT = *(CUR.next);                       \
      free(CUR.next);                           \
      CUR = NEXT;                               \
    }                                           \
  }

/* An empty node. */
#define EMPTY_NODE (Node_t) { (Node_v) { .i=0 }, NULL }

/* A simple singly linked list. 
   
   A node itself is incomplete, without the number of elements, and a
   temporary pointer. */

/* We need to forward declare regions and queries to use a pointer to
   that type.

   REVIEW: We may use a void pointer instead. But I think there is no
   need for that extra burden to convert between types as of now. Of
   course this needs to be reviewed. */

struct Region_s;

struct RSet_s;

/* struct Query_u; */

struct Query_s;

typedef union {
  int i;
  char *s;
  struct Region_s *r;
  struct RSet_s *rs;
  /* struct Query_u *qu; */
  struct Query_s *q;
} Node_v;

typedef struct Node {
  Node_v value;
  struct Node *next;
} Node_t;

/* Some simple macros. */

#define ABS(X) (((X)<0) ? -(X) : (X))

#define MAX(X, Y) ((X)>(Y)) ? (X) : (Y)

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

/* This is easier to do than using a polymorphic stream, and is
   probably faster and more idiomatic. */
long int read_entire_file (const char *file_name, char **str);

#endif
