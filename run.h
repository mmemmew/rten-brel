#ifndef RUN_H
#define RUN_H

#include "machine.h"
#include "utf8.h"

int run_prog (Prog p, ProgInfo info, char *s, int *matches);

/* int run_prog_s (Prog p, ProgInfo info, stream *s, int *matches); */

#endif
