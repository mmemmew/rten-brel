#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "region.h"

#define MARK_TO_STR(M)                          \
  ((M) == NONE) ? "NONE" :                      \
  (((M) == ACCEPTED) ? "ACCEPTED" :             \
   ((M) == SUFFIX ? "SUFFIX" :                  \
    "ACCEPTED_SUFFIX"))

#define PRINT_REGION(X) {                       \
    printf("Region beg = %ld, ", (X).beg);      \
    printf("end = %ld\n", (X).end);             \
  }

#define CONTAINED_IN(X, Y) ((X).beg >= (Y).beg && (X).end <= (Y).end)

#define REGION_LESS_START(X, Y)                         \
  ((X)->beg < (Y)->beg ||                               \
   ((X)->beg == (Y)->beg && (X)->end <= (Y)->end))

#define REGION_LESS_END(X, Y)                           \
  ((X)->end < (Y)->end ||                               \
   ((X)->end == (Y)->end && (X)->beg <= (Y)->beg))

#define DESTROY_REGIONS(X, Y) {                 \
    destroy_RSet(X);                            \
    destroy_RSet(Y);                            \
  }

typedef enum {
  NONE,
  SUFFIX,
  ACCEPTED,
  ACCEPTED_SUFFIX
} MARKS;

struct Region_s {
  long beg;                     /* Beginning of the region */
  long end;                     /* End of the region */
};

long *RSet_to_array(RSet rs)
{
  long *result = MYALLOC(long, (rs.size)*2);
  for (int i = 0; i < rs.size; i++) {
    *(result+2*i) = (*(rs.rs+i)).beg;
    *(result+2*i+1) = (*(rs.rs+i)).end;
  }

  return result;
}

/* Return a permutation corresponding to the end position order of
   REGIONS.

   If STARTP is not zero, then this sorts based on the start position
   order; otherwise this sorts based on the end position order.*/

/* REVIEW: Accept one more argument which is a pointer to long, so
   that we can reuse existing pointers if needed. */
__attribute__((__hot__))
static long *merge_sort_RSet(Region *regions, long size, unsigned char startp)
{
  long *current = MYALLOC(long, size);
  long *working = MYALLOC(long, size);
  long *temp = NULL;

  /* start with the identity permutation */
  for (int i = 0; i < size; i++) {
    *(current+i) = i;
    *(working+i) = 0;
  }

  long end = 0, times = 0, right = 0, ii = 0, jj = 0;

  if (!startp) {

    for (long width = 1; width < size; width <<= 1, times++) {
      for (long i = 0; i < size; i += 2*width) {
        end = MIN(i+2*width, size);
        right = MIN(i+width, size);
        ii = i;
        jj = right;
        for (long k = i; k < end; k++) {
          if (ii < right &&
              (jj >= end ||
               (regions+(*(current+ii)))->end <
               (regions+(*(current+jj)))->end ||
               ((regions+(*(current+ii)))->end ==
                (regions+(*(current+jj)))->end &&
                (regions+(*(current+ii)))->beg <=
                (regions+(*(current+jj)))->beg)))
            *(working+k) = *(current+ii++);
          else
            *(working+k) = *(current+jj++);
        }
      }

      temp = current;
      current = working;
      working = temp;
    }
  } else {

    for (long width = 1; width < size; width <<= 1, times++) {
      for (long i = 0; i < size; i += 2*width) {
        end = MIN(i+2*width, size);
        right = MIN(i+width, size);
        ii = i;
        jj = right;
        for (long k = i; k < end; k++) {
          if (ii < right &&
              (jj >= end ||
               (regions+(*(current+ii)))->beg <
               (regions+(*(current+jj)))->beg ||
               ((regions+(*(current+ii)))->beg ==
                (regions+(*(current+jj)))->beg &&
                (regions+(*(current+ii)))->end <=
                (regions+(*(current+jj)))->end)))
            *(working+k) = *(current+ii++);
          else
            *(working+k) = *(current+jj++);
        }
      }

      temp = current;
      current = working;
      working = temp;
    }
  }

  free(working);

  return current;
}

__attribute__((__hot__))
static void merge_three_regions(Region *a, Region *b, Region *c,
                         long alen, long blen, long clen,
                         Region *d,
                         long *ca, long *cb, long *cc)
{
  long i = 0, j = 0, k = 0, ell = 0;

  for (;i<alen && j < blen && k < clen;) {
    if (REGION_LESS_START(a+i, b+j) &&
        REGION_LESS_START(a+i, c+k)) {
      *(ca+i) = ell;
      *(d+ell++) = *(a+i++);
    }

    else if (REGION_LESS_START(b+j, a+i) &&
             REGION_LESS_START(b+j, c+k)) {
      *(cb+j) = ell;
      *(d+ell++) = *(b+j++);
    }

    else {
      *(cc+k) = ell;
      *(d+ell++) = *(c+k++);
    }
  }

  for (;i<alen && j<blen;) {
    if (REGION_LESS_START(a+i, b+j)) {
      *(ca+i) = ell;
      *(d+ell++) = *(a+i++);
    } else {
      *(cb+j) = ell;
      *(d+ell++) = *(b+j++);
    }
  }

  for (;i<alen && k<clen;) {
    if (REGION_LESS_START(a+i, c+k)) {
      *(ca+i) = ell;
      *(d+ell++) = *(a+i++);
    } else {
      *(cc+k) = ell;
      *(d+ell++) = *(c+k++);
    }
  }

  for (;j<blen && k<clen;) {
    if (REGION_LESS_START(b+j, c+k)) {
      *(cb+j) = ell;
      *(d+ell++) = *(b+j++);
    } else {
      *(cc+k) = ell;
      *(d+ell++) = *(c+k++);
    }
  }

  for (;i<alen;) {
    *(ca+i) = ell;
    *(d+ell++) = *(a+i++);
  }

  for (;j<blen;) {
    *(cb+j) = ell;
    *(d+ell++) = *(b+j++);
  }

  for (;k<clen;) {
    *(cc+k) = ell;
    *(d+ell++) = *(c+k++);
  }
}

/* Convert an ARRAY of size SIZE to a region set.

   If ARRAY is already in start position order, give ORDER the value
   1, so that this won't sort the start positions again.

   Similarly, if ARRRAY is already in end position order, give ORDER
   the value 2, so that this won't sort the end positions again.

   If ARRAY is both in start position order and in end position order,
   then give ORDER the value 3, and this won't sort the arrays at
   all.

   Finally, if the array is not in any order, give ORDER the value
   0. */
RSet array_to_RSet(long *array, long size, unsigned char order)
{
  /* size must be even */
  size /= 2;
  RSet result = (RSet) { NULL, size, NULL, NULL };
  result.rs = MYALLOC(Region, size);
  /* I don't have to allocate memory for this array. */
  long *temp_order = NULL;

  if (!(order & 1)) {
    Region *temp = MYALLOC(Region, size);

    for (int i = 0; i < size; i++) {
      *(temp+i) = (Region) { *(array+2*i), *(array+2*i+1) };
    }

    temp_order = merge_sort_RSet(temp, size, 1);

    for (int i = 0; i < size; i++) {
      *(result.rs+i) = *(temp+*(temp_order+i));
    }

    free(temp);
    free(temp_order);
  } else {
    for (int i = 0; i < size; i++)
      *(result.rs+i) = (Region) { *(array+2*i), *(array+2*i+1) };

  }

  if (!(order & 2)) {
    result.stoe = merge_sort_RSet(result.rs, size, 0);

    result.etos = MYALLOC(long, size);

    for (int i = 0; i < size; i++)
      *(result.etos+*(result.stoe+i)) = i;
  } else {
    result.stoe = MYALLOC(long, size);
    result.etos = MYALLOC(long, size);

    for (int i = 0; i < size; i++) {
      *(result.stoe+i) = i;
      *(result.etos+i) = i;
    }
  }

  return result;
}

__attribute__((__unused__))
void print_RSet(RSet rs)
{
  printf("Start order:\n");
  for (int i = 0; i < rs.size; i++) {
    PRINT_REGION(*(rs.rs+i));
  }
  printf("\n");

  if (rs.stoe == NULL) {
    printf("End order = Start order\n\n");
  } else {
    printf("End order:\n");
    for (int i = 0; i < rs.size; i++) {
      PRINT_REGION(*(rs.rs+*(rs.stoe+i)));
    }

    printf("\n");

    printf("stoe: ");
    for (int i = 0; i < rs.size; i++)
      printf("%s%ld", (i) ? ", " : "", *(rs.stoe+i));
    printf("\n");

    printf("etos: ");
    for (int i = 0; i < rs.size; i++)
      printf("%s%ld", (i) ? ", " : "", *(rs.etos+i));
    printf("\n");
  }
}

inline void destroy_RSet(RSet rs)
{
  free(rs.rs);
  if (rs.stoe) free(rs.stoe);
  if (rs.etos) free(rs.etos);
}

RSet RSet_outer(RSet rs)
{
  if (!(rs.size))
    return (RSet) { NULL, 0, NULL, NULL };

  long maximal_end = 0, size = 0;
  Region rg = (Region) { 0, 0 };
  MARKS *marks = MYALLOC(MARKS, rs.size);

  RSet result = (RSet) { NULL, 0, NULL, NULL };

  for (long i = 0; i < rs.size; i++) {
    rg = *(rs.rs+i);

    if ((rs.rs+i)->end > maximal_end) {
      *(marks+i) = ACCEPTED;
      if (i && ((rs.rs+i)->beg == (rs.rs+i-1)->beg)) {
        if (*(marks+i-1) == ACCEPTED) {
          *(marks+i-1) = NONE;
        } else {
          size++;
        }
      } else {
        size++;
      }
    } else {
      *(marks+i) = NONE;
    }

    maximal_end = MAX(maximal_end, rg.end);
  }

  result.size = size;
  result.rs = MYALLOC(Region, size);

  /* The outer regions are always flat, so the start position and the
     end position orders coincide. */
  result.stoe = NULL;
  result.etos = NULL;

  for (long i = 0, j = 0; i < rs.size; i++)
    if (*(marks+i) == ACCEPTED)
      *(result.rs+j++) = *(rs.rs+i);

  free(marks);

  return result;
}

RSet RSet_inner(RSet rs)
{
  RSet result_reg = (RSet) { NULL, 0, NULL, NULL };
  if (!(rs.size)) {
    return result_reg;
  } else if (rs.size == 1) {
    result_reg.rs = MYALLOC(Region, 1);
    *(result_reg.rs) = *(rs.rs);
    result_reg.size = 1;
    /* Start order = End order */
    return result_reg;
  }

  long *pending = MYALLOC(long, rs.size);
  long *result = MYALLOC(long, rs.size);
  long current = 0, pending_length = 0;
  long result_length = 0;

  /* If a region is contained in the next region, then we delete that
     next region. If the region is not contained in the next region,
     then we collect that region into the array PENDING, and filter
     regions by the next region. So the arrays in PENDING satisfy the
     property that no region is contained in the next region in the
     array. */
  for (long i = 1; i < rs.size;) {
    if (CONTAINED_IN(*(rs.rs+current),
                     *(rs.rs+i))) {
      i++;
    } else {
      *(pending+pending_length++) = current;
      current = i++;
    }
  }

  /* Now we delete those regions which contain the next region. */
  *(result+result_length++) = current;

  for (;pending_length;pending_length--) {
    if (CONTAINED_IN(*(rs.rs+*(result+result_length-1)),
                     *(rs.rs+*(pending+pending_length-1)))) {
    } else {
      *(result+result_length++) = *(pending+pending_length-1);
    }
  }

  free(pending);

  /* Now result is in the reversed order, so we reverse the order. */

  result_reg.rs = MYALLOC(Region, result_length);

  for (long i = 0; i < result_length; i++)
    *(result_reg.rs+i) = *(rs.rs+*(result+result_length-1-i));

  free(result);

  result_reg.size = result_length;

  /* Start order = End order */
  return result_reg;
}


RSet RSet_in(RSet a, RSet b)
{
  RSet result = (RSet) { NULL, 0, NULL, NULL };

  RSet bprime = RSet_outer(b);

  MARKS *marks = MYALLOC(MARKS, a.size);

  long *correspondence = MYALLOC(long, a.size);

  long size = 0;

  for (long i = 0; i < a.size;)
    *(marks+i++) = NONE;

  for (long i = 0, j = 0;
       i < a.size && j < bprime.size;) {
    if (CONTAINED_IN(*(a.rs+i), *(bprime.rs+j))) {
      *(correspondence+i) = size++;
      *(marks+i++) = ACCEPTED;
    } else if ((a.rs+i)->beg <=
               (bprime.rs+j)->beg) {
      i++;
    } else if (j < bprime.size-1) {
      if ((bprime.rs+j+1)->beg <=
          (a.rs+i)->beg) j++;
      else i++;
    } else {
      if ((bprime.rs+j)->end > (a.rs+i)->beg) i++;
      else j++;
    }
  }

  result.rs = MYALLOC(Region, size);
  result.size = size;
  result.stoe = MYALLOC(long, size);
  result.etos = MYALLOC(long, size);

  for (long i = 0, j = 0, k = 0; i < a.size; i++) {
    if (*(marks+i) == ACCEPTED)
      *(result.rs+j++) = *(a.rs+i);

    if (*(marks+*(a.stoe+i)) == ACCEPTED) {
      *(result.stoe+k) = *(correspondence+*(a.stoe+i));
      *(result.etos+*(correspondence+*(a.stoe+i))) = k;
      k++;
    }
  }

  free(marks);
  free(correspondence);
  destroy_RSet(bprime);

  return result;
}

RSet RSet_ni(RSet a, RSet b)
{
  RSet c = RSet_inner(b);

  MARKS *marks = MYALLOC(MARKS, a.size);

  long *correspondence = MYALLOC(long, a.size);

  long size = 0;

  for (long i = 0; i < a.size;) *(marks+i++) = NONE;

  for (long i = 0, j = 0;
       i<a.size && j<c.size;) {
    if (CONTAINED_IN(*(c.rs+j), *(a.rs+i))) {
      *(correspondence+i) = size++;
      *(marks+i++) = ACCEPTED;
    } else if ((a.rs+i)->end <=
               (c.rs+j)->end) {
      i++;
    } else {
      j++;
    }
  }

  RSet result = (RSet) { NULL, size, NULL, NULL };

  result.rs = MYALLOC(Region, size);
  result.stoe = MYALLOC(long, size);
  result.etos = MYALLOC(long, size);

  for (long i = 0, j = 0, k = 0; i < a.size; i++) {
    if (*(marks+i) == ACCEPTED)
      *(result.rs+j++) = *(a.rs+i);

    if (*(marks+*(a.stoe+i)) == ACCEPTED) {
      *(result.stoe+k) = *(correspondence+*(a.stoe+i));
      *(result.etos+*(correspondence+*(a.stoe+i))) = k;
      k++;
    }
  }

  free(marks);
  free(correspondence);
  destroy_RSet(c);

  return result;
}

RSet RSet_notin(RSet a, RSet b)
{
  RSet result = (RSet) { NULL, 0, NULL, NULL };

  RSet bprime = RSet_outer(b);

  MARKS *marks = MYALLOC(MARKS, a.size);

  long *correspondence = MYALLOC(long, a.size);

  long size = 0;

  for (long i = 0; i < a.size;)
    *(marks+i++) = ACCEPTED;

  for (long i = 0, j = 0;
       i < a.size && j < bprime.size;) {
    if (CONTAINED_IN(*(a.rs+i), *(bprime.rs+j))) {
      *(marks+i++) = NONE;
    } else if ((a.rs+i)->beg <=
               (bprime.rs+j)->beg) {
      *(correspondence+i) = size++;
      i++;
    } else if (j < bprime.size-1) {
      if ((bprime.rs+j+1)->beg <=
          (a.rs+i)->beg) {
        j++;
      } else {
        *(correspondence+i) = size++;
        i++;
      }
    } else {
      if ((bprime.rs+j)->end > (a.rs+i)->beg) {
        *(correspondence+i) = size++;
        i++;
      } else {
        j++;
        for (;i<a.size;)
          *(correspondence+i++) = size++;
      }
    }
  }

  result.rs = MYALLOC(Region, size);
  result.size = size;
  result.stoe = MYALLOC(long, size);
  result.etos = MYALLOC(long, size);

  for (long i = 0, j = 0, k = 0; i < a.size; i++) {
    if (*(marks+i) == ACCEPTED)
      *(result.rs+j++) = *(a.rs+i);

    if (*(marks+*(a.stoe+i)) == ACCEPTED) {
      *(result.stoe+k) = *(correspondence+*(a.stoe+i));
      *(result.etos+*(correspondence+*(a.stoe+i))) = k;
      k++;
    }
  }

  free(marks);
  free(correspondence);
  destroy_RSet(bprime);

  return result;
}

RSet RSet_notni(RSet a, RSet b)
{
  RSet c = RSet_inner(b);

  MARKS *marks = MYALLOC(MARKS, a.size);

  long *correspondence = MYALLOC(long, a.size);

  long size = 0;

  for (long i = 0; i < a.size;) *(marks+i++) = ACCEPTED;

  for (long i = 0, j = 0;
       i<a.size && j<c.size;) {
    if (CONTAINED_IN(*(c.rs+j), *(a.rs+i))) {
      *(marks+i++) = NONE;
    } else if ((a.rs+i)->end <=
               (c.rs+j)->end) {
      *(correspondence+i) = size++;
      i++;
    } else {
      j++;
      if (j == c.size) {
        for (;i<a.size;)
          *(correspondence+i++) = size++;
      }
    }
  }

  RSet result = (RSet) { NULL, size, NULL, NULL };

  result.rs = MYALLOC(Region, size);
  result.stoe = MYALLOC(long, size);
  result.etos = MYALLOC(long, size);

  for (long i = 0, j = 0, k = 0; i < a.size; i++) {
    if (*(marks+i) == ACCEPTED)
      *(result.rs+j++) = *(a.rs+i);

    if (*(marks+*(a.stoe+i)) == ACCEPTED) {
      *(result.stoe+k) = *(correspondence+*(a.stoe+i));
      *(result.etos+*(correspondence+*(a.stoe+i))) = k;
      k++;
    }
  }

  free(marks);
  free(correspondence);
  destroy_RSet(c);

  return result;
}

/* flag = 0 => include start and end regions
   flag = 1 => exclude start region
   flag = 2 => exclude end region
   flag = 3 => exclude start and end regions */
static RSet RSet_follow_generic(RSet a, RSet b, unsigned char flag)
{
  long *stack = MYALLOC(long, a.size);

  long stack_length = 0;

  MARKS *marksa = MYALLOC(MARKS, a.size);
  MARKS *marksb = MYALLOC(MARKS, b.size);

  long *correspondence_a = MYALLOC(long, a.size);
  long *correspondence_ba = MYALLOC(long, b.size);

  for (long i = 0; i<a.size || i<b.size; i++) {
    if (i < a.size) *(marksa+i) = NONE;
    if (i < b.size) *(marksb+i) = NONE;
  }

  long r = 0, size = 0;

  for (long i=0,j=0;
       (i<a.size || stack_length) && j<b.size;) {
    for (;i<a.size &&
           (a.rs+*(a.stoe+i))->end <=
           (b.rs+j)->beg;)
      *(stack+stack_length++) = i++;

    if (stack_length) {
      r = *(a.stoe+*(stack+ --stack_length));
      switch (flag) {
      case 0:
        (a.rs+r)->end = (b.rs+j)->end;
        break;
      case 1:
        (a.rs+r)->end = (b.rs+j)->end;
        (a.rs+r)->beg = (a.rs+r)->end+1;
        break;
      case 2:
        (a.rs+r)->end = (b.rs+j)->beg-1;
        break;
      default:
        /* NOTE: A wrong flag is treated as if 3 is passed. */
        (a.rs+r)->beg = (a.rs+r)->end+1;
        (a.rs+r)->end = (b.rs+j)->beg-1;
        break;
      }

      if ((a.rs+r)->beg <= (a.rs+r)->end) {
        *(b.rs+j) = *(a.rs+r);

        *(marksa+r) = ACCEPTED;
        *(marksb+j) = ACCEPTED;

        *(correspondence_ba+j) = r;

        size++;
      }
      
      j++;
    } else if (i<a.size) {
      /* i end > j beg */

      for (;j<b.size && (b.rs+j)->beg < (a.rs+*(a.stoe+i))->end;)
        j++;
    }
  }

  RSet result = (RSet) { NULL, size, NULL, NULL };
  result.rs   = MYALLOC(Region, size);
  result.stoe = MYALLOC(long, size);
  result.etos = MYALLOC(long, size);

  /* For sorting */
  long *pending = MYALLOC(long, size), *working = MYALLOC(long, size);
  long *temp = NULL;
  long pending_length = 0;
  long end = 0, right = 0, ii = 0, jj = 0;

  /* For deleting duplicates */

  for (long i = 0, j = 0; i < a.size; i++) {
    if (*(marksa+i) == ACCEPTED) {
      if (!(pending_length) ||
          (a.rs+i)->beg ==
          (a.rs+*(pending+pending_length-1))->beg) {
        *(pending+pending_length++) = i;
      } else {
        /* Merge sorting */
        for (long width = 1; width<pending_length; width <<= 1) {
          for (long k = 0; k<pending_length; k+=2*width) {
            end = MIN(k+2*width, pending_length);
            right = MIN(k+width, pending_length);
            ii = k;
            jj = right;
            for (long ell = k; ell<end; ell++) {
              if (ii<right &&
                  (jj>=end ||
                   (a.rs+*(pending+ii))->end <=
                   (a.rs+*(pending+jj))->end))
                *(working+ell) = *(pending+ii++);
              else
                *(working+ell) = *(pending+jj++);
            }
          }

          temp = pending;
          pending = working;
          working = temp;
        }

        for (long k = 0, last = -1; k<pending_length;k++) {
          if ((a.rs+*(pending+k))->end != last) {
            last = (a.rs+*(pending+k))->end;
            *(result.rs+j) = *(a.rs+*(pending+k));
            *(correspondence_a+*(pending+k)) = j;
            j++;
          } else {
            *(correspondence_a+*(pending+k)) = j;
            size--;
          }
        }

        pending_length = 0;

        *(pending+pending_length++) = i;
      }
    }

    if (i+1 == a.size) {
      /* Merge sorting */
      for (long width = 1; width<pending_length; width <<= 1) {
        for (long k = 0; k<pending_length; k+=2*width) {
          end = MIN(k+2*width, pending_length);
          right = MIN(k+width, pending_length);
          ii = k;
          jj = right;
          for (long ell = k; ell<end; ell++) {
            if (ii<right &&
                (jj>=end ||
                 (a.rs+*(pending+ii))->end <=
                 (a.rs+*(pending+jj))->end))
              *(working+ell) = *(pending+ii++);
            else
              *(working+ell) = *(pending+jj++);
          }
        }

        temp = pending;
        pending = working;
        working = temp;
      }

      for (long k = 0, last = -1; k<pending_length;k++) {
        if ((a.rs+*(pending+k))->end != last) {
          last = (a.rs+*(pending+k))->end;
          *(result.rs+j) = *(a.rs+*(pending+k));
          *(correspondence_a+*(pending+k)) = j;
          j++;
        } else {
          *(correspondence_a+*(pending+k)) = j;
          size--;
        }
      }
    }
  }

  pending_length = 0;

  for (long i = 0, j = 0; i < b.size; i++) {
    r = *(b.stoe+i);
    if (*(marksb+r) == ACCEPTED) {
      if (!(pending_length) ||
          (b.rs+r)->end ==
          (b.rs+*(pending+pending_length-1))->end) {
        *(pending+pending_length++) = r;
      } else {
        /* Merge sorting */
        for (long width = 1; width<pending_length; width <<= 1) {
          for (long k = 0; k<pending_length; k+=2*width) {
            end = MIN(k+2*width, pending_length);
            right = MIN(k+width, pending_length);
            ii = k;
            jj = right;
            for (long ell = k; ell<end; ell++) {
              if (ii<right &&
                  (jj>=end ||
                   (b.rs+*(pending+ii))->beg <=
                   (b.rs+*(pending+jj))->beg))
                *(working+ell) = *(pending+ii++);
              else
                *(working+ell) = *(pending+jj++);
            }
          }

          temp = pending;
          pending = working;
          working = temp;
        }

        for (long k = 0, temp_long = -1, temp_long_2 = -1;
             k<pending_length;k++) {
          temp_long = *(correspondence_a+*(correspondence_ba+*(pending+k)));

          if (temp_long_2 < 0 ||
              (a.rs+temp_long)->beg != (a.rs+temp_long_2)->beg) {
            temp_long_2 = temp_long;
            *(result.stoe+j) = temp_long;
            *(result.etos+temp_long) = j;
            j++;
          }
        }

        pending_length = 0;

        *(pending+pending_length++) = r;
      }
    }

    if (i+1 == b.size) {
      /* Merge sorting */
      for (long width = 1; width<pending_length; width <<= 1) {
        for (long k = 0; k<pending_length; k+=2*width) {
          end = MIN(k+2*width, pending_length);
          right = MIN(k+width, pending_length);
          ii = k;
          jj = right;
          for (long ell = k; ell<end; ell++) {
            if (ii<right &&
                (jj>=end ||
                 (b.rs+*(pending+ii))->beg <=
                 (b.rs+*(pending+jj))->beg))
              *(working+ell) = *(pending+ii++);
            else
              *(working+ell) = *(pending+jj++);
          }
        }

        temp = pending;
        pending = working;
        working = temp;
      }

      for (long k = 0, temp_long = -1, temp_long_2 = -1;
           k<pending_length;k++) {
        temp_long = *(correspondence_a+*(correspondence_ba+*(pending+k)));
        if (temp_long_2 < 0 ||
            (a.rs+temp_long)->beg != (a.rs+temp_long_2)->beg) {
          temp_long_2 = temp_long;
          *(result.stoe+j) = temp_long;
          *(result.etos+temp_long) = j;
          j++;
        }
      }
    }
  }

  result.size = size;

  free(stack);
  free(marksa);
  free(marksb);
  free(correspondence_a);
  free(correspondence_ba);
  free(pending);
  free(working);

  return result;
}

RSet RSet_follow(RSet a, RSet b)
{
  return RSet_follow_generic(a, b, 0);
}

RSet RSet_follow_es(RSet a, RSet b)
{
  return RSet_follow_generic(a, b, 1);
}

RSet RSet_follow_ee(RSet a, RSet b)
{
  return RSet_follow_generic(a, b, 2);
}

RSet RSet_follow_eb(RSet a, RSet b)
{
  return RSet_follow_generic(a, b, 3);
}

RSet RSet_hull(RSet rs)
{
  RSet result = { NULL, 0, NULL, NULL };

  RSet outer = RSet_outer(rs);

  result.rs = MYALLOC(Region, outer.size);

  long end = 0, size = 0;

  for (long i = 0, j = 0; i < outer.size;) {
    j = i + 1;
    end = (outer.rs+i)->end;

    for (;j<outer.size && (outer.rs+j)->beg <= end+1;)
      end = (outer.rs+j++)->end;

    *(result.rs+size++) = (Region) { (outer.rs+i)->beg, end };
    i = j;
  }

  result.size = size;

  destroy_RSet(outer);

  return result;
}

RSet RSet_union(RSet a, RSet b)
{
  long len = a.size + b .size;

  Region *totals = MYALLOC(Region, len);

  for (long i = 0; i < a.size; i++)
    *(totals+i) = *(a.rs+i);

  for (long i = 0; i < b.size; i++)
    *(totals+a.size+i) = *(b.rs+i);

  /* sort based on the start positions */
  long *start_permutations = merge_sort_RSet(totals, len, 1);

  /* The inverse permutation of start_permutations */
  long *start_inv = MYALLOC(long, len);

  /* sort based on the end positions */
  long *end_permutations = merge_sort_RSet(totals, len, 0);

  RSet result = (RSet) {
    MYALLOC(Region, len), len,
    MYALLOC(long, len), MYALLOC(long, len)
  };

  for (long i = 0; i < len; i++) {
    *(result.rs+i) = *(totals+*(start_permutations+i));
    *(start_inv+*(start_permutations+i)) = i;
  }

  for (long i = 0; i < len; i++) {
    *(result.stoe+i) = *(start_inv+*(end_permutations+i));
    *(result.etos+*(start_inv+*(end_permutations+i))) = i;
  }

  free(totals);
  free(start_permutations);
  free(start_inv);
  free(end_permutations);

  return result;
}

RSet RSet_intersection(RSet a, RSet b)
{
  MARKS *marks = MYALLOC(MARKS, a.size), size = 0;

  Region ra = (Region) { 0, 0 }, rb = (Region) { 0, 0 };

  for (long i = 0, j = 0; i < a.size && j < b.size;) {
    ra = *(a.rs+i);
    rb = *(b.rs+j);
    if (ra.beg == rb.beg) {
      if (ra.end == rb.end) {
        *(marks+i) = ACCEPTED;
        size++;
        i++;
        j++;
      } else if (ra.end < rb.end) {
        *(marks+i) = NONE;
        i++;
      } else {
        *(marks+i) = NONE;
        j++;
      }
    } else if (ra.beg < rb.beg) {
      *(marks+i) = NONE;
      i++;
    } else {
      *(marks+i) = NONE;
      j++;
    }
  }

  RSet result = (RSet) {
    MYALLOC(Region, size), size,
    MYALLOC(long, size), MYALLOC(long, size)
  };

  long *correspondence = MYALLOC(long, a.size);

  for (long i = 0, j = 0; i < a.size; i++) {
    if (*(marks+i) == ACCEPTED) {
      *(result.rs+j) = *(a.rs+i);
      *(correspondence+i) = j;
      j++;
    }
  }

  for (long i = 0, j = 0; i < a.size ; i++) {
    if (*(marks+*(a.stoe+i)) == ACCEPTED) {
      *(result.stoe+j) = *(correspondence+*(a.stoe+i));
      *(result.etos+*(correspondence+*(a.stoe+i))) = j;
      j++;
    }
  }

  free(marks);
  free(correspondence);

  return result;
}

RSet RSet_difference(RSet a, RSet b)
{
  MARKS *marks = MYALLOC(MARKS, a.size), size = 0;

  Region ra = (Region) { 0, 0 }, rb = (Region) { 0, 0 };

  for (long i = 0, j = 0; i < a.size && j < b.size;) {
    ra = *(a.rs+i);
    rb = *(b.rs+j);
    if (ra.beg == rb.beg) {
      if (ra.end == rb.end) {
        *(marks+i) = NONE;
        i++;
        j++;
      } else if (ra.end < rb.end) {
        size++;
        *(marks+i) = ACCEPTED;
        i++;
      } else {
        size++;
        *(marks+i) = ACCEPTED;
        j++;
      }
    } else if (ra.beg < rb.beg) {
      size++;
      *(marks+i) = ACCEPTED;
      i++;
    } else {
      j++;
      if (j>=b.size) {
        for (;i<a.size;) {
          *(marks+i++) = ACCEPTED;
          size++;
        }
      }
    }

  }

  RSet result = (RSet) {
    MYALLOC(Region, size), size,
    MYALLOC(long, size), MYALLOC(long, size)
  };

  long *correspondence = MYALLOC(long, a.size);

  for (long i = 0, j = 0; i < a.size; i++) {
    if (*(marks+i) == ACCEPTED) {
      *(result.rs+j) = *(a.rs+i);
      *(correspondence+i) = j;
      j++;
    }
  }

  for (long i = 0, j = 0; i < a.size ; i++) {
    if (*(marks+*(a.stoe+i)) == ACCEPTED) {
      *(result.stoe+j) = *(correspondence+*(a.stoe+i));
      *(result.etos+*(correspondence+*(a.stoe+i))) = j;
      j++;
    }
  }

  free(marks);
  free(correspondence);

  return result;
}

/* flag = 0 => include start and end regions
   flag = 1 => exclude start region
   flag = 2 => exclude end region
   flag = 3 => exclude start and end regions */
static RSet RSet_quote_generic(RSet a, RSet b, unsigned char flag)
{
  MARKS *marks = MYALLOC(MARKS, a.size);
  long *correspondence = MYALLOC(long, a.size);

  long size = 0;

  for (long i = 0; i < a.size;) *(marks+i++) = NONE;

  for (long i = 0, j = 0; i < a.size && j < b.size;) {
    for (;j<b.size && (b.rs+j)->beg < (a.rs+i)->end;) j++;

    if (j<b.size) {
      switch (flag) {
      case 0:
        (a.rs+i)->end = (b.rs+j)->end;
        break;
      case 1:
        (a.rs+i)->end = (b.rs+j)->end;
        (a.rs+i)->beg = (a.rs+i)->end+1;
        break;
      case 2:
        (a.rs+i)->end = (b.rs+j)->beg-1;
        break;
      default:
        /* NOTE: A wrong flag is treated as if 3 is passed. */
        (a.rs+i)->beg = (a.rs+i)->end+1;
        (a.rs+i)->end = (b.rs+j)->beg-1;
        break;
      }

      if ((a.rs+i)->beg <= (a.rs+i)->end) {
        *(marks+i) = ACCEPTED;
        size++;
      }

      for (;i<a.size && (a.rs+i)->beg < (b.rs+j)->end;) i++;
    }
  }

  RSet result = (RSet) {
    MYALLOC(Region, size), size,
    MYALLOC(long, size), MYALLOC(long, size)
  };

  for (long i = 0, j = 0; i < a.size; i++) {
    if (*(marks+i) == ACCEPTED) {
      *(result.rs+j) = *(a.rs+i);
      *(correspondence+i) = j;
      j++;
    }
  }

  for (long i = 0, j = 0; i < a.size; i++) {
    if (*(marks+*(a.stoe+i)) == ACCEPTED) {
      *(result.stoe+j) = *(correspondence+*(a.stoe+i));
      *(result.etos+*(correspondence+*(a.stoe+i))) = j;
      j++;
    }
  }

  free(marks);
  free(correspondence);

  return result;
}

RSet RSet_quote(RSet a, RSet b)
{
  return RSet_quote_generic(a, b, 0);
}

RSet RSet_quote_es(RSet a, RSet b)
{
  return RSet_quote_generic(a, b, 1);
}

RSet RSet_quote_ee(RSet a, RSet b)
{
  return RSet_quote_generic(a, b, 2);
}

RSet RSet_quote_eb(RSet a, RSet b)
{
  return RSet_quote_generic(a, b, 3);
}

/* NOTE: This algorithm is particularly messy. And this is the only
   place (up to now) that makes use of the array etos, while the
   inverse array stoe appears in a lot of places. */
RSet RSet_extract(RSet a, RSet b)
{
  /* NOTE: The created suffixes share the end position with the
     original region, so we only need to store its start position. */
  long *sufs = MYALLOC(long, a.size);

  /* A stack needs no initialization. */
  long *stack_sufs = MYALLOC(long, a.size);
  long stack_sufs_length = 0;

  Node_t new_region = EMPTY_NODE;
  Node_t new_region_next = EMPTY_NODE;
  long new_region_num = 0;
  Node_t *new_region_pointer = NULL;

  Region *temp_region = NULL;

  RSet bprime = RSet_hull(b);

  MARKS *marksa = MYALLOC(MARKS, a.size);
  MARKS *markss = MYALLOC(MARKS, a.size);

  for (long i = 0; i < a.size; i++) {
    *(sufs+i) = -1;
    *(marksa+i) = NONE;
    *(markss+i) = NONE;
  }

  /* If non-zero then it is from the array a. */
  unsigned char from_a_p = 1;

  /* If non-zero then this is a continuation by advancing B. */
  unsigned char continuep = 0;

  Region current = (Region) { 0, 0 };
  long current_stack = -1;

  for (long i = 0, j = 0;
       (from_a_p && i<a.size) || stack_sufs_length || continuep;) {

    if (from_a_p) current = *(a.rs+i);
    else if (!continuep) {
      current_stack = *(stack_sufs+ --stack_sufs_length);
      current = (Region) {
        *(sufs+current_stack), (a.rs+*(a.stoe+current_stack))->end
      };
    }

    continuep = 0;

#ifdef EXTRA_TEST
    printf("current beg = %ld, end = %ld\n",
           current.beg, current.end);
#endif

    if (j >= bprime.size || current.end < (bprime.rs+j)->beg) {
#ifdef EXTRA_TEST
      printf("accepting a region %s\n",
             (from_a_p) ? "from a" : "from suffixes");
#endif
      if (from_a_p)
        *(marksa+i) = ACCEPTED;
      else
        *(markss+current_stack) = ACCEPTED_SUFFIX;

      if (i+1<a.size &&
          (!stack_sufs_length ||
           (a.rs+i+1)->beg <
           *(sufs+*(stack_sufs+stack_sufs_length-1)) ||
           ((a.rs+i+1)->beg ==
            *(sufs+*(stack_sufs+stack_sufs_length-1)) &&
            (a.rs+i+1)->end <=
            (a.rs+*(a.stoe+*(stack_sufs+stack_sufs_length-1)))->end))) {
#ifdef EXTRA_TEST
        printf("next is from a\n");
        printf("now i = %ld\n", i+1);
#endif
        from_a_p = 1;
        i++;
      } else {
#ifdef EXTRA_TEST
        printf("next is a suffix\n");
#endif
        from_a_p = 0;
      }
    } else if ((bprime.rs+j)->end < current.beg) {
      continuep = 1;
#ifdef EXTRA_TEST
      printf("advance b, now j = %ld\n", j+1);
#endif
      j++;
    } else {                    /* non-empty intersection */
      if (current.beg < (bprime.rs+j)->beg) {
#ifdef EXTRA_TEST
        printf("adding a new region with beg = %ld and end = %ld\n",
               current.beg, (bprime.rs+j)->beg-1);
#endif
        temp_region = MYALLOC(Region, 1);
        *temp_region = (Region) { current.beg, (bprime.rs+j)->beg-1 };
        PUSH_NODE(new_region_num, new_region_pointer,
                  new_region, temp_region, r);
      }

      if ((bprime.rs+j)->end < current.end) {
#ifdef EXTRA_TEST
        printf("j = %ld", j);
        printf("a suffix with beg = %ld, end = %ld\n",
               (bprime.rs+j)->end+1,
               (from_a_p) ? (a.rs+i)->end :
               (a.rs+current_stack)->end);
#endif
        if (from_a_p) {
          *(stack_sufs+stack_sufs_length++) = *(a.etos+i);
          *(sufs+*(a.etos+i)) = (bprime.rs+j)->end+1;
        } else {
          *(sufs+current_stack) = (bprime.rs+j)->end+1;
        }
      }

      if (i+1<a.size &&
          (!stack_sufs_length ||
           (a.rs+i+1)->beg <
           *(sufs+*(stack_sufs+stack_sufs_length-1)) ||
           ((a.rs+i+1)->beg ==
            *(sufs+*(stack_sufs+stack_sufs_length-1)) &&
            (a.rs+i+1)->end <=
            (a.rs+*(a.stoe+*(stack_sufs+stack_sufs_length-1)))->end))) {
#ifdef EXTRA_TEST
        printf("next is from a.\n");
        printf("now i = %ld\n", i+1);
#endif
        from_a_p = 1;
        i++;
      } else {
#ifdef EXTRA_TEST
        printf("next is from a suffix.\n");
#endif
        from_a_p = 0;
      }
    }
  }

  Region *newsufs = MYALLOC(Region, a.size);

  long newsufs_len = 0;

  for (long i = 0; i < a.size; i++) {
    if (*(markss+*(a.stoe+i)) == ACCEPTED_SUFFIX) {
      *(newsufs+newsufs_len++) = (Region) {
        *(sufs+*(a.stoe+i)), (a.rs+i)->end
      };
    }
  }

  Region *new_region_array = MYALLOC(Region, new_region_num);

  if (new_region_num) {
    if (new_region_num > 1) {
      for (long i = 0; i < new_region_num; i++) {
        *(new_region_array+new_region_num-1-i) = *(new_region.value.r);
        free(new_region.value.r);
        if (new_region.next) {
          new_region_next = *(new_region.next);
          free(new_region.next);
          new_region = new_region_next;
        }
      }
    } else {
      *(new_region_array) = *(new_region.value.r);
      free(new_region.value.r);
    }
  };

  Region *accepted = MYALLOC(Region, a.size);
  long accepted_len = 0;

    for (long i = 0; i < a.size; i++)
      if (*(marksa+i) == ACCEPTED)
        *(accepted+accepted_len++) = *(a.rs+i);

  long total_len = new_region_num + newsufs_len + accepted_len;

#ifdef EXTRA_TEST
  printf("total = %ld, new region = %ld, new sufs = %ld, accepted = %ld\n",
         total_len, new_region_num, newsufs_len, accepted_len);
#endif

  RSet result = (RSet) { NULL, 0, NULL, NULL };

  result.rs = MYALLOC(Region, total_len);

  long *correspondence_a = MYALLOC(long, new_region_num);
  long *correspondence_b = MYALLOC(long, newsufs_len);
  long *correspondence_c = MYALLOC(long, accepted_len);

  merge_three_regions(new_region_array, newsufs, accepted,
                      new_region_num, newsufs_len, accepted_len,
                      result.rs,
                      correspondence_a, correspondence_b, correspondence_c);

  result.size = total_len;
  result.stoe = MYALLOC(long, total_len);
  result.etos = MYALLOC(long, total_len);

  /* Manually sorting... Too messy indeed. */

  long acursor = 0, bcursor = 0, ccursor = 0, cursor = 0;

  for (;acursor<new_region_num && bcursor<newsufs_len &&
         ccursor<accepted_len;) {
    if (REGION_LESS_END(new_region_array+acursor,
                        newsufs+bcursor) &&
        REGION_LESS_END(new_region_array+acursor,
                        accepted+ccursor)) {
      *(result.stoe+cursor) = *(correspondence_a+acursor);
      *(result.etos+*(correspondence_a+acursor)) = cursor;
      cursor++;
      acursor++;
    } else if (REGION_LESS_END(newsufs+bcursor,
                               new_region_array+acursor) &&
               REGION_LESS_END(new_region_array+acursor,
                               accepted+ccursor)) {
      *(result.stoe+cursor) = *(correspondence_b+bcursor);
      *(result.etos+*(correspondence_b+bcursor)) = cursor;
      cursor++;
      bcursor++;
    } else {
      *(result.stoe+cursor) = *(correspondence_c+ccursor);
      *(result.etos+*(correspondence_c+ccursor)) = cursor;
      cursor++;
      ccursor++;
    }
  }

  for (;acursor<new_region_num && bcursor<newsufs_len;) {
    if (REGION_LESS_END(new_region_array+acursor,
                        newsufs+bcursor)) {
      *(result.stoe+cursor) = *(correspondence_a+acursor);
      *(result.etos+*(correspondence_a+acursor)) = cursor;
      cursor++;
      acursor++;
    } else {
      *(result.stoe+cursor) = *(correspondence_b+bcursor);
      *(result.etos+*(correspondence_b+bcursor)) = cursor;
      cursor++;
      bcursor++;
    }
  }

  for (;acursor<new_region_num && ccursor<accepted_len;) {
    if (REGION_LESS_END(new_region_array+acursor,
                        accepted+ccursor)) {
      *(result.stoe+cursor) = *(correspondence_a+acursor);
      *(result.etos+*(correspondence_a+acursor)) = cursor;
      cursor++;
      acursor++;
    } else {
      *(result.stoe+cursor) = *(correspondence_c+ccursor);
      *(result.etos+*(correspondence_c+ccursor)) = cursor;
      cursor++;
      ccursor++;
    }
  }

  for (;bcursor<newsufs_len && ccursor<accepted_len;) {
    if (REGION_LESS_END(newsufs+bcursor,
                        new_region_array+acursor) &&
        REGION_LESS_END(newsufs+bcursor,
                        accepted+ccursor)) {
      *(result.stoe+cursor) = *(correspondence_b+bcursor);
      *(result.etos+*(correspondence_b+bcursor)) = cursor;
      cursor++;
      bcursor++;
    } else {
      *(result.stoe+cursor) = *(correspondence_c+ccursor);
      *(result.etos+*(correspondence_c+ccursor)) = cursor;
      cursor++;
      ccursor++;
    }
  }

  for (;acursor<new_region_num;) {
    *(result.stoe+cursor) = *(correspondence_a+acursor);
    *(result.etos+*(correspondence_a+acursor)) = cursor;
    cursor++;
    acursor++;
  }

  for (;bcursor<newsufs_len;) {
    *(result.stoe+cursor) = *(correspondence_b+bcursor);
    *(result.etos+*(correspondence_b+bcursor)) = cursor;
    cursor++;
    bcursor++;
  }

  for (;ccursor<accepted_len;) {
    *(result.stoe+cursor) = *(correspondence_c+ccursor);
    *(result.etos+*(correspondence_c+ccursor)) = cursor;
    cursor++;
    ccursor++;
  }

  free(correspondence_a);
  free(correspondence_b);
  free(correspondence_c);
  free(accepted);
  free(newsufs);
  free(sufs);
  free(stack_sufs);
  free(new_region_array);
  free(marksa);
  free(markss);
  destroy_RSet(bprime);

  return result;
}

/* Run query */

RSet run_query(const Query q, const index_result *source, const long len)
{
  /* This might be used as a temporary variable. */
  RSet result = (RSet) { NULL, 0, NULL, NULL };

  /* For storing queries */
  Node_t stack = EMPTY_NODE;
  int stack_len = 0;
  Node_t *stack_pointer = NULL;
  Node_t stack_next = EMPTY_NODE;

  /* For storing arguments */
  Node_t arg_stack = EMPTY_NODE;
  int arg_stack_len = 0;
  Node_t *arg_stack_pointer = NULL;
  Node_t arg_stack_next = EMPTY_NODE;

  Query *temp = NULL;
  RSet *temprs = NULL, *temprss = NULL;

  Query *left = NULL, *right = NULL;
  Query current = QUERY_LEAF(NULL);

  unsigned char matched = 0;

  temp = MYALLOC(Query, 1);

  *temp = q;

  PUSH_NODE(stack_len, stack_pointer,
            stack, temp, q);
  
  /* fprintf(stderr, "before for loop\n"); */

  for (;stack_len;) {
    POP_NODE(stack_len, temp, stack,
             stack_next, q);
    /* fprintf(stderr, "stack length = %d, and type = %d\n",
     *        stack_len, temp->type); */

    current = *temp;
    free(temp);

    left = current.left;
    right = current.right;

    if (current.type == 1) {
      /* a node */
      current.type = 2;
      current.left = NULL;
      current.right = NULL;
      temp = MYALLOC(Query, 1);
      *temp = current;
      PUSH_NODE(stack_len, stack_pointer, stack, temp, q);
      switch (current.value.op) {
      case sum:
      case prod:
      case minus:
      case in:
      case ni:
      case notin:
      case notni:
      case follow:
      case follow_es:
      case follow_ee:
      case follow_eb:
      case quote:
      case quote_es:
      case quote_ee:
      case quote_eb:
      case extract:
        /* Two args */
        temp = MYALLOC(Query, 1);
        *temp = *left;
        PUSH_NODE(stack_len, stack_pointer,
                  stack, temp, q);
        temp = MYALLOC(Query, 1);
        *temp = *right;
        PUSH_NODE(stack_len, stack_pointer,
                  stack, temp, q);
        break;
      case inner:
      case outer:
      case hull:
        /* One arg */
        temp = MYALLOC(Query, 1);
        *temp = *left;
        PUSH_NODE(stack_len, stack_pointer,
                  stack, temp, q);
        break;
      default:
        /* This should not happen. */
        goto stack_ends;
        break;
      }
    } else if (!(current.type)) {
      /* a leaf */

      /* fprintf(stderr, "found a leaf: %s\n", current.value.leaf); */

      if (current.value.leaf == NULL) {
        /* This should not happen */
        /* fprintf(stderr, "a null leaf, its op is %d\n", current.value.op); */
        goto stack_ends;
        break;
      }
        
      long i = 0;
      int j = 0;

      /* char *tempchar = NULL;
       *
       * fprintf(stderr, "type = 0, len = %ld\n", len);
       *
       * fprintf(stderr, "current value = %s\n", current.value.leaf); */

      for (; i < len; i++) {
        matched = 1;
        /* tempchar = MYALLOC(char, (source+i)->key_len+1);
         * for (int g = 0; g < (source+i)->key_len; g++)
         *   *(tempchar+g) = *((source+i)->key+g);
         *
         * *(tempchar+(source+i)->key_len) = 0;
         *
         * fprintf(stderr, "%ld th key = %s\n", i, tempchar);
         *
         * free(tempchar); */

        for (;j < (source+i)->key_len &&
               *(current.value.leaf+j) && matched;
             j++)
          if (*((source+i)->key+j) != *(current.value.leaf+j))
            matched = 0;

        /* Account for different lengths of strings. */
        if (matched &&
            (*(current.value.leaf+j) ||
             j<(source+i)->key_len))
          matched = 0;

        if (matched) {
          temprs = MYALLOC(RSet, 1);
          /* It is already in start position order, so we don't have
             to sort it again. */
          *temprs = array_to_RSet
            ((source+i)->indices,
             (source+i)->indices_len, 1);
          PUSH_NODE(arg_stack_len, arg_stack_pointer,
                    arg_stack, temprs, rs);
          break;
        }
      }
    } else {
      /* There is no branch.  If we reach here, then the arguments
         should already be on the ARG_STACK, so we can start
         evaluating the query. */

      /* First test the arg_stack.  If it is empty, then we are in
         trouble, so leave a message and end here. */
      
      if (!arg_stack_len) {
        fprintf(stderr, "run_query: reached a type 2 node while arg_stack is empty\n");
        goto stack_ends;
        break;
      }

      /* If there are two arguments, the left argument will be the
         first element in the stack, since it was pushed first, and
         hence encountered last. */
      switch (current.value.op) {
      case sum:
      case prod:
      case minus:
      case in:
      case ni:
      case notin:
      case notni:
      case follow:
      case follow_es:
      case follow_ee:
      case follow_eb:
      case quote:
      case quote_es:
      case quote_ee:
      case quote_eb:
      case extract:
        /* Two args */
        /* left => rs, right => rss */
        POP_NODE(arg_stack_len, temprs, arg_stack,
                 arg_stack_next, rs);
        POP_NODE(arg_stack_len, temprss, arg_stack,
                 arg_stack_next, rs);
        break;
      case inner:
      case outer:
      case hull:
        /* One arg */
        POP_NODE(arg_stack_len, temprs, arg_stack,
                 arg_stack_next, rs);
        break;
      default:
        /* This should not happen. */
        goto stack_ends;
        break;
      }

      switch (current.value.op) {
      case sum:
        result = RSet_union(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case prod:
        result = RSet_intersection(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case minus:
        result = RSet_difference(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case in:
        result = RSet_in(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case ni:
        result = RSet_ni(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case notin:
        result = RSet_notin(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case notni:
        result = RSet_notni(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case follow:
        result = RSet_follow(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case follow_es:
        result = RSet_follow_es(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case follow_ee:
        result = RSet_follow_ee(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case follow_eb:
        result = RSet_follow_eb(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case quote:
        result = RSet_quote(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case quote_es:
        result = RSet_quote_es(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case quote_ee:
        result = RSet_quote_ee(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case quote_eb:
        result = RSet_quote_eb(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case extract:
        result = RSet_extract(*temprs, *temprss);
        DESTROY_REGIONS(*temprs, *temprss);
        *temprs = result;
        free(temprss);
        break;
      case inner:
        result = RSet_inner(*temprs);
        destroy_RSet(*temprs);
        *temprs = result;
        break;
      case outer:
        result = RSet_outer(*temprs);
        destroy_RSet(*temprs);
        *temprs = result;
        break;
      case hull:
        result = RSet_hull(*temprs);
        destroy_RSet(*temprs);
        *temprs = result;
        break;
      default:
        /* This should not happen. */
        goto stack_ends;
        break;
      }

      PUSH_NODE(arg_stack_len, arg_stack_pointer,
                arg_stack, temprs, rs);
    }
  }

  /* fprintf(stderr, "after for loop\n"); */
  
 stack_ends:
  /* Now stack should be empty and arg_stack should contain exactly
     one element. */

  /* fprintf(stderr, "now stack len = %d, and arg stack len = %d\n",
   *         stack_len, arg_stack_len); */

  if (stack_len || arg_stack_len > 1) {
    /* fprintf(stderr, "Stack is empty or arg_stack has more than one element.\n"); */
    for (;stack_len;) {
      POP_NODE(stack_len, temp, stack,
               stack_next, q);
      free(temp);
    }

    for (;arg_stack_len;) {
      POP_NODE(arg_stack_len, temprs, arg_stack,
               arg_stack_next, rs);

      result = *temprs;
      free(temprs);
    }

    return result;
  }

  if (arg_stack_len) {
    POP_NODE(arg_stack_len, temprs, arg_stack,
             arg_stack_next, rs);
    result = *temprs;

    free(temprs);
  }
  
  return result;
}

void destroy_query(Query q)
{
  /* For storing queries */
  Node_t stack = EMPTY_NODE;
  int stack_len = 0;
  Node_t *stack_pointer = NULL;
  Node_t stack_next = EMPTY_NODE;

  Query *temp = NULL, *temp1 = NULL, *temp2 = NULL;

  temp = MYALLOC(Query, 1);
  *temp = q;
  PUSH_NODE(stack_len, stack_pointer,
            stack, temp, q);

  for (;stack_len;) {
    POP_NODE(stack_len, temp, stack, stack_next, q);

    switch (temp->type) {
    case 0:
      free(temp->value.leaf);
      free(temp);
      break;
    case 1:
      switch (temp->value.op) {
      case sum:
      case prod:
      case minus:
      case in:
      case ni:
      case notin:
      case notni:
      case follow:
      case follow_es:
      case follow_ee:
      case follow_eb:
      case quote:
      case quote_es:
      case quote_ee:
      case quote_eb:
      case extract:
        /* Two args */
        /* left => rs, right => rss */
        temp1 = temp->left;
        temp2 = temp->right;
        PUSH_NODE(stack_len, stack_pointer, stack, temp1, q);
        PUSH_NODE(stack_len, stack_pointer, stack, temp2, q);
        free(temp);
        break;
      case inner:
      case outer:
      case hull:
        /* One arg */
        temp1 = temp->left;
        PUSH_NODE(stack_len, stack_pointer, stack, temp1, q);
        free(temp);
        break;
      default:
        /* This should not happen. */
        goto stack_ends;
        break;
      }
      break;
    case 2:
      /* This should not happen since type 2 is only used internally
         by run_query. */
    default:
      goto stack_ends;
      break;
    }
  }

 stack_ends:
  return;
}

inline int query_argument_number(const Query q)
{
  switch (q.value.op) {
  case sum:
  case prod:
  case minus:
  case in:
  case ni:
  case notin:
  case notni:
  case follow:
  case follow_es:
  case follow_ee:
  case follow_eb:
  case quote:
  case quote_es:
  case quote_ee:
  case quote_eb:
  case extract:
    return 2;
    break;
  case outer:
  case inner:
  case hull:
    return 1;
    break;
  default:
    /* This should not happen. */
    return 0;
    break;
  }
}

#ifdef TEST

int main(int argc, char **argv)
{
  long *array = MYALLOC(long, 16);

  *(array+0)  = 2;
  *(array+1)  = 16;
  *(array+2)  = 12;
  *(array+3)  = 24;
  *(array+4)  = 23;
  *(array+5)  = 30;
  *(array+6)  = 29;
  *(array+7)  = 34;
  /* *(array+8)  = 6;
   * *(array+9)  = 10;
   * *(array+10) = 8;
   * *(array+11) = 9;
   * *(array+12) = 20;
   * *(array+13) = 24;
   * *(array+14) = 19;
   * *(array+15) = 25; */

  /* RSet rsa = array_to_RSet(array, 8, 0);
   *
   * printf("Printing rsa...\n");
   * print_RSet(rsa);
   * printf("\n"); */

  index_result *inres = MYALLOC(index_result, 1);

  *inres = (index_result) { MYALLOC(char, 3), 2, array, 8 };

  *(inres->key) = 'h';
  *(inres->key+1) = 'a';

  Query q = QUERY_NODE(follow_eb, &QUERY_LEAF("ha"), &QUERY_LEAF("ha"));

  RSet result = run_query(q, inres, 1);

  printf("result is as follows \n");
  print_RSet(result);

  /* RSet innera = RSet_hull(rsa); */

  /* printf("Printing hulla...\n");
   * print_RSet(innera); */

  /* *(array+0) = 56;
   * *(array+1) = 60;
   * *(array+2) = 64;
   * *(array+3) = 68;
   * *(array+4) = 68;
   * *(array+5) = 82; */
  /* *(array+6) = 7;
   * *(array+7) = 10;
   * *(array+8) = 5;
   * *(array+9) = 8; */

  /* RSet rsb = array_to_RSet(array, 6, 0);
   *
   * printf("Printing rsb...\n");
   * print_RSet(rsb);
   * printf("\n"); */

  /* RSet innerb = RSet_hull(rsb); */

  /* printf("Printing hullb...\n");
   * print_RSet(innerb); */

  /* RSet rsc = RSet_quote(rsa, rsb);
   *
   * printf("Printing rsc...\n");
   * print_RSet(rsc);
   * printf("\n"); */

  /* printf("before free array\n"); */
  free(array);
  /* printf("before free inres key\n"); */
  free(inres->key);
  free(inres);
  /* printf("before destroy result\n"); */
  destroy_RSet(result);
  /* destroy_RSet(rsa); */
  /* destroy_RSet(innera); */
  /* destroy_RSet(rsb); */
  /* destroy_RSet(innerb); */
  /* destroy_RSet(rsc); */

  return 0;
}
#endif
