#include "machine.h"
#include "compile.h"
#include "run.h"
#include "util.h"
#include "index.h"
#include <stdlib.h>
#include <stdio.h>

/* Return the length of keys so that we can allocate space for the
   array of keys. */
void preprocess_spec(const char *spec, unsigned char filep, long int *keys_len,
                     long int *spec_len, Node_t *parens)
{
  char *real_spec = NULL;

  if (!filep) real_spec = (char*) spec;
  else {
    real_spec = MYALLOC(char, READ_AMOUNT);
    read_entire_file(spec, &real_spec);
  }

  long int keys_length = 0, spec_length = 0;

  unsigned int c = 0;
  int c_utf8_increment = 0;

  unsigned char encountered = 0;

  long int num_parens_in = 0;
  Node_t *parens_pointer = NULL;
  Node_t parens_current = EMPTY_NODE;

  int parens_num = 0;
  
  for (long int i = 0;*(real_spec+i);i+=c_utf8_increment) {
    c_utf8_increment = utf8_len(*(real_spec+i));
    c = to_cp(real_spec+i, c_utf8_increment);
    spec_length+=c_utf8_increment;
    switch (c) {
    case ':':
      if (encountered) break;
      encountered = 1;
      keys_length++;
      break;
    case '\\':
      i+=c_utf8_increment;
      c_utf8_increment = utf8_len(*(real_spec+i));
      spec_length+=c_utf8_increment;
      break;
    case '(':
      parens_num++;
      break;
    case 13:
    case 10:
      if (encountered) {
        PUSH_NODE(num_parens_in, parens_pointer,
                  parens_current, parens_num, i);
      }
      
      encountered = 0;
    default:
      break;
    }
  }
  
  if (encountered) {
    PUSH_NODE(num_parens_in, parens_pointer,
              parens_current, parens_num, i);
  }

  if (filep) free(real_spec);

  *parens = parens_current;
  
  *keys_len = keys_length;
  *spec_len = spec_length;
}

/* REVIEW: We might want a function to "merge" different branches of
   the regular expression so that the resulting regular expression
   becomes more compact. But I think this is kind of an over-kill, so
   I will only leave a remark here. */

/* Process the specifications SPEC into a valid regular expression.

   If FILEP is non-zero, then SPEC specifies a file which contains the
   specifications; otherwise SPEC is itself a specification to be
   processed.

   KEYS is required to be already properly allocated.*/
char *spec_to_re(char *spec, unsigned char filep, char **keys,
                 long int keys_len, long int spec_len,
                 long int *total_keys_len)
{
  char *real_spec = NULL, *result = NULL;

  if (!filep) real_spec = spec;
  else {
    real_spec = MYALLOC(char, READ_AMOUNT);
    read_entire_file(spec, &real_spec);
  }

  /* The format of the specification is simple: a keyword followed by
     a colon, a space, and a regular expression. The regular
     expression can continue more than one line if the last character
     of the line is a backslash character.

     After the regular expressions are collected together, they will
     be compiled into a long regular expression, with appropriate
     parentheses. */

  int increment = 0;

  char **res_ptr = NULL;

  long int keys_cursor = 0, res_cursor = 0;

  long int result_length = 0;
  
  unsigned int c = 0;

  res_ptr = MYALLOC(char*, keys_len+1);

  long int last_cursor = 0;
  unsigned char ignore_p = 0, encounterd_p = 0;

  long *res_lenths = MYALLOC(long, keys_len+1);

  for (int i = 0; i<keys_len+1;) *(res_lenths+i++) = 0;

  *total_keys_len = 0;

  char *to_print = MYALLOC(char, 50);

  /* fprintf(stderr, "in spec_to_re:\n"); */

  for (long int spec_cursor = 0, negative_impact = 0;
       spec_cursor<=spec_len && *(real_spec+spec_cursor);) {
    
    negative_impact = 0;

    increment = utf8_len(*(real_spec+spec_cursor));
    c = to_cp(real_spec+spec_cursor, increment);

    to_utf8(c, to_print);

    /* fprintf(stderr, "read character %s\n", to_print); */
    
    /* if (*to_print == 'd')
     *   fprintf(stderr, "when reading d, c = %d\n", c); */
    
    switch (c) {
    case 13:
    case '\n':
      encounterd_p = 0;
      if (spec_cursor <= last_cursor || ignore_p) {
      } else {
        *(res_ptr+res_cursor) = MYALLOC(char, spec_cursor-last_cursor+1);

        for (int i = 0, for_increment = 0; i < spec_cursor-last_cursor;
             i++, for_increment++) {
          if (*(real_spec+last_cursor+i) == '\\' &&
              (*(real_spec+last_cursor+i+1) == 13 ||
               *(real_spec+last_cursor+i+1) == 10)) {
            *(*(res_ptr+res_cursor)+for_increment) = '\n';
            negative_impact++;
            i++;
          } else {
            *(*(res_ptr+res_cursor)+for_increment) = *(real_spec+last_cursor+i);
          }
        }
        *(*(res_ptr+res_cursor)+spec_cursor-last_cursor) = 0;

        *(res_lenths+res_cursor) = spec_cursor-last_cursor-negative_impact;
          
        res_cursor++;
        result_length += spec_cursor-last_cursor-negative_impact;
      }

      last_cursor = spec_cursor+1;
      ignore_p = 0;
      break;
    case ':':
      if (encounterd_p) break;
      
      encounterd_p = 1;
      if (spec_cursor <= last_cursor) {
        ignore_p = 1;
      } else {
        *(keys+keys_cursor) = MYALLOC(char, spec_cursor-last_cursor+1);
        *total_keys_len += spec_cursor-last_cursor;

        for (int i = 0; i < spec_cursor-last_cursor; i++) {
          *(*(keys+keys_cursor)+i) = *(real_spec+last_cursor+i);
        }
        *(*(keys+keys_cursor)+spec_cursor-last_cursor) = 0;

        keys_cursor++;
      }

      last_cursor = spec_cursor+1;
      break;
    case '\\':
      increment = 1+utf8_len(*(real_spec+spec_cursor+1));
      break;
    default:
      break;
    }

    spec_cursor += increment;

    negative_impact = 0;

    if (spec_cursor >= spec_len) {
      if (spec_cursor <= last_cursor || ignore_p) {
      } else {
        *(res_ptr+res_cursor) = MYALLOC(char, spec_cursor-last_cursor+1);

        for (int i = 0, for_increment = 0; i < spec_cursor-last_cursor;
             i++, for_increment++) {
          if (*(real_spec+last_cursor+i) == '\\' &&
              (*(real_spec+last_cursor+i+1) == 13 ||
               *(real_spec+last_cursor+i+1) == 10)) {
            *(*(res_ptr+res_cursor)+for_increment) = *(real_spec+last_cursor+i+1);
            negative_impact++;
            i++;
          } else {
            *(*(res_ptr+res_cursor)+for_increment) = *(real_spec+last_cursor+i);
          }
        }
        *(*(res_ptr+res_cursor)+spec_cursor-last_cursor) = 0;

        *(res_lenths+res_cursor) = spec_cursor-last_cursor-negative_impact;

        result_length += spec_cursor-last_cursor-negative_impact;
      }

      break;
    }
  }

  *(keys+keys_len) = NULL;
  *(res_ptr+keys_len) = NULL;

  if (filep) free(real_spec);

  result = MYALLOC(char, result_length+3*keys_len);

  res_cursor = keys_cursor = 0;

  int result_true_length = 0;

  *(result+result_true_length++) = '(';

  for (;keys_cursor < keys_len; keys_cursor++) {
    res_cursor = 0;
    
    for (;res_cursor<*(res_lenths+keys_cursor);res_cursor++) {
      /* fprintf(stderr, "appending %d\n", (int) *(*(res_ptr+keys_cursor)+res_cursor)); */
      *(result+result_true_length++) = *(*(res_ptr+keys_cursor)+res_cursor);
    }

    *(result+result_true_length++) = ')';
    
    if (keys_cursor+1<keys_len) {
      *(result+result_true_length++) = '|';
      *(result+result_true_length++) = '(';
    }
  }
  
  *(result+result_true_length) = 0;
  
/*   fprintf(stderr, "for the record the true length of the result is %d, \
 * while the estimated length is %ld\n",
 *           result_true_length, result_length+3*keys_len-1); */

  for (int i = 0; *(res_ptr+i); i++)
    free(*(res_ptr+i));

  free(res_ptr);
  free(res_lenths);
  free(to_print);

  return result;
}

/* Indexes the document given by STR, and outputs the resulting
   indices into a file with the name OUT_NAME.

   PARENS should be an array of the number of parentheses from the
   beginning of RE to the INDEX-th key. This is used to correspond
   keys with groups in the regular expression. */
__attribute__((__unused__, __cold__))
void index_document_file(const char *str, const char *re, const char* out_name,
                         long int *parens, int parens_length,
                         char **keys, long int total_keys_len)
{
  ProgInfo info = preprocess_prog((char *) re);
  Prog p = compile((char*) re, info);

  p = postprocess_prog(p, &info);
  
  if (!p) {
    fprintf(stderr, "Cannot compile the regular expression.\n");
    return;
  }
  
#ifdef EXTRA_TEST
  printf("Program is as follows.\n");
  print_prog(p, info.prog_depth);
#endif

  // Owner = index_document
  int *match_list = MYALLOC(int, 2*(info.parens+1));

  for (int i = 0; i < 2*(info.parens+1);) *(match_list+i++) = 0;

  Node_t *match_nodes = MYALLOC(Node_t, parens_length);
  
  int *match_node_nums = MYALLOC(int, parens_length);

  Node_t **match_node_pointers = MYALLOC(Node_t*, parens_length);

  Node_t *match_num_nodes = MYALLOC(Node_t, parens_length);
  
  int *match_num_node_nums = MYALLOC(int, parens_length);

  Node_t **match_num_node_pointers = MYALLOC(Node_t*, parens_length);

  Node_t temp_node = EMPTY_NODE;
  
  for (int i = 0; i < parens_length;i++) {
    *(match_nodes+i)=EMPTY_NODE;
    *(match_node_nums+i)=0;
    *(match_node_pointers+i) = NULL;
    
    *(match_num_nodes+i)=EMPTY_NODE;
    *(match_num_node_nums+i)=0;
    *(match_num_node_pointers+i) = NULL;
  }

  long int str_index = 0, increment = 0;

  char *to_write = NULL;
  long int max_write_num = total_keys_len;
  int to_write_cursor = 0;

  char *convert_buffer = NULL;
  int convert_size = sizeof(int)*8+1, number_length = 0;

  while (*(str+str_index) &&
         (increment=run_prog(p, info, (char*) (str+str_index), match_list))
         >0) {
    
#ifdef TEST
    printf("match list = ");
    for (int i = 0; i < 2*(info.parens+1); i++)
      printf("%d%s ", *(match_list+i),
             (i+1<2*(info.parens+1)) ? "," : "");
    printf("\n");
#endif

    for (int i = 0, j = 0; i < parens_length; i++) {
      j = i + 1 + *(parens+((i) ? i - 1 : 0));
#ifdef EXTRA_TEST
      printf("i = %d\n", i);
      printf("parens = %ld\n", *(parens+((i) ? i - 1 : 0)));
      printf("j = %d\n", j);
#endif
      /* Zero means matched, and -1 is unmatched. */
      if (*(match_list+2*j)+1 && *(match_list+2*j+1)+1) {
        convert_buffer = MYALLOC(char, convert_size);
        number_length = sprintf(convert_buffer, "%d", *(match_list+2*j));
        PUSH_NODE(*(match_node_nums+i),
                  *(match_node_pointers+i),
                  *(match_nodes+i),
                  convert_buffer, s);
        PUSH_NODE(*(match_num_node_nums+i),
                  *(match_num_node_pointers+i),
                  *(match_num_nodes+i),
                  number_length, i);
        convert_buffer = MYALLOC(char, convert_size);
        number_length = sprintf(convert_buffer, "%d", *(match_list+2*j+1));
        PUSH_NODE(*(match_node_nums+i),
                  *(match_node_pointers+i),
                  *(match_nodes+i),
                  convert_buffer, s);
        PUSH_NODE(*(match_num_node_nums+i),
                  *(match_num_node_pointers+i),
                  *(match_num_nodes+i),
                  number_length, i);
      }
    }

    str_index += increment;
  }

  max_write_num += 3*parens_length;
  
  for (int i = 0; i < parens_length; i++) {
    if (*(match_num_node_nums+i)) {
      if (*(match_num_node_nums+i)>1) {
        max_write_num += 2*(*(match_num_node_nums+i)-1);
        for (int j = 0; j < *(match_num_node_nums+i); j++) {
          max_write_num += (*(match_num_nodes+i)).value.i;
          if ((*(match_num_nodes+i)).next) {
            temp_node = *((*(match_num_nodes+i)).next);
            free((*(match_num_nodes+i)).next);
            *(match_num_nodes+i) = temp_node;
          }
        }
      } else {
        max_write_num += (*(match_num_nodes+i)).value.i;
      }
    }
  }

  to_write = MYALLOC(char, max_write_num);

#ifdef EXTRA_TEST
  printf("max_write_num = %ld\n", max_write_num);
  printf("parens_length = %d\n", parens_length);
#endif

  char **match_strs = MYALLOC(char*, 1);

  for (int i = 0; i < parens_length; i++) {
    for (int j = 0;*(*(keys+i)+j);j++)
      *(to_write+to_write_cursor++) = *(*(keys+i)+j);
    
    *(to_write+to_write_cursor++) = ':';
    *(to_write+to_write_cursor++) = ' ';
    
    match_strs = (char**)realloc(match_strs, *(match_node_nums+i));

    if (*(match_node_nums+i)) {
      if (*(match_node_nums+i) > 1) {
        for (int k = 0; k < *(match_node_nums+i); k++) {
          *(match_strs+((*(match_node_nums+i))-k-1)) = (*(match_nodes+i)).value.s;
          if ((*(match_nodes+i)).next) {
            temp_node = *((*(match_nodes+i)).next);
            free((*(match_nodes+i)).next);
            (*(match_nodes+i)) = temp_node;
          }
        }
      } else {
        *(match_strs) = (*(match_nodes+i)).value.s;
      }
    }
  
    for (int j = 0; j < *(match_node_nums+i); j++) {
      for (int k = 0;*(*(match_strs+j) + k);k++)
        *(to_write+to_write_cursor++) = *(*(match_strs+j) + k);
          
      free(*(match_strs+j));

      if (j+1<*(match_node_nums+i))
        *(to_write+to_write_cursor++) = ',';
    }

    *(to_write+to_write_cursor++) = 10;
  }
  
#ifdef EXTRA_TEST
  *(to_write+to_write_cursor) = 0;
  printf("to_write = %s\n", to_write);
#endif

  FILE *file = fopen(out_name, "w");
    
  if (!file) {
    fprintf(stderr, "Cannot open file \"%s\": ", out_name);
    perror(NULL);
    return;
  }

  fwrite(to_write, sizeof(char), to_write_cursor, file);

  fclose(file);

  free(to_write);
  free(match_strs);
  free(match_list);
  free(match_nodes);
  free(match_node_pointers);
  free(match_node_nums);
  free(match_num_nodes);
  free(match_num_node_pointers);
  free(match_num_node_nums);
  destroy_prog(p, info.prog_depth);
  destroy_info(info);
  return;
}

/* Indexes the document given by STR, and outputs the resulting
   indices into a file with the name OUT_NAME.

   PARENS should be an array of the number of parentheses from the
   beginning of RE to the INDEX-th key. This is used to correspond
   keys with groups in the regular expression.

   The difference from index_document_file is that this returns the
   result instead of storing the result in a file. */
index_result *index_document(const char *str, const char *re,
                             long int *parens, int parens_length,
                             char **keys)
{
  ProgInfo info = preprocess_prog((char *) re);
  Prog p = compile((char*) re, info);

  if (!p) {
    fprintf(stderr, "Cannot compile the regular expression.\n");
    return NULL;
  }

  p = postprocess_prog(p, &info);
  
#ifdef EXTRA_TEST
  printf("Program is as follows.\n");
  print_prog(p, info.prog_depth);
#endif

  // Owner = index_document
  int *match_list = MYALLOC(int, 2*(info.parens+1));

  for (int i = 0; i < 2*(info.parens+1);) *(match_list+i++) = 0;

  Node_t *match_nodes = MYALLOC(Node_t, parens_length);
  
  int *match_node_nums = MYALLOC(int, parens_length);

  Node_t **match_node_pointers = MYALLOC(Node_t*, parens_length);

  Node_t temp_node = EMPTY_NODE;
  
  for (int i = 0; i < parens_length;i++) {
    *(match_nodes+i)=EMPTY_NODE;
    *(match_node_nums+i)=0;
    *(match_node_pointers+i) = NULL;
  }

  long int str_index = 0, increment = 0;

  index_result *result = MYALLOC(index_result, parens_length+1);
  
  while (*(str+str_index) &&
         (increment=run_prog(p, info, (char*) (str+str_index), match_list))
         >0) {
    
#ifdef TEST
    printf("match list = ");
    for (int i = 0; i < 2*(info.parens+1); i++)
      printf("%d%s ", *(match_list+i),
             (i+1<2*(info.parens+1)) ? "," : "");
    printf("\n");
#endif

    for (int i = 0, j = 0; i < parens_length; i++) {
      j = i + 1 + *(parens+((i) ? i - 1 : 0));
#ifdef EXTRA_TEST
      printf("i = %d\n", i);
      printf("parens = %ld\n", *(parens+((i) ? i - 1 : 0)));
      printf("j = %d\n", j);
#endif
      /* Zero means matched, and -1 is unmatched. */
      if (*(match_list+2*j)+1 && *(match_list+2*j+1)+1) {
        PUSH_NODE(*(match_node_nums+i),
                  *(match_node_pointers+i),
                  *(match_nodes+i),
                  *(match_list+2*j)+str_index, i);
        PUSH_NODE(*(match_node_nums+i),
                  *(match_node_pointers+i),
                  *(match_nodes+i),
                  *(match_list+2*j+1)+str_index, i);
      }
    }

    str_index += increment;
#ifdef EXTRA_TEST
    printf("increment = %ld\nnow the rest is %s\n", increment, str+str_index);
#endif
  }

  long int *result_indices = NULL;

  for (int i = 0; i < parens_length; i++) {
    increment = 0;
    for (; *((*(keys+i))+increment);) increment++;
    
    *(result+i) = (index_result) {
      *(keys+i), increment, NULL, *(match_node_nums+i)
    };

    if (*(match_node_nums+i)) {
      result_indices = MYALLOC(long int, *(match_node_nums+i));

      if (*(match_node_nums+i)>1) {
        for (int j = 0; j < *(match_node_nums+i); j++) {
          *(result_indices+((*(match_node_nums+i))-j-1)) =
            (match_nodes+i)->value.i;
          
          if ((*(match_nodes+i)).next) {
            temp_node = *((*(match_nodes+i)).next);
            free((*(match_nodes+i)).next);
            *(match_nodes+i) = temp_node;
          }
        }
      } else {
        *(result_indices) = (match_nodes+i)->value.i;
      }
    }
    
    (result+i)->indices = result_indices;
  }

  free(match_list);
  free(match_nodes);
  free(match_node_pointers);
  free(match_node_nums);
  destroy_prog(p, info.prog_depth);
  destroy_info(info);
  return result;
}

void merge_keys(key_group_pairs *kgp, char ***keys,
                long **parens, long *keys_len)
{
  int extra_keys_num = 0, key_str_len = 0;

  for (int i = 0; i < kgp->key_len;)
    extra_keys_num += *(kgp->keys_lens+i++);

  char **temp_keys = MYALLOC(char*, *keys_len);
  long *temp_parens = MYALLOC(long, *keys_len);

  /* Copy *keys into temp_keys and *parens into temp_parens */
  for (int i = 0; i < *keys_len; i++) {
    key_str_len = 0;

    for (;*(*(*keys+i)+key_str_len);) key_str_len++;
    
    *(temp_keys+i) = MYALLOC(char, key_str_len+1);
    
    *(*(temp_keys+i)+key_str_len) = 0;

    for (int j = 0; j < key_str_len; j++)
      *(*(temp_keys+i)+j) = *(*(*keys+i)+j);
    
    *(temp_parens+i) = *(*parens+i);
  }

  /* NOTE: These pointers will no longer be available after the
     re-allocation, so we free them here. */
  for (int i = 0; i < *keys_len;) free(*(*keys+i++));
  
  *keys = (char**) realloc(*keys, sizeof(char*)*(*keys_len+extra_keys_num));
  *parens = (long*) realloc(*parens, sizeof(long)*(*keys_len+extra_keys_num));
  
  for (int i=0, j=0, k=0; i < *keys_len; i++) {
    
    *(*keys+j++) = *(temp_keys+i);
    *(*parens+k) = (i<kgp->key_len) ?
      (((k) ? *(*parens+k-1) : 0) + 
       ((*(kgp->keys_lens+i)) ? (**(kgp->group+i) - 1) :
        ((i) ? *(temp_parens+i) - *(temp_parens+i-1) :
         *(temp_parens+i)))) :
      (((k) ? *(*parens+k-1) : 0) +
       ((i) ? *(temp_parens+i) - *(temp_parens+i-1) :
        *(temp_parens+i)));
    k++;
    
    if (i < kgp->key_len) {
      for (int ell = 0; ell < *(kgp->keys_lens+i); ell++) {
        *(*keys+j++) = *(*(kgp->keys+i)+ell);
        /* k must be >= 1 here */
        *(*parens+k) = *(*parens+k-1) +
          ((ell == *(kgp->keys_lens+i)-1) ?
           (MAX(0, *(temp_parens+i)-1-*(*(kgp->group+i)+ell))) :
           *(*(kgp->group+i)+ell+1)-1-*(*(kgp->group+i)+ell));
        k++;
      }
    }
  }
  
  *keys_len += extra_keys_num;
  
  free(temp_parens);
  free(temp_keys);
}

/* If FREE_KEYS_P is non-zero, then also free the keys.

   Normally the keys will be merged with other keys, so that they will
   also get freed when we free those other keys. But if one does not
   merge the keys for some reason, then this still keeps the
   possibility of freeing those keys here. */
void destroy_kgp(key_group_pairs kgp, unsigned char free_keys_p)
{
  for (int i = 0; i < kgp.key_len; i++) {
    if (*(kgp.group+i)) free(*(kgp.group+i));
    
    if (free_keys_p)
      for (int j = 0; j < *(kgp.keys_lens+i); j++)
        if (*(*(kgp.keys+i)+j))
          free(*(*(kgp.keys+i)+j));
    
    
    if (*(kgp.keys+i)) free(*(kgp.keys+i));
  }

  free(kgp.keys_lens);
  free(kgp.keys);
  free(kgp.group);
}

/* This won't free the keys in the result, since that is still
   available through the original keys pointer. */
inline void free_index_result(index_result res)
{
  free(res.indices);
}

#ifdef TEST

int main (int argc, char **argv)
{
  long int keys_len = 0, spec_len = 0, total_keys_len;

  Node_t parens_node = EMPTY_NODE;
  Node_t parens_next = EMPTY_NODE;

  preprocess_spec("test_file", 1, &keys_len, &spec_len, &parens_node);

  long int *parens = MYALLOC(long int, keys_len+1);

   if (keys_len) {
     if (keys_len > 1) {
       for (int i = 0; i < keys_len; i++) {
         *(parens+keys_len-i-1) = (parens_node).value.i;
         if ((parens_node).next) {
           parens_next = *((parens_node).next);
           free((parens_node).next);
           (parens_node) = parens_next;
         }
       }
     } else {
       *(parens) = (parens_node).value.i;
     }
   }

#ifdef EXTRA_TEST
  printf("keys_len = %ld\n", keys_len);
  for (int i = 0; i < keys_len; i++) {
    printf("parens + %d = %ld\n", i, *(parens+i));
  }
#endif
  
  char **keys = MYALLOC(char*, keys_len+1);
  char *test = spec_to_re("test_file", 1, keys, keys_len,
                          spec_len, &total_keys_len);

  char ***extra_keys = MYALLOC(char**, 4);
  *(extra_keys) = MYALLOC(char*, 1);
  *(extra_keys+1) = MYALLOC(char*, 2);
  *(extra_keys+2) = NULL;
  *(extra_keys+3) = MYALLOC(char*, 1);

  *(*(extra_keys)) = MYALLOC(char, 3);
  *(*(extra_keys+1)) = MYALLOC(char, 7);
  *(*(extra_keys+1)+1) = MYALLOC(char, 7);
  *(*(extra_keys+3)) = MYALLOC(char, 7);

  *(*(*(extra_keys))) = 'a';
  *(*((*(extra_keys)))+1) = 'h';
  *(*((*(extra_keys)))+2) = 0;
  
  *(*((*(extra_keys+1)))) = 'w';
  *(*((*(extra_keys+1)))+1) = 'a';
  *(*((*(extra_keys+1)))+2) = 'n';
  *(*((*(extra_keys+1)))+3) = 'k';
  *(*((*(extra_keys+1)))+4) = 'z';
  *(*((*(extra_keys+1)))+5) = 'n';
  *(*((*(extra_keys+1)))+6) = 0;
  
  *(*(*(extra_keys+1)+1)) = 'w';
  *(*((*(extra_keys+1)+1))+1) = 'a';
  *(*((*(extra_keys+1)+1))+2) = 'w';
  *(*((*(extra_keys+1)+1))+3) = 'a';
  *(*((*(extra_keys+1)+1))+4) = 'w';
  *(*((*(extra_keys+1)+1))+5) = 'a';
  *(*((*(extra_keys+1)+1))+6) = 0;
  
  *(*((*(extra_keys+3)))) = 0xe8;
  *(*((*(extra_keys+3)))+1) = 0xa9;
  *(*((*(extra_keys+3)))+2) = 0xa6;
  *(*((*(extra_keys+3)))+3) = 0xe7;
  *(*((*(extra_keys+3)))+4) = 0x9c;
  *(*((*(extra_keys+3)))+5) = 0x8b;
  *(*((*(extra_keys+3)))+6) = 0;
  
  int *keys_lenss = MYALLOC(int, 4);
  *(keys_lenss) = 1;
  *(keys_lenss+1) = 2;
  *(keys_lenss+2) = 0;
  *(keys_lenss+3) = 1;
  
  int **groupss = MYALLOC(int*, 4);
  *(groupss) = MYALLOC(int, 1);
  *(groupss+1) = MYALLOC(int, 2);
  *(groupss+2) = NULL;
  *(groupss+3) = MYALLOC(int, 1);

  **(groupss) = 1;
  **(groupss+1) = 1;
  *(*(groupss+1)+1) = 2;
  **(groupss+3) = 1;

  key_group_pairs kgp = (key_group_pairs) { extra_keys, 4, keys_lenss, groupss };

  /* merge_keys(&kgp, &keys, &parens, &keys_len); */

  /* for (int i = 0; i<keys_len;i++) {
   *   printf("keys+%d = %s\n", i, *(keys+i));
   *   printf("parens+%d = %ld\n", i, *(parens+i));
   * } */
  
  printf("test = %s\n", test);

  char *test_in = MYALLOC(char, READ_AMOUNT);
  
  read_entire_file("test_in", &test_in);

  printf("test_in = %s\n", test_in);

  /* ProgInfo info = preprocess_prog(test);
   * Prog p = compile(test, info);
   * 
   * p = postprocess_prog(p, &info); */

  index_result *result = index_document
    (test_in, test, parens, keys_len, keys);

  /* printf("Program is as follows.\n");
   * print_prog(p, info.prog_depth); */
  /* printf("test = ");
   * for (int i = 0; *(test+i); i++) {
   *   printf("%d ", *(test+i));
   * }
   * printf("\n"); */
  
  index_result true_result = (index_result) { NULL, 0, NULL, 0 };
  
  for (int i = 0; i < keys_len; i++) {
    true_result = *(result+i);
    printf("i = %d, ", i);
    printf("key = %s, ", true_result.key);
    printf("key_len = %d, ", true_result.key_len);
    printf("indices_len = %ld, ", true_result.indices_len);
    printf("indices = ");
    for (int j = 0; j < true_result.indices_len; j++)
      printf("%s%ld", (j) ? ", " : "",
             *(true_result.indices+j));
    printf("\n");
    free(true_result.indices);
  }

  /* printf("before free result\n"); */
  free(result);

  /* printf("before free test\n"); */
  free(test);
  /* printf("before free test_in\n"); */
  free(test_in);

  /* printf("before free each key\n"); */
  for (int i = 0; i<keys_len; i++) {
    free(*(keys+i));
  }
  
  /* printf("before free parens\n"); */
  free(parens);
  /* printf("before free keys\n"); */
  free(keys);

  /* free(*(extra_keys));
   * free(*(extra_keys+1));
   * free(*(extra_keys+3)); */
  /* free(extra_keys); */
  /* free(keys_lenss); */
  /* free(*groupss);
   * free(*(groupss+1));
   * free(*(groupss+3)); */
  /* free(groupss); */

  destroy_kgp(kgp, 1);

  return 0;
}

#endif
