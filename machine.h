#ifndef MACHINE_H
#define MACHINE_H

#define MAX_CLASS 2             /* The number of supported operations
                                   in a character class */

typedef enum {
  SINGLE = -1,
  RANGE = -2
} CLASS_t;

typedef enum {
  AND,
  OR,
  NOT,
  NEXT,
  BEFORE_CHAR,                  // code name: BC
  BEFORE_CLASS,                 // code name: BL
  AFTER_CHAR,                   // code name: AC
  AFTER_CLASS,                  // code name: AL
  BETWEEN_CHAR,                 // code name: TC
  BETWEEN_CLASS                 // code name: TL
} ASSERT_t;

typedef enum {
  CHAR,
  CLASS,
  ASSERT,
  JUMP,
  SPLIT,
  SAVE,
  MATCH,
  FAIL,
  ANY
} OPCODE_t;

/* Maybe we will use a different data type for args in the future. */
typedef long int ARG_t;

typedef struct {
  OPCODE_t op;
  ARG_t *args;
} Inst_t;

/* An array of pointers to Inst_t */
typedef Inst_t **Prog;

/* REVIEW: Maybe add a field that holds the number of instructions
   that consume a character. Then we can obtain the number of
   instructions that don't consume characters by a simple
   substraction. This will be useful when we add threads, since the
   depth of the stack used there has an upper bound of the number of
   consecutive jump / split instructions, and that is bounded by the
   number of non-consuming instructions from above.

   But I think this is pre-mature optimisation now. */

/* A pointer to pointer is useless without some auxiliary
   information. */
typedef struct {
  int length;                   /* Length of the regular expression as text */
  int prog_depth;               /* Length of the program */
  int re_depth;                 /* Depth of the parentheses */
  int parens;                   /* Number of the parentheses */
  int num_class;                /* Number of character classes used */
  int *cl;                      /* Array of lengths of character classes */
  int num_escape;               /* Number of escape constructs */
  int *es;                      /* Array of lengths of escape constructs */
} ProgInfo;

typedef enum {
  NORMAL,
  SUCCESS,
  FAILURE
} PoolState;

#endif
