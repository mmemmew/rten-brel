#include <stdio.h>
#include <stdlib.h>
#include "util.h"

long int read_entire_file (const char *file_name, char **str)
{
  long int file_size = 0;
    
  FILE *file = fopen(file_name, "r");
    
  if (!file) {
    fprintf(stderr, "Cannot open file \"%s\": ", file_name);
    perror(NULL);
    return 0;
  }

  fseek(file, 0, SEEK_END);
  file_size = ftell(file);
  fseek(file, 0, SEEK_SET);

  *str = (char*)realloc(*str, file_size+1);
  *((*str)+file_size) = 0;

  fread(*str, sizeof(char), file_size, file);
    
  fclose(file);  
  return file_size;
}
