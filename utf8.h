#ifndef UTF8_H
#define UTF8_H

/* This code is adapted from Rosetta codes: 
   https://rosettacode.org/wiki/UTF-8_encode_and_decode 

   I removed the dependency on <inttypes.h> by the way. */

/* All lengths are in bytes */
int codepoint_len(const unsigned int cp); /* len of associated utf-8 char */

int utf8_len(const char ch);    /* len of utf-8 encoded char */
int utf8_len_back(const char *);
 
void to_utf8(const unsigned int cp, char *);

unsigned int to_cp(const char *chr, const int bytes);


#endif
