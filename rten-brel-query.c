#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "index.h"
#include "region.h"
#include <emacs-module.h>

/* I use GPLv3 in this project, but that is irrelevant. */
int plugin_is_GPL_compatible = 3;

#define INTERN(val) env->intern(env, val)

#define GLOBAL_REF(val) env->make_global_ref(env, val)

#define REF(val) env->make_global_ref(env, env->intern(env, val))

#define STRING(val, len) env->make_string(env, val, len)

#define FUNCALL0(func) env->funcall(env, func, 0, NULL)

#define FUNCALL1(func, a) env->funcall(env, func, 1, (emacs_value[]){a})

#define FUNCALL2(func, a, b) env->funcall(env, func, 2, (emacs_value[]){a, b})

#define FUNCALLN(func, N, array) env->funcall(env, func, N, array)

#define MESSAGE(X, Y) FUNCALL1(INTERN("message"), STRING(X, Y))

#define CONS(car, cdr) FUNCALL2(INTERN("cons"), car, cdr)

#define CONSP(X) FUNCALL1(INTERN("consp"), X)

#define CAR(X) FUNCALL1(INTERN("car"), X)

#define CDR(X) FUNCALL1(INTERN("cdr"), X)

#define INT(val) env->make_integer(env, val)

#define LIST(len, array) FUNCALLN(INTERN("list"), len, array)

#define NTH(N, LS) FUNCALL2(INTERN("nth"), INT(N), LS)

#define VEC(len, array) FUNCALLN(INTERN("vector"), len, array)

#define PROVIDE(X) FUNCALL1(INTERN("provide"), INTERN(X))

#define TYPEOF(X) env->type_of(env, X)

#define EQ(X, Y) env->eq(env, X, Y)

#define OFTYPE(X, Y) EQ(INTERN(Y), TYPEOF(X))

#define GET_STRING(X, Y, LEN) {                         \
    env->copy_string_contents(env, X, NULL, &LEN);      \
    Y = MYALLOC(char, LEN);                             \
    env->copy_string_contents(env, X, Y, &LEN);         \
  }

#define GET_INT(X) env->extract_integer(env, X)

#define NIL (INTERN("nil"))

#define CHECK_TYPE(X, T, P, R) {                                        \
    if (!(OFTYPE(X, T))) {                                              \
      emacs_value error_data[] = {                                      \
        INTERN(P), X                                                    \
      };                                                                \
      env->non_local_exit_signal(env, INTERN("wrong-type-argument"),    \
                                 LIST(2, error_data));                  \
      return R;                                             \
    }                                                                   \
  }

#define CHECK_TYPE_NON_NIL(X, T, P, R) {                                \
    if (!(EQ(X, NIL))) {                                                \
      if (!(OFTYPE(X, T))) {                                            \
        emacs_value error_data[] = {                                    \
          INTERN(P), X                                                  \
        };                                                              \
        env->non_local_exit_signal(env, INTERN("wrong-type-argument"),  \
                                   LIST(2, error_data));                \
        return R;                                                       \
      }                                                                 \
    }                                                                   \
  }

__attribute__((__cold__))
static void
emacs_defun(emacs_env *env, void* cfunc, char* func_name, char* doc, size_t min, size_t max)
{
  emacs_value func = env->make_function(env, min, max, cfunc, doc, NULL);
  FUNCALL2(INTERN("defalias"), INTERN(func_name), func);
}

__attribute__((__const__))
static emacs_value
index_result_to_emacs_list(emacs_env *env, index_result result)
{
  emacs_value key = STRING(result.key, result.key_len);
  /* printf("key = %s\n", result.key);
   * printf("indices_len = %ld\n", result.indices_len); */
  emacs_value *indices = MYALLOC(emacs_value, result.indices_len);
  for (int i = 0; i < result.indices_len; i++)
    *(indices+i) = INT(*(result.indices+i));
  emacs_value value = VEC(result.indices_len, indices);
  free(indices);
  return CONS(key, value);
}

__attribute__((__const__))
static emacs_value
RSet_to_emacs_list(emacs_env *env, RSet rs)
{
  long *array = RSet_to_array(rs);

  emacs_value *earray = MYALLOC(emacs_value, 2*(rs.size));

  for (long i = 0; i < 2*(rs.size); i++)
    *(earray+i) = INT(*(array+i));

  free(array);

  emacs_value result = LIST(2*(rs.size), earray);

  free(earray);

  return result;
}

__attribute__((__pure__))
static RSet
emacs_list_to_RSet(emacs_env *env, emacs_value ls)
{
  RSet result = (RSet) { NULL, 0, NULL, NULL };

  if (EQ(FUNCALL1(INTERN("listp"), ls), NIL))
    {
      emacs_value error_data[] = { INTERN("listp"), ls };
      env->non_local_exit_signal(env, INTERN("wrong-type-argument"),
                                 LIST(2, error_data));
      return (result);
    }

  ptrdiff_t len = GET_INT(FUNCALL1(INTERN("length"), ls));

  if (len % 2) {
    emacs_value error_data[] = {
      STRING("Length should be even.", 22)
    };
    env->non_local_exit_signal(env, INTERN("error"),
                               LIST(1, error_data));
    return result;
  }

  ptrdiff_t *array = MYALLOC(ptrdiff_t, len);

  for (long i = 0; i < len; i+=2) {
    *(array+i) = GET_INT(NTH(i, ls));
    *(array+i+1) = GET_INT(NTH(i+1, ls));
  }

  result = array_to_RSet(array, len, 0);

  free(array);

  return result;
}

/* Apply an operation OP to ARGS.

   OP should be a string naming a supported operation.

   ARGS should be a list of arguments.  Depending on the operation,
   there might be one or two arguments.  Each argument should be a
   list of integers of even length.  It consists of the beginning and
   the ending positions of the regions.  So for example, the list (1 2
   3 4) describes two regions; the first region is the interval (1 2)
   and the second is (3 4). */
emacs_value
rten_brel_apply_op_to_lists(emacs_env *env, ptrdiff_t nargs,
                            emacs_value *args, void* data)
{
  emacs_value op = *args;
  emacs_value ls = *(args+1);

  CHECK_TYPE(op, "string", "stringp", NIL);

  if (EQ(FUNCALL1(INTERN("listp"), ls), NIL))
    {
      emacs_value error_data[] = { INTERN("listp"), ls };
      env->non_local_exit_signal(env, INTERN("wrong-type-argument"),
                                 LIST(2, error_data));
      return (NIL);
    }

  ptrdiff_t len = GET_INT(FUNCALL1(INTERN("length"), ls));

  if (len < 1 || len > 2) {
    emacs_value error_data [] = {
      STRING("The list should have length either 1 or 2.", 42)
    };
    env->non_local_exit_signal(env, INTERN("error"),
                               LIST(1, error_data));
    return NIL;
  }

  char *op_str = NULL;
  ptrdiff_t op_str_len =0;

  GET_STRING(op, op_str, op_str_len);

  RSet left = (RSet) { NULL, 0, NULL, NULL };
  RSet right= (RSet) { NULL, 0, NULL, NULL };
  RSet result = (RSet) { NULL, 0, NULL, NULL };

  switch (*op_str) {
  case 'u':                     /* union */
    left = emacs_list_to_RSet(env, NTH(0, ls));
    right = emacs_list_to_RSet(env, NTH(1, ls));
    result = RSet_union(left, right);
    destroy_RSet(left);
    destroy_RSet(right);
    break;
  case 'i':
    if (op_str_len == 2) {           /* in */
      left = emacs_list_to_RSet(env, NTH(0, ls));
      right = emacs_list_to_RSet(env, NTH(1, ls));
      result = RSet_in(left, right);
      destroy_RSet(left);
      destroy_RSet(right);
    } else if (*(op_str+2) == 't') { /* intersection */
      left = emacs_list_to_RSet(env, NTH(0, ls));
      right = emacs_list_to_RSet(env, NTH(1, ls));
      result = RSet_intersection(left, right);
      destroy_RSet(left);
      destroy_RSet(right);
    } else {                         /* inner */
      left = emacs_list_to_RSet(env, NTH(0, ls));
      result = RSet_inner(left);
      destroy_RSet(left);
    }
    break;
  case 'd':                     /* difference */
    left = emacs_list_to_RSet(env, NTH(0, ls));
    right = emacs_list_to_RSet(env, NTH(1, ls));
    result = RSet_difference(left, right);
    destroy_RSet(left);
    destroy_RSet(right);
    break;
  case 'o':                     /* outer */
    left = emacs_list_to_RSet(env, NTH(0, ls));
    result = RSet_outer(left);
    destroy_RSet(left);
    break;
  case 'h':                     /* hull */
    left = emacs_list_to_RSet(env, NTH(0, ls));
    result = RSet_hull(left);
    destroy_RSet(left);
    break;
  case 'n':
    if (op_str_len == 2) {           /* ni */
      left = emacs_list_to_RSet(env, NTH(0, ls));
      right = emacs_list_to_RSet(env, NTH(1, ls));
      result = RSet_ni(left, right);
      destroy_RSet(left);
      destroy_RSet(right);
    } else if (*(op_str+3) == 'i') { /* notin */
      left = emacs_list_to_RSet(env, NTH(0, ls));
      right = emacs_list_to_RSet(env, NTH(1, ls));
      result = RSet_notin(left, right);
      destroy_RSet(left);
      destroy_RSet(right);
    } else {                         /* notni */
      left = emacs_list_to_RSet(env, NTH(0, ls));
      right = emacs_list_to_RSet(env, NTH(1, ls));
      result = RSet_notni(left, right);
      destroy_RSet(left);
      destroy_RSet(right);
    }
    break;
  case 'f':                     /* follow variants */
    left = emacs_list_to_RSet(env, NTH(0, ls));
    right = emacs_list_to_RSet(env, NTH(1, ls));

    if (op_str_len == 6) {       /* follow */
      result = RSet_follow(left, right);
    } else if (*(op_str+8) == 's') { /* follow_es */
      result = RSet_follow_es(left, right);
    } else if (*(op_str+8) == 'e') { /* follow_ee */
      result = RSet_follow_ee(left, right);
    } else {                    /* follow_eb */
      result = RSet_follow_eb(left, right);
    }

    destroy_RSet(left);
    destroy_RSet(right);

    break;
  case 'q':                     /* quote variants */
    left = emacs_list_to_RSet(env, NTH(0, ls));
    right = emacs_list_to_RSet(env, NTH(1, ls));

    if (op_str_len == 5) {      /* quote */
      result = RSet_quote(left, right);
    } else if (*(op_str+7) == 's') { /* quote_es */
      result = RSet_quote_es(left, right);
    } else if (*(op_str+7) == 'e') { /* quote_ee */
      result = RSet_quote_ee(left, right);
    } else {                   /* quote_eb */
      result = RSet_quote_eb(left, right);
    }

    destroy_RSet(left);
    destroy_RSet(right);
    break;
  case 'e':                     /* extract */
    left = emacs_list_to_RSet(env, NTH(0, ls));
    right = emacs_list_to_RSet(env, NTH(1, ls));
    result = RSet_extract(left, right);
    destroy_RSet(left);
    destroy_RSet(right);
    break;
  default:                      /* Should not occur */
    break;
  }

  free(op_str);

  emacs_value result_value = RSet_to_emacs_list(env, result);

  destroy_RSet(result);

  return result_value;
}

static key_group_pairs
emacs_value_to_kgp(emacs_env *env, emacs_value kgpv)
{

  key_group_pairs kgp = (key_group_pairs) { NULL, 0, NULL, NULL };

  ptrdiff_t vector_size = 0, size = 0;

  emacs_value vector_element = NIL, current = NIL, car = NIL, cdr = NIL;

  if (!(EQ(kgpv, NIL))) {
    vector_size = env->vec_size(env, kgpv);

    for (ptrdiff_t i = 0; i < vector_size; i++) {
      vector_element = env->vec_get(env, kgpv, i);
      CHECK_TYPE(vector_element, "vector", "vectorp", kgp);

      size = env->vec_size(env, vector_element);

      for (ptrdiff_t j = 0; j < size; j++) {
        current = env->vec_get(env, vector_element, j);
        CHECK_TYPE(current, "cons", "consp", kgp);

        car = CAR(current);

        CHECK_TYPE(car, "string", "stringp", kgp);

        cdr = CDR(current);

        CHECK_TYPE(cdr, "integer", "integerp", kgp);
      }
    }

    /* Now all the arguments should be of correct types. */

    kgp.key_len = (int) vector_size;
    kgp.keys = MYALLOC(char**, vector_size);
    kgp.keys_lens = MYALLOC(int, vector_size);
    kgp.group = MYALLOC(int*, vector_size);

    ptrdiff_t str_len = 0;

    for (ptrdiff_t i = 0; i < vector_size; i++) {
      vector_element = env->vec_get(env, kgpv, i);
      size = env->vec_size(env, vector_element);

      *(kgp.keys_lens+i) = size;
      *(kgp.keys+i) = MYALLOC(char*, size);
      *(kgp.group+i) = MYALLOC(int, size);

      for (ptrdiff_t j = 0; j < size; j++) {
        current = env->vec_get(env, vector_element, j);
        car = CAR(current);
        cdr = CDR(current);

        GET_STRING(car, *(*(kgp.keys+i)+j), str_len);
        *(*(kgp.group+i)+j) = GET_INT(cdr);
      }
    }
  }

  return kgp;
}

/* This function is the test function of the module, which indexes the
   documents according to the specifications of the user.

   The function accepts three required arguments and one optional
   argument.

   The first argument specifies the name of the file from which the
   function should read a specification. The second argument should be
   the name of the file to index. The third argument should specify
   the name of the file to which the resulting indices should be
   stored. And the fourth optional argument specifies additional
   specification that should be considered together with the
   specifications in the file.

   In the design, the basic format of a document should be stored in a
   file, since that might be used frequently. And when the user
   queries for something special, then those special things will be
   converted to specifications, and that can be merged easily. */

__attribute__((__deprecated__("This is no longer used.\n"
                              "Please use rten_brel_query directly"),
               __unused__))
static
emacs_value
rten_brel_index(emacs_env *env, ptrdiff_t nargs, emacs_value *args,
                void *data)
{
  /* I don't have to check the number of arguments falls in the
     specified range, since when calling from Emacs this will be
     handled already. */

  CHECK_TYPE(*args, "string", "stringp", NIL);
  CHECK_TYPE(*(args+1), "string", "stringp", NIL);
  if (nargs >= 3 && !(EQ(*(args+2), NIL)))
    CHECK_TYPE(*(args+2), "string", "stringp", NIL);

  emacs_value current, car, cdr;
  emacs_value vector_element;
  ptrdiff_t size = 0, vector_size = 0;

  key_group_pairs kgp = (key_group_pairs) { NULL, 0, NULL, NULL };

  if (nargs == 4 && !(EQ(*(args+3), NIL))) {
    vector_size = env->vec_size(env, *(args+3));

    for (ptrdiff_t i = 0; i < vector_size; i++) {
      vector_element = env->vec_get(env, *(args+3), i);
      CHECK_TYPE(vector_element, "vector", "vectorp", NIL);

      size = env->vec_size(env, vector_element);

      for (ptrdiff_t j = 0; j < size; j++) {
        current = env->vec_get(env, vector_element, j);
        CHECK_TYPE(current, "cons", "consp", NIL);

        car = CAR(current);

        CHECK_TYPE(car, "string", "stringp", NIL);

        cdr = CDR(current);

        CHECK_TYPE(cdr, "integer", "integerp", NIL);
      }
    }

    /* Now all the arguments should be of correct types. */

    kgp.key_len = (int) vector_size;
    kgp.keys = MYALLOC(char**, vector_size);
    kgp.keys_lens = MYALLOC(int, vector_size);
    kgp.group = MYALLOC(int*, vector_size);

    ptrdiff_t str_len = 0;

    for (ptrdiff_t i = 0; i < vector_size; i++) {
      vector_element = env->vec_get(env, *(args+3), i);
      size = env->vec_size(env, vector_element);

      *(kgp.keys_lens+i) = size;
      *(kgp.keys+i) = MYALLOC(char*, size);
      *(kgp.group+i) = MYALLOC(int, size);

      for (ptrdiff_t j = 0; j < size; j++) {
        current = env->vec_get(env, vector_element, j);
        car = CAR(current);
        cdr = CDR(current);

        GET_STRING(car, *(*(kgp.keys+i)+j), str_len);
        *(*(kgp.group+i)+j) = GET_INT(cdr);
      }
    }
  }

  long int keys_len = 0, spec_len = 0, total_keys_len;

  Node_t parens_node = EMPTY_NODE;
  Node_t parens_next = EMPTY_NODE;

  char *re_file = NULL, *index_file = NULL;
  char *spec = NULL, *extra_spec = NULL;

  ptrdiff_t str_len = 0;

  GET_STRING(*args, re_file, str_len);

  if (nargs >= 3 && !(EQ(*(args+2), NIL))) {
    /* fprintf(stderr, "inside non-nil additional spec\n"); */
    GET_STRING(*(args+2), extra_spec, str_len);
    /* We abuse the pointer index_file to store a temporary string, and
       spec_len for the length of the temporary string. */
    index_file = MYALLOC(char, READ_AMOUNT);
    spec_len = read_entire_file(re_file, &index_file);

    if (!spec_len) {

      for (str_len = 0;*(re_file+str_len);) str_len++;

      emacs_value error_data[] = {
        STRING("Cannot open file: ", 18),
        STRING(re_file, str_len)
      };

      env->non_local_exit_signal(env, INTERN("error"),
                                 LIST(2, error_data));

      free(extra_spec);
      free(index_file);
      free(re_file);
      destroy_kgp(kgp, 0);
      return NIL;
    }

    spec = MYALLOC(char, spec_len + str_len);

    for (int i = 0; i < spec_len; i++)
      *(spec+i) = *(index_file+i);

    for (int i = 0; i < str_len; i++)
      *(spec+spec_len+i) = *(extra_spec+i);

    free(index_file);
    free(extra_spec);
  } else {
    spec = MYALLOC(char, READ_AMOUNT);

    if (!(read_entire_file(re_file, &spec))) {

      for (str_len = 0;*(re_file+str_len);) str_len++;

      emacs_value error_data[] = {
        STRING("Cannot open file: ", 18),
        STRING(re_file, str_len)
      };

      env->non_local_exit_signal(env, INTERN("error"),
                                 LIST(2, error_data));

      free(spec);
      free(re_file);
      destroy_kgp(kgp, 0);
      return NIL;
    }
  }

  GET_STRING(*(args+1), index_file, str_len);

  /* fprintf(stderr, "spec = ");
   *
   * for (int z = 0; *(spec+z); z++)
   *   fprintf(stderr, "%s%d", (z) ? ", " : "",
   *           (int) *(spec+z));
   *
   * fprintf(stderr, "\n"); */

  preprocess_spec(spec, 0, &keys_len, &spec_len, &parens_node);

  long int *parens = MYALLOC(long int, keys_len+1);

#ifdef EXTRA_TEST
  printf("keys_len = %ld\n", keys_len);
  for (int i = 0; i < keys_len; i++) {
    printf("parens + %d = %ld\n", i, *(parens+i));
  }
#endif

  if (keys_len) {
    if (keys_len > 1) {
      for (int i = 0; i < keys_len; i++) {
        *(parens+keys_len-i-1) = (parens_node).value.i;
        if ((parens_node).next) {
          parens_next = *((parens_node).next);
          free((parens_node).next);
          (parens_node) = parens_next;
        }
      }
    } else {
      *(parens) = (parens_node).value.i;
    }
  }

  char **keys = MYALLOC(char*, keys_len+1);
  char *re = spec_to_re(spec, 0, keys, keys_len,
                        spec_len, &total_keys_len);

  /* fprintf(stderr, "re = %s\n", re); */

#ifdef TEST
  printf("The regular expression = %s\n", re);
#endif

  char *to_index = MYALLOC(char, READ_AMOUNT);

  if (!(read_entire_file(index_file, &to_index))) {

    emacs_value error_data[] = {
      STRING("Cannot open file: ", 18),
      STRING(index_file, str_len)
    };

    env->non_local_exit_signal(env, INTERN("error"),
                               LIST(2, error_data));

    free(spec);
    free(index_file);
    free(to_index);
    for (int i = 0; i<keys_len; i++)
      free(*(keys+i));

    destroy_kgp(kgp, 0);

    free(re);
    free(parens);
    free(keys);
    return NIL;
  }

#ifdef TEST
  printf("The document to index = %s\n", to_index);
#endif

  if (kgp.key_len)
    merge_keys(&kgp, &keys, &parens, &keys_len);

  /* char num_buffer[100];
   * int keys_lenss = 0, temp = 0;
   *
   * for (int i = 0; i < keys_len; i++) {
   *   keys_lenss += 3;
   *   for (int j = 0; *(*(keys+i)+j); j++) keys_lenss++;
   *   keys_lenss += sprintf(num_buffer, "%ld", *(parens+i));
   * }
   *
   * char *to_print = MYALLOC(char, keys_lenss);
   *
   * for (int i = 0, k = 0; i < keys_len && k < keys_lenss; i++) {
   *   for (int j = 0; *(*(keys+i)+j); j++)
   *     *(to_print+k++) = *(*(keys+i)+j);
   *
   *   *(to_print+k++) = ':';
   *   *(to_print+k++) = 32;
   *
   *   temp = sprintf(num_buffer, "%ld", *(parens+i));
   *
   *   for (int j = 0; j < temp;)
   *     *(to_print+k++) = *(num_buffer+j++);
   *
   *   *(to_print+k++) = '\n';
   * }
   *
   * fprintf(stderr, "merged keys = %s\n", to_print); */

  /* MESSAGE(to_print, keys_lenss); */

  /* free(to_print); */

  /* fprintf(stderr, "after message\n");
   *
   * fprintf(stderr, "before index re = %s\n", re); */

  /* ProgInfo info = preprocess_prog(test);
   * Prog p = compile(test, info);
   *
   * p = postprocess_prog(p, &info); */

  /* The result will be of length equal to keys_len. */
  index_result *raw_result = index_document
    (to_index, re, parens, keys_len, keys);

  /* index_result true_result = (index_result) { NULL, 0, NULL, 0 };
   *
   * for (int i = 0; i < keys_len; i++) {
   *   true_result = *(raw_result+i);
   *   printf("i = %d, ", i);
   *   printf("key = %s, ", true_result.key);
   *   printf("key_len = %d, ", true_result.key_len);
   *   printf("indices_len = %ld, ", true_result.indices_len);
   *   printf("indices = \n");
   *   for (int j = 0; j < true_result.indices_len; j++)
   *     printf("%s%ld", (j) ? ", " : "",
   *            *(true_result.indices+j));
   *   printf("\n");
   * } */

  emacs_value *result_lists = MYALLOC(emacs_value, keys_len);

  emacs_value result;

  if (raw_result) {

    for (int i = 0; i < keys_len; i++) {
      *(result_lists+i) = index_result_to_emacs_list(env, *(raw_result+i));
      free_index_result(*(raw_result+i));
    }

    result = LIST(keys_len, result_lists);
    free(raw_result);
  } else {
    emacs_value error_data[] = { STRING("Cannot compile the program", 27) };
    env->non_local_exit_signal(env, INTERN("error"), LIST(1, error_data));
    result = NIL;
  }

  free(spec);
  free(index_file);
  free(to_index);
  free(result_lists);
  free(re_file);

#ifdef EXTRA_TEST
  for (int i = 0; i < keys_len; i++)
    printf("keys+%d = %s\n", i, *(keys+i));
#endif

  /* fprintf(stderr, "before free each key, len = %ld\n", keys_len); */
  for (int i = 0; i<keys_len; i++) {
    /* fprintf(stderr, "before free key %d\n", i); */
    free(*(keys+i));
  }
  /* fprintf(stderr, "after free each key\n"); */

#ifdef EXTRA_TEST
  for (int i = 0; i < keys_len; i++)
    printf("parens+%d = %ld\n", i, *(parens+i));
#endif

  destroy_kgp(kgp, 0);

  free(re);
  free(parens);
  free(keys);

  return result;

}

/* NOTE: Supposedly the queries should not be too long, so it should
   be relatively easy to write Emacs Lisp code to obtain the maximal
   depth of the query.  This can greatly simplify things. */
static Query
rten_brel_list_to_query(emacs_env *env, emacs_value user_query, long query_depth)
{
  Query result = QUERY_LEAF(NULL);

  CHECK_TYPE(user_query, "cons", "consp", result);

  emacs_value current = NIL, current_car = NIL;

  /* For storing queries */
  Node_t stack = EMPTY_NODE;
  int stack_len = 0;
  Node_t *stack_pointer = NULL;
  Node_t stack_next = EMPTY_NODE;

  Query current_query = QUERY_LEAF(NULL), temp_query = QUERY_LEAF(NULL);

  Query *query_pointer = NULL;

  emacs_value *value_stack = MYALLOC(emacs_value, query_depth);
  long value_stack_len = 0;

  char *temp_str = NULL;
  long temp_str_len = 0;

  *(value_stack+value_stack_len++) = user_query;

  for (;value_stack_len;) {
    current = *(value_stack+ --value_stack_len);
    
    if (EQ(FUNCALL1(INTERN("listp"), current), INTERN("t"))) {
      /* FUNCALL2(INTERN("message"), STRING("current = %S", 12), current); */
      current_car = CAR(current);
      current = CDR(current);
    } else {
      /* FUNCALL2(INTERN("message"), STRING("current = %S", 12), current); */
      current_car = current;
    }

    /* We push the right branch on the stack first, so that the left
       branch will be encountered first, and hence we can always fill
       in the left branch first. */
    if (EQ(current_car, INTERN("sum"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(sum);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("prod"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(prod);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("minus"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(minus);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("in"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(in);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("ni"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(ni);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("notin"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(notin);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("notni"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(notni);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("follow"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(follow);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("follow_es"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(follow_es);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("follow_ee"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(follow_ee);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("follow_eb"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(follow_eb);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("quote"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(quote);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("quote_es"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(quote_es);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("quote_ee"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(quote_ee);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("quote_eb"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(quote_eb);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("extract"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(extract);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(CDR(current));
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("outer"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(outer);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("inner"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(inner);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (EQ(current_car, INTERN("hull"))) {
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = QUERY_NODE_OP(hull);
      PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      *(value_stack+value_stack_len++) = CAR(current);
    } else if (OFTYPE(current_car, "string")) {
      /* A leaf */

      GET_STRING(current_car, temp_str, temp_str_len);
      temp_query = QUERY_LEAF(temp_str);
      query_pointer = MYALLOC(Query, 1);
      *query_pointer = temp_query;

      if (stack_len) {
        for (;stack_len;) {
          /* We don't want to pop the stack immediately, since the top of
             the stack may be a query that needs two arguments.  */
          current_query = *(stack.value.q);

          int arg_num = query_argument_number(current_query);

          if (arg_num == 2 && current_query.left == NULL) {
            /* If the query accepts two arguments and is not filled in
               yet. */
            (stack.value.q)->left = query_pointer;
            break;
          } else if (arg_num == 2) {
            /* The query accepts two arguments and is filled in one
               argument already. */
            (stack.value.q)->right = query_pointer;
            (stack.value.q)->type = 1;

            if (stack_len == 1) break;

            POP_NODE(stack_len, query_pointer, stack, stack_next, q);
          } else {
            /* The query accepts one argument. */
            (stack.value.q)->left = query_pointer;
            (stack.value.q)->type = 1;

            if (stack_len == 1) break;

            POP_NODE(stack_len, query_pointer, stack, stack_next, q);
          }
        }
      } else {
        PUSH_NODE(stack_len, stack_pointer, stack, query_pointer, q);
      }
    } else {
      /* An error from the user. */
      goto value_stack_ends;
    }
  }

 value_stack_ends:

  free(value_stack);

  if (stack_len) {
    if (stack_len > 1) {
      /* Something is wrong, but we still pretend nothing is wrong. */
      for (int i = 0; i < stack_len; i++) {
        query_pointer = (stack).value.q;
        if ((stack).next) {
          /* This is no longer needed, so we discard it. */
          free(query_pointer);
          stack_next = *((stack).next);
          free((stack).next);
          (stack) = stack_next;
        }
      }
    } else {
      query_pointer = stack.value.q;
    }

    result = *query_pointer;
    free(query_pointer);
  }

  return result;
}

/* The arguments are:

   query, query_depth, re_file_name, index_file_name, additional_spec,
   additional_keys.

   Each of them is required.

   This is not supposed to be called by the user.  In fact, passing
   invalid arguments to this function might lead to crashes of the
   program, so it is recommended to know what one is doing before one
   messes with this function. */

emacs_value
rten_brel_query(emacs_env *env, ptrdiff_t nargs, emacs_value *args,
                void *data)
{

  /* A basic unnecessary sanity check */
  if (nargs != 6) return NIL;

  CHECK_TYPE_NON_NIL(*(args+1), "integer", "integerp", NIL);
  CHECK_TYPE_NON_NIL(*(args+2), "string", "stringp", NIL);
  CHECK_TYPE_NON_NIL(*(args+3), "string", "stringp", NIL);
  CHECK_TYPE_NON_NIL(*(args+4), "string", "stringp", NIL);

  emacs_value result = NIL;

  long query_depth = GET_INT(*(args+1));

  Query user_query = rten_brel_list_to_query(env, *args, query_depth);

  key_group_pairs kgp = emacs_value_to_kgp(env, *(args+5));

  char *re_file_name = NULL, *re_file = NULL;
  ptrdiff_t re_file_len = 0;

  GET_STRING(*(args+2), re_file_name, re_file_len);

  re_file = MYALLOC(char, READ_AMOUNT);

  re_file_len = read_entire_file(re_file_name, &re_file);

  if (!re_file_len) {
    for (re_file_len = 0;*(re_file_name+re_file_len);) re_file_len++;

    emacs_value error_data[] = {
      STRING("Cannot open file: ", 18),
      STRING(re_file_name, re_file_len)
    };

    env->non_local_exit_signal(env, INTERN("error"),
                               LIST(2, error_data));

    free(re_file_name);
    free(re_file);
    destroy_kgp(kgp, 1);

    return NIL;
  }

  char *index_file_name = NULL, *index_file = NULL;
  ptrdiff_t index_file_len = 0;


  GET_STRING(*(args+3), index_file_name, index_file_len);

  index_file = MYALLOC(char, READ_AMOUNT);

  index_file_len = read_entire_file(index_file_name, &index_file);

  if (!index_file_len) {
    for (index_file_len = 0;*(index_file_name+index_file_len);) index_file_len++;

    emacs_value error_data[] = {
      STRING("Cannot open file: ", 18),
      STRING(index_file_name, index_file_len)
    };

    env->non_local_exit_signal(env, INTERN("error"),
                               LIST(2, error_data));

    free(index_file_name);
    free(index_file);
    free(re_file_name);
    free(re_file);
    destroy_kgp(kgp, 1);

    return NIL;
  }

  char *spec = NULL;

  ptrdiff_t spec_len = 0;

  if (!(EQ(*(args+4), NIL))) {
    char *extra_spec = NULL;
    ptrdiff_t extra_spec_len = 0;

    GET_STRING(*(args+4), extra_spec, extra_spec_len);

    spec_len = re_file_len+extra_spec_len+2;
    spec = MYALLOC(char, re_file_len+extra_spec_len+2);

    for (long i = 0; i < re_file_len; i++)
      *(spec+i) = *(re_file+i);

    /* NOTE: Manually add a newline character to prevent anomalies. */

    *(spec+re_file_len) = '\n';

    for (long i = 0; i < extra_spec_len; i++)
      *(spec+re_file_len+i+1) = *(extra_spec+i);

    free(extra_spec);
    free(re_file);

    /* NOTE: The end of the file is not the NULL character, so we add
       a NULL character to conform to the NULL-terminating
       convention. */

    *(spec+re_file_len+extra_spec_len+1) = 0;
  } else {
    spec = MYALLOC(char, re_file_len+1);
    spec_len = re_file_len+1;
    for (long i = 0; i < re_file_len; i++)
      *(spec+i) = *(re_file+i);

    *(spec+re_file_len) = 0;

    free(re_file);
  }

  long keys_len = 0, total_keys_len = 0;

  Node_t parens_node = EMPTY_NODE;
  Node_t parens_next = EMPTY_NODE;

  preprocess_spec(spec, 0, &keys_len, &spec_len, &parens_node);

  long int *parens = MYALLOC(long int, keys_len+1);

  if (keys_len) {
    if (keys_len > 1) {
      for (int i = 0; i < keys_len; i++) {
        *(parens+keys_len-i-1) = (parens_node).value.i;
        if ((parens_node).next) {
          parens_next = *((parens_node).next);
          free((parens_node).next);
          (parens_node) = parens_next;
        }
      }
    } else {
      *(parens) = (parens_node).value.i;
    }
  }

  char **keys = MYALLOC(char*, keys_len+1);
  char *re = spec_to_re(spec, 0, keys, keys_len,
                        spec_len, &total_keys_len);

  if (kgp.key_len)
    merge_keys(&kgp, &keys, &parens, &keys_len);

  /* The result will be of length equal to keys_len. */
  index_result *index_result = index_document
    (index_file, re, parens, keys_len, keys);

  RSet result_RSet = run_query(user_query, index_result, keys_len);

  long *result_array = RSet_to_array(result_RSet);

  emacs_value *result_ev_array = MYALLOC(emacs_value, 2*(result_RSet.size));

  for (long i = 0; i < 2*(result_RSet.size); i++)
    *(result_ev_array+i) = INT(*(result_array+i));

  result = LIST(2*(result_RSet.size), result_ev_array);

  /* Cleaning and returning */

  for (long i = 0; i < keys_len; i++) {
    free(*(keys+i));
    free_index_result(*(index_result+i));
  }

  free(re_file_name);
  free(index_file_name);
  free(index_file);
  free(parens);
  free(keys);
  free(re);
  free(spec);
  destroy_kgp(kgp, 0);
  free(index_result);
  destroy_RSet(result_RSet);
  destroy_query(user_query);
  free(result_array);
  free(result_ev_array);

  return result;
}

int
emacs_module_init(struct emacs_runtime *ert)
{
  emacs_env *env = ert->get_environment(ert);

  unsigned char emacs_version = 0;

  /* The following test works because later Emacs versions always add
   * members to the environment, never remove any members, so the size
   * can only grow with new Emacs releases. Given the version of
   * Emacs, the module can use only the parts of the module API that
   * existed in that version, since those parts are identical in later
   * versions. */

  if (env->size >= sizeof(struct emacs_env_27))
    emacs_version = 27;         /* 27 or later */
  else if (env->size >= sizeof(struct emacs_env_26))
    emacs_version = 26;
  else if (env->size >= sizeof(struct emacs_env_25))
    emacs_version = 25;
  else return 2;
  /* Earlier versions are not supported since they do not support
     dynamic modules. */

  emacs_defun(env, rten_brel_query, "rten-brel-query-inner",
              "Query INDEX_FILE_NAME by QUERY.\n"
"The specification comes from file RE_FILE_NAME and "
"ADDITIONAL_SPEC.\n\n"
"Additionally, ADDITIONAL_KEYS can specify additional keys\n"
"to correspond to regular expression groups.\n\n"
"QUERY_DEPTH is the depth of the QUERY.  A wrong value of \n"
"QUERY_DEPTH might lead to the crash of the program.  So be careful.\n\n"
"\(fn QUERY QUERY_DEPTH RE_FILE_NAME INDEX_FILE_NAME ADDITIONAL_SPEC \
ADDITIONAL_KEYS)",
              6, 6);

  emacs_defun(env, rten_brel_apply_op_to_lists, "rten-brel-op",
              "Apply OP to ARGS.\n"
              "OP should be a string naming a supported operation.\n\n"
              "ARGS should be a list of arguments.\n"
              "Depending on the operation, there might be one or two arguments.\n"
              "Each argument should be a "
              "list of integers of even length.\n"
              "It consists of the beginning and "
              "the ending positions of the regions.\n\n"
              "So for example, the list (1 2 3 4) describes two regions;\n"
              "the first region is the interval (1 2) and the second is (3 4).\n\n"
              "\(fn OP ARGS)",
              2, 2);

  PROVIDE("rten-brel-query");
  return 0;
}
