ifdef MODULE_FILE_SUFFIX
	SUFFIX=$(MODULE_FILE_SUFFIX)
else
	SUFFIX=.so
endif

exe = main
lib = rten-brel$(SUFFIX)
test-exe = test
test-run = testr
test-out = test_out
test-index = testi
test-util = testu
test-region = teste
CC = gcc
TESTCFLAGS = -Wall -DTEST # -DEXTRA_TEST
CCFLAGS =
CPPFLAGS = -Wall -O2
LIBFLAGS = -Wall
sources = compile.c run.c index.c util.c rten-brel-query.c region.c

ifeq ($(SUFFIX), .dylib)
	LIBFLAGS += -dynamiclib
else
	LIBFLAGS += -shared
endif

ifdef EMACS_MODULE_HEADER_DIR
	LIBFLAGS += -I $(EMACS_MODULE_HEADER_DIR)
endif

all: $(lib) TAGS

%.d: %.c
	@echo "Remaking makefiles..."
	@set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$
	@echo "Done"

include $(sources:.c=.d)

$(lib): $(sources:.c=.o) utf8.o
	$(CC) $(LIBFLAGS) $+ -o $@

$(test-region): test-region.o index.o compile.o util.o run.o utf8.o
	$(CC) $(TESTCFLAGS) $+ -o $@

$(exe): $(sources:.c=.o) utf8.o
	$(CC) $(CPPFLAGS) $+ -o $@

$(test-util): test-util.o
	$(CC) $(TESTCFLAGS) $< -o $@

$(test-exe): test-compile.o utf8.o
	$(CC) $+ -o $@

$(test-run): test-run.o compile.o util.o utf8.o
	$(CC) $+ -o $@

$(test-index): test-index.o compile.o run.o util.o utf8.o
	$(CC) $+ -o $@

test-region.o: region.c
	$(CC) $(TESTCFLAGS) -c $< -o $@

test-util.o: util.c
	$(CC) $(TESTCFLAGS) -c $< -o $@

test-index.o: index.c machine.h compile.h utf8.h run.h util.h
	$(CC) $(TESTCFLAGS) -c $< -o $@

test-compile.o: compile.c compile.h machine.h
	$(CC) $(TESTCFLAGS) -c $< -o $@

test-run.o: run.c run.h machine.h
	$(CC) $(TESTCFLAGS) -c $< -o $@

TAGS: *.c *.h
	etags *.c *.h

.PHONY: clean all

clean:
	@echo "Cleaning..."
	-@rm -rf *.o $(lib) $(exe) $(test-exe) \
	  $(test-run) $(test-index) $(test-out) \
          $(test-util) $(test-region) TAGS *.dSYM 2>/dev/null || true
	@echo "Done."
