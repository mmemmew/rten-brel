#ifndef COMPILE_H
#define COMPILE_H

#include "machine.h"
#include "utf8.h"

ProgInfo preprocess_prog (char *regex);

void print_info (ProgInfo info);

Prog compile (char *regex, ProgInfo info);

/* ProgInfo preprocess_prog_s (stream *regex);
 * 
 * Prog compile_s (stream *regex, ProgInfo info); */

void print_prog(Prog p, int depth);

void destroy_prog (Prog p, int length);

void destroy_info (ProgInfo info);

/* int *concat_pointer (int *p1, int *p2, int p1l, int p2l); */

Prog concat_prog (Prog p1, ProgInfo *info1, Prog p2, ProgInfo info2);

Prog postprocess_prog (Prog p, ProgInfo *info);

#endif
