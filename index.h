#ifndef INDEX_H
#define INDEX_H

typedef struct {
  char *key;
  int key_len;
  long int *indices;
  long int indices_len;
} index_result;

typedef struct {
  char ***keys;
  int key_len;
  int *keys_lens;
  int **group;
} key_group_pairs;

void merge_keys(key_group_pairs *kgp, char ***keys,
                long **parens, long *keys_len);

void destroy_kgp(key_group_pairs kgp, unsigned char free_keys_p);

void free_index_result(index_result res);

index_result *index_document(const char *str, const char *re,
                             long int *parens, int parens_length,
                             char **keys);

void index_document_file(const char *str, const char *re, const char *out_name,
                         long int *parens, int parens_length,
                         char **keys, long int total_keys_len);

void preprocess_spec(const char *spec, unsigned char filep,
                     long int *keys_len, long int *spec_len,
                     Node_t *parens);

char *spec_to_re(char *spec, unsigned char filep, char **keys,
                 long int keys_len, long int spec_len,
                 long int *total_keys_len);

#endif
