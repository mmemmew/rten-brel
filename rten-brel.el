;;; rten-brel.el --- Document Indexing Scheme for Emacs   -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Durand

;; Author: Durand <mmemmew@gmail.com>
;; Keywords: c, convenience, data, files, hypermedia, outlines

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a Document Indexing Scheme for Emacs. It first indexes the
;; documents and then queries the indices, and finally presents the
;; results to the user.

;;; Code:

(require 'subr-x)

(defgroup rten-brel ()
  "Document Indexing Scheme for Emacs."
  :group 'outlines
  :version "27.2.50")

(defcustom rten-brel-emacs-module-header-dir
  (let ((module-header (expand-file-name
                        "emacs-module.h"
                        (expand-file-name "src" source-directory))))
    (cond ((file-exists-p module-header) source-directory)))
  "The path to the directory of the Emacs module header file.

Set it to nil if the emacs module header file found in the
standard include path has the same version as the Emacs you want
to use, for example if one wants to use the Emacs that comes with
the operating system. Otherwise one should set this to the name
of a directory containing an \"emacs-module.h\" file that has the
same version as the target Emacs."
  :type 'string
  :group 'rten-brel)

(defconst rten-brel-compile-commands
  '(("gcc" "-Wall" "-O2" "-c" "-o" "compile.o" "compile.c")
    ("gcc" "-Wall" "-O2" "-c" "-o" "run.o" "run.c")
    ("gcc" "-Wall" "-O2" "-c" "-o" "index.o" "index.c")
    ("gcc" "-Wall" "-O2" "-c" "-o" "util.o" "util.c")
    ("gcc" "-Wall" "-O2" "-c" "-o" "region.o" "region.c")
    ("gcc" "-Wall" "-O2" "-c" "-o" "utf8.o" "utf8.c")
    ("gcc" "-Wall" "-O2" "-c" "-o" "rten-brel-query.o" "rten-brel-query.c")
    ("gcc" "-Wall" share include "compile.o" "run.o" "index.o"
     "util.o" "rten-brel-query.o" "region.o" "utf8.o" "-o" "rten-brel%s"))
  "The commands to compile the dynamic module.")

(defvar rten-brel-dir
  (file-name-directory (or load-file-name buffer-file-name))
  "The name of the directory that contains the rten 'brel \
package.")

(defvar rten-brel-module-path
  (expand-file-name
   (concat "rten-brel" module-file-suffix)
   rten-brel-dir)
  "The name of the module file that Emacs should try to load.")

(defun rten-brel-build-env ()
  "Define some variables for the makefile to use."
  (append
   (cond
    (rten-brel-emacs-module-header-dir
     (list
      (format
       "EMACS_MODULE_HEADER_DIR=%s"
       (file-name-as-directory
        (expand-file-name
         rten-brel-emacs-module-header-dir))))))
   (cond
    ((not module-file-suffix)
     (error "Emacs is not compiled with dynamic module support"))
    ((list
      (format
       "MODULE_FILE_SUFFIX=%s"
       module-file-suffix))))))

(defun rten-brel-substitute-manual-command (command-list)
  "Substitute the COMMAND-LIST appropriately.
Assumes the Emacs is compiled with dynamic module support."
  (let (result)
    (mapc (lambda (command)
            (cond
             ((eq command 'share)
              (cond ((string= module-file-suffix ".dylib")
                     (setq result (cons "-dynamiclib" result)))
                    ((setq result (cons "-shared" result)))))
             ((eq command 'include)
              (cond
               ((stringp rten-brel-emacs-module-header-dir)
                (setq result
                      (cons
                       (concat
                        "-I " rten-brel-emacs-module-header-dir)
                       result)))))
             ((stringp command)
              (setq result (cons (format command module-file-suffix) result)))))
          command-list)
    (nreverse result)))

;;;###autoload
(defun rten-brel-compile-load ()
  "Compile and load the dynamic module."
  (interactive)
  (cond
   ((not (file-exists-p rten-brel-module-path))
    (mapc (lambda (command-list)
            (let ((substituted (rten-brel-substitute-manual-command
                                command-list)))
	      (message "Doing %s..."
		       (string-join substituted " "))
              (apply 'call-process
		     (car substituted)
                     nil (get-buffer-create
                          "*compile rten-brel*")
                     nil
                     (cdr substituted))))
          rten-brel-compile-commands)))
  (cond ((file-exists-p rten-brel-module-path)
	 (load-file rten-brel-module-path))))

;; TODO: Implement the user-facing function to query.

(provide 'rten-brel)
;;; rten-rel.el ends here
