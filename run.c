#include "run.h"
#include "util.h"
#include <stdlib.h>
#include <stdio.h>

#define PRINT_CASE(TEXT, OP) case OP:           \
  printf("%s = %s\n", TEXT, #OP);               \
  break

#define POOL_SEES(POOL, ITEM)                   \
  *(POOL->pcs+POOL->ps) = (ITEM);               \
  *(POOL->ipcs+(ITEM)) = POOL->ps;              \
  POOL->ps++

#define PUSH_STACK(X, Y) {                                      \
    stack_length++;                                             \
    if (stack_length > 1) {                                     \
      stack = MYALLOC(Node_t, 1);                               \
      *stack = current;                                         \
      current = (Node_t) { (Node_v) { .Y = (X) }, stack };      \
    } else {                                                    \
      current = (Node_t) { (Node_v) { .Y = (X) }, NULL };       \
    }                                                           \
  }

#define POP_STACK(X, Y) {                       \
    stack_length--;                             \
    X = current.value.Y;                        \
    if (current.next) {                         \
      next = *(current.next);                   \
      free(current.next);                       \
      current = next;                           \
    }                                           \
  }

typedef struct {
  int pc;                       /* program counter */
  int *ml;                      /* match list */
} Thread_t;

// We have two fields for the size/length since a program counter can
// be attempted to be added but not actually added.
typedef struct {
  Thread_t *ts;                 /* threads */
  int tl;                       /* Thread length */
  int *pcs;                     /* Added program counters, a dense array */
  int *ipcs;                    /* The inverse of pcs, a sparse array */
  int ps;                       /* The size of the pool */
  int pl;                       /* The length of the program */
  PoolState state;              /* The state of the pool */
} Pool;

__attribute__((__cold__, __unused__))
static void print_state (PoolState ps)
{
  switch (ps) {
    PRINT_CASE("State", NORMAL);
    PRINT_CASE("State", SUCCESS);
    PRINT_CASE("State", FAILURE);
  }
}

__attribute__((__const__, __always_inline__))
static inline Thread_t thread(int pc, int *ml)
{
  return (Thread_t) { pc, ml };
}

// Set some initial values to a pointer to a Pool. Note that the
// pointer must already be allocated.
static inline void init_pool (Pool *po, ProgInfo info)
{
  // Owner = caller of init_pool
  Thread_t *ts = MYALLOC(Thread_t, info.prog_depth);
  int tl = 0;
  int *pcs = MYALLOC(int, info.prog_depth);
  int *ipcs = MYALLOC(int, info.prog_depth);
  int ps = 0;
  int pl = info.prog_depth;
  PoolState state = NORMAL;
  *po = (Pool) { ts, tl, pcs, ipcs, ps, pl, state };
}

// Clean up the pool.
__attribute__((__always_inline__))
static inline void destroy_pool (Pool *po)
{
  free(po->ts);
  free(po->pcs);
  free(po->ipcs);
  free(po);
}

// Free the pointer in a thread.
__attribute__((__always_inline__))
static inline void free_thread (Thread_t t)
{
  free(t.ml);
}

/* This is a constant time operation. */
__attribute__((__pure__, __always_inline__))
static inline int is_member (int *a_list, int *a_inv_list, int max_len, int item)
{
  int inverse_index = *(a_inv_list+item);
  return (inverse_index >= 0 &&
          inverse_index < max_len &&
          *(a_list+inverse_index) == item);
}

/* Save the index into the specified array. */
__attribute__((__always_inline__))
static inline void save_match(Thread_t t, int paren_num, int index)
{
  /* printf("ml = %p, paren = %d, index = %d\n", t.ml, paren_num, index); */
  *(t.ml+paren_num) = index;
}

/* Match a character C against a class specification CLASS. */
__attribute__((__pure__))
static unsigned char match_class(ARG_t *class, long c)
{
  int temp = 0, result = 0;
  
  for (int j = 0; *(class+3*j) && !result; j++) {
    temp = *(class+3*j) < -1*MAX_CLASS-1;

    switch (*(class+3*j) % (MAX_CLASS+1)) {
    case SINGLE:
      if ((temp) ? (c == *(class+3*j+1)) :
          (c != *(class+3*j+1)))
        break;
      result |= 1;
      break;
    case RANGE:
      if ((temp) ?
          (c >= *(class+3*j+1) || c <= *(class+3*j+2)) :
          (c < *(class+3*j+1) || c > *(class+3*j+2)))
        break;
      result |= 1;
      break;
    default:              /* non-reachable */
      fprintf(stderr, "match_class: ");
      fprintf(stderr, "Why am I here?\n");
      fprintf(stderr, "j = %d\n", j);
      fprintf(stderr, "class = %ld\n", *(class+3*j));
      fprintf(stderr, "Modulus = %ld\n", (*(class+3*j) % (MAX_CLASS+1)));
      result |= 2;
      break;
    }
  }
  
  return result;
}

/* Execute a mini program (mp) and return the result as a boolean.
   Since C has no dedicated boolean type, we use the smallest possible
   type to represent that boolean value. */
__attribute__((__pure__))
static unsigned char execute_assert(char *s, ARG_t *mp, unsigned char startp)
{
  unsigned char result = 0;

  long temp = 0, temp_one = 0;

  Node_t current = (Node_t) { (Node_v) { .i = 0 }, NULL };
  Node_t next = (Node_t) { (Node_v) { .i = 0 } , NULL };
  Node_t *stack = NULL;
  int stack_length = 0;
  
  for (;*mp;mp++) {
    switch (*mp) {
    case AND:
      POP_STACK(temp, i);
      POP_STACK(temp_one, i);
      result = temp && temp_one;
      break;
    case OR:
      POP_STACK(temp, i);
      POP_STACK(temp_one, i);
      result = temp || temp_one;
      break;
    case NOT:
      POP_STACK(result, i);
      result = !result;
      break;
    case NEXT:
      mp++;
      temp = 0;
      for (int i = 0; i < *mp;i++) temp += utf8_len(*(s+temp));
      PUSH_STACK(*(s+temp), i);
      break;
    case BEFORE_CHAR:
      /* temp = utf8_len(*s); */
      if (!(*(s))) {
        PUSH_STACK(0, i);
      } else {
        temp = *(s);
        temp_one = *++mp;
        PUSH_STACK(temp == temp_one, i);
      }
      break;
    case BEFORE_CLASS:
      /* temp = utf8_len(*s); */
      if (!(*(s))) {
        PUSH_STACK(0, i);
      } else {
        PUSH_STACK(match_class
                   (++mp, to_cp(s,utf8_len(*(s))))
                   == 1, i);
      }
      if (!(*(s))) mp++;
      for (; *mp;) mp+=3;
      break;
    case AFTER_CHAR:
      if (startp) {
        PUSH_STACK(0, i);
      } else {
        temp = utf8_len_back(s-1);
        temp = to_cp(s-temp, temp);
        temp_one = *mp++;
        PUSH_STACK(temp == temp_one, i);
      }
      break;
    case AFTER_CLASS:
      if (startp) {
        PUSH_STACK(0, i);
      } else {
        temp = utf8_len_back(s-1);
        temp = to_cp(s-temp, temp);
        PUSH_STACK(match_class(++mp, temp) == 1, i);
      }
      if (!(*(s-1))) mp++;
      for (; *mp;) mp+=3;
      break;
    case BETWEEN_CHAR:
      if (startp || !(*(s+utf8_len(*s)))) {
        PUSH_STACK(0, i);
      } else {
        temp = utf8_len_back(s-1);
        temp = to_cp(s-temp, temp);
        temp_one = *mp++;
        temp = temp == temp_one;
        temp_one = utf8_len(*s);
        temp_one =
          to_cp(s+temp_one, utf8_len(*(s+temp_one))) == *mp++;
        PUSH_STACK(temp && temp_one, i);
      }
      break;
    case BETWEEN_CLASS:
      if (startp || !(*(s+utf8_len(*s)))) {
        PUSH_STACK(0, i);
      } else {
        temp = utf8_len_back(s-1);
        temp = match_class(++mp, to_cp(s-temp, temp));
        for (; *mp;) mp+=3;
        temp_one = match_class(++mp, *(s+utf8_len(*s)));
        PUSH_STACK(temp == 1 && temp_one == 1, i);
        for (; *mp;) mp+=3;
      }
      break;
    }
  }

#ifdef EXTRA_TEST
  printf("result = %d\n", (int) result);
  printf("length = %d\n", (int) stack_length);
  printf("char = %c\n", *s);
#endif
  
  return result;
}

// Add the thread T to the pool PO. Threads with program counters that
// are already in the pool won't be added again. Also this won't add
// thread with program counters that point to an instruction which is
// a JUMP or SPLIT. Instead the function will just record that this
// program counter is seen, and proceeds to add other program counters
// in a depth-first manner: this makes sure that the order of the
// threads respects the priority of threads.
//
// Also note that the depth-first traversal is performed by means of a
// stack rather than by use of recursion. This might help prevent some
// potential stack overflow.
__attribute__((__hot__))
static void add_thread(Pool *po, Thread_t t, Prog p, int ml_len,
                       int s_index, char *s, int char_index)
{
  if (is_member(po->pcs, po->ipcs, po->ps, t.pc)) { // is already added
    // Don't forget to free this poor thread
    free_thread(t);
    return;
  }

  // Owner = add_thread
  Thread_t *stack = MYALLOC(Thread_t, po->pl);
  int stack_length = 1;
  *stack = t;

  // Some silly initial values that don't mean anything.
  int pc = 0;
  Inst_t inst = { CHAR, NULL };
  OPCODE_t op = CHAR;
  ARG_t *args = NULL;

  int added_times = 0;

  /* Current thread */
  Thread_t c = (Thread_t) { CHAR, NULL };
  Thread_t temp = (Thread_t) { CHAR, NULL };

  for (;;) {
    c = *(stack+(--stack_length));
    pc = c.pc;

    /* Check if PC is greater than the program length */
    if (pc >= po->pl) {
      /* If so this is a match. */
      *(po->ts+(po->tl++)) = c;
      POOL_SEES(po, pc);

      if (stack_length == 0) break;
      continue;
    }

    inst = **(p+pc);
    op = inst.op;
    args = inst.args;
    switch (op) {
    case JUMP:
      POOL_SEES(po, pc);
      if (!is_member(po->pcs, po->ipcs, po->ps, *args)) {
        c.pc = *args;
        *(stack+(stack_length++)) = c;
      } else {
        free_thread(c);
      }
      break;
    case SPLIT:
      POOL_SEES(po, pc);
      added_times = 0;
      if (!is_member(po->pcs, po->ipcs, po->ps, *(args+1))) {
        c.pc = *(args+1);
        *(stack+(stack_length++)) = c;
        added_times++;
      } /* Don't free yet as we might want to use this thread in the
           next block. */

      if (!is_member(po->pcs, po->ipcs, po->ps, *args)) {
        if (added_times) {
          temp.pc = *args;
          temp.ml = MYALLOC(int, ml_len);
          for (int i = 0; i < ml_len; i++)
            *(temp.ml+i) = *(c.ml+i);

          *(stack+(stack_length++)) = temp;
        } else {
          c.pc = *args;
          *(stack+(stack_length++)) = c;
        }
      } else if (!added_times) {
        free_thread(c);
      }
      break;
    case ASSERT:
      if (!is_member(po->pcs, po->ipcs, po->ps, pc)) {
        POOL_SEES(po, pc);
        /* int back_len = (!s_index) ? 0 : utf8_len_back(s+s_index-1); */
        
        if (execute_assert(s+s_index, args, !(s_index))) {
          c.pc++;
          *(stack+(stack_length++)) = c;
        } else {
          free_thread(c);
        }
      }
      break;      
    case SAVE:
      if (!is_member(po->pcs, po->ipcs, po->ps, pc)) {
#ifdef EXTRA_TEST
        printf("PC = %d, saving %d\n", pc,
               (*args%2) ?
               ((char_index) ? char_index : 0) :
               ((char_index) ? char_index+1 : 0));
#endif
        POOL_SEES(po, pc);
        if (*args == 0 || *args % 2 ||
            *(c.ml+(*args)+1) == -1 ||
            *(c.ml+(*args)+1) != char_index-1) {
          save_match(c, *args,
                     (*args%2) ?
                     ((char_index) ? char_index : 0) :
                     ((char_index) ? char_index+1 : 0));
        }
        c.pc++;
        *(stack+(stack_length++)) = c;
      } else {
        free_thread(c);
      }
      break;
    default:
      /* printf("pc = %d\n", pc);
       * printf("Is it a member? %d\n", is_member(po->pcs, po->ipcs, po->ps, pc)); */
      /* if (!is_member(po->pcs, po->ipcs, po->ps, pc+1))
       *   *(stack+(stack_length++)) = pc+1; */

      if (!is_member(po->pcs, po->ipcs, po->ps, pc)) {
        POOL_SEES(po, pc);
        *(po->ts+(po->tl++)) = c;
      } else {
        free_thread(c);
      }
      break;
    }

    if (stack_length == 0)
      break;
  }

  free(stack);
}

/* TODO: Implement ASSERT a construct */

// We use two pools in running the program. One pool stores the
// threads under execution, and the other pool stores the threads that
// will be executed in the next iteration.

// At each iteration we will determine whether to spawn a new thread,
// or to continue the thread, or to discard the thread. In the first
// two cases, nothing needs to be cleaned. But in the last case, the
// thread that will be discarded needs to be freed right away,
// otherwise it will be leaked, since we won't even be able to access
// that thread afterwards.
int run_prog (Prog p, ProgInfo info, char *s, int *matches)
{
  int result = FAILURE;

  long result_index = -1;

  // tpool is only a temporary pool not allocated.
  Pool *cpool, *npool, *tpool = NULL;
  // Owner = run_prog
  cpool = MYALLOC(Pool, 1);
  npool = MYALLOC(Pool, 1);
  init_pool(cpool, info);
  init_pool(npool, info);

  int ml_len = 2*(info.parens+1);
  int *init_ml = MYALLOC(int, ml_len);
  for (int i = 0; i < ml_len; i++)
    *(init_ml+i) = -1;

  add_thread(cpool, thread(0, init_ml), p, ml_len, 0, s, 0);

  int s_index = 0, char_index = 0;
  long c = 0;
  int increment = 0;
  Inst_t inst = { CHAR, NULL };
  OPCODE_t op = CHAR;
  ARG_t *args = NULL;
  int next_thread = 1;

  int temp = 0;

#ifdef EXTRA_TEST
  char utf8_temp[5], utf8_temp_one[5];
#endif
  
  for (; *(s+s_index); s_index+=increment, char_index++) {
    increment = utf8_len(*(s+s_index));
    c = to_cp(s+s_index, increment);
    if (cpool->tl == 0)
      break;
#ifdef EXTRA_TEST
    to_utf8(c, utf8_temp);
    printf("char = %s\n", utf8_temp);
#endif
    for (int i = 0; i < cpool->tl;) {
      next_thread = 1;
#ifdef EXTRA_TEST
      printf("tl = %d\n", cpool->tl);
      printf("i = %d, pc = %d\n", i, ((cpool->ts)+i)->pc);
#endif
      if (((cpool->ts)+i)->pc < 0 ||
          ((cpool->ts)+i)->pc >= info.prog_depth) {
        /* This thread runs out of programs, so is a match. */
        for (int j = 0; j < ml_len; j++)
          *(matches+j) = *((cpool->ts+i)->ml+j);
#ifdef EXTRA_TEST
        printf("success, s_index = %d\n", s_index);
#endif
        result = SUCCESS;
        result_index = s_index;
        for (; i < cpool->tl; i++) {
          free_thread(*(cpool->ts+i));
        }
        cpool->ps = 0;
        cpool->tl = 0;
        continue;
      }

      inst = **(p + ((cpool->ts)+i)->pc);
      op = inst.op;
      args = inst.args;
#ifdef EXTRA_TEST
      switch (op) {
        PRINT_CASE("OP", CHAR);
        PRINT_CASE("OP", CLASS);
        PRINT_CASE("OP", ASSERT);
        PRINT_CASE("OP", JUMP);
        PRINT_CASE("OP", SPLIT);
        PRINT_CASE("OP", SAVE);
        PRINT_CASE("OP", MATCH);
        PRINT_CASE("OP", FAIL);
        PRINT_CASE("OP", ANY);
      }
      if (op == CHAR) {
        to_utf8(c, utf8_temp);
        to_utf8(*args, utf8_temp_one);
        printf("looking at %s, matching with %s\n",
               utf8_temp, utf8_temp_one);
      }
#endif

      switch (op) {
      case CHAR:
        if (*args != c) {
          // Match fails. Clear the thread.
          free_thread(*((cpool->ts)+i));
          break;
        }
        // Match succeeds. Continue the thread.
        ((cpool->ts)+i)->pc++;
        add_thread(npool, *((cpool->ts)+i), p, ml_len, s_index+increment, s,
                   char_index+1);
        break;
      case ANY:
        if (c == '\n') {
          // Match fails. Clear the thread.
          free_thread(*((cpool->ts)+i));
          break;
        }
        // Match succeeds. Continue the thread.
        ((cpool->ts)+i)->pc++;
        add_thread(npool, *((cpool->ts)+i), p, ml_len, s_index+increment, s,
                   char_index+1);
        break;
      case CLASS:
        temp = match_class(args, c);
        
        if (temp == 1) {
          ((cpool->ts)+i)->pc++;
          add_thread(npool, *((cpool->ts)+i), p, ml_len, s_index+increment, s,
                     char_index+1);
        } else {
          free_thread(*((cpool->ts)+i));
        }
        break;
      case JUMP:
      case SPLIT:
        /* These three don't consume characters, and are hence handled
           in the same way. */
        add_thread(cpool, *((cpool->ts)+i), p, ml_len, s_index, s,
                   char_index);
        break;
      case ASSERT:
        POOL_SEES(cpool, (cpool->ts+i)->pc);
        /* int back_len = (!s_index) ? 0 : utf8_len_back(s+s_index-1); */
        if (execute_assert(s+s_index, (**(p+(cpool->ts+i)->pc)).args, !(s_index))) {
          (cpool->ts+i)->pc++;
          next_thread = 0;
        } else {
          free_thread(*(cpool->ts+i));
        }
        break;
      case SAVE:
        if (*args == 0 || *args % 2 ||
            *((cpool->ts+i)->ml+(*args)+1) == -1 ||
            *((cpool->ts+i)->ml+(*args)+1) != char_index-1) {
          save_match(*((cpool->ts)+i), *args,
                     (*args%2) ?
                     ((char_index) ? char_index : 0) :
                     ((char_index) ? char_index+1 : 0));
        }
        POOL_SEES(cpool, (cpool->ts+i)->pc);
        (cpool->ts+i)->pc++;
        next_thread = 0;
        break;
      case MATCH:
        for (int j = 0; j < ml_len; j++)
          *(matches+j) = *((cpool->ts+i)->ml+j);
        result = SUCCESS;
#ifdef EXTRA_TEST
        printf("success, s_index = %d\n", s_index);
#endif
        result_index = s_index;
        for (; i < cpool->tl; i++) {
          free_thread(*(cpool->ts+i));
        }
        cpool->ps = 0;
        cpool->tl = 0;
        break;
      case FAIL:
        free_thread(*(cpool->ts+i));
        break;
      }
      if (next_thread) i++;
    }
    tpool = cpool;
    cpool = npool;
    npool = tpool;
    npool->ps = 0;
    npool->tl = 0;
  }

  /* After the end of the string we shall process the remaining pool
     one more time. But the index s_index should be offset by 1 as if
     we were still processing the last character. */

#ifdef EXTRA_TEST
  if (cpool->tl)
    printf("Last round\n");
#endif
  for (int i = 0; i < cpool->tl;) {
    next_thread = 1;
#ifdef EXTRA_TEST
    printf("tl = %d\n", cpool->tl);
    printf("i = %d, pc = %d\n", i, ((cpool->ts)+i)->pc);
    printf("ml = ");
    for (int j = 0; j < ml_len; j++)
      printf("%s%d", (j) ? ", " : " ", *((cpool->ts+i)->ml+j));
    printf("\n");
#endif
    if (((cpool->ts)+i)->pc < 0 ||
        ((cpool->ts)+i)->pc >= info.prog_depth) {
      /* This thread runs out of programs, so is a match. */
      for (int j = 0; j < ml_len; j++)
        *(matches+j) = *((cpool->ts+i)->ml+j);
      result = SUCCESS;
#ifdef EXTRA_TEST
      printf("success, s_index = %d\n", s_index);
#endif
      result_index = s_index;
      for (; i < cpool->tl; i++) {
        free_thread(*(cpool->ts+i));
      }
      cpool->ps = 0;
      cpool->tl = 0;
      continue;
    }
    inst = **(p + ((cpool->ts)+i)->pc);
    op = inst.op;
    args = inst.args;
#ifdef EXTRA_TEST
    switch (op) {
      PRINT_CASE("OP", CHAR);
      PRINT_CASE("OP", CLASS);
      PRINT_CASE("OP", ASSERT);
      PRINT_CASE("OP", JUMP);
      PRINT_CASE("OP", SPLIT);
      PRINT_CASE("OP", SAVE);
      PRINT_CASE("OP", MATCH);
      PRINT_CASE("OP", FAIL);
      PRINT_CASE("OP", ANY);
    }
#endif

    switch (op) {
    case ANY:
    case CHAR:
    case CLASS:
    case ASSERT:
    case FAIL:
      // In the last loop if these occur then this is a fail.
      free_thread(*((cpool->ts)+i));
      break;
    case SAVE:
      if (*args == 0 || *args % 2 ||
          *((cpool->ts+i)->ml+(*args)+1) == -1 ||
          *((cpool->ts+i)->ml+(*args)+1) != char_index-1) {
        save_match(*((cpool->ts)+i), *args,
                   (*args%2) ?
                   ((char_index) ? char_index : 0) :
                   ((char_index) ? char_index+1 : 0));
      }
      ((cpool->ts)+i)->pc++;
      next_thread = 0;
      break;
    case MATCH:
      for (int j = 0; j < ml_len; j++)
        *(matches+j) = *((cpool->ts+i)->ml+j);
      result = SUCCESS;
#ifdef EXTRA_TEST
      printf("success, s_index = %d\n", s_index);
#endif
      result_index = s_index;
      for (i++; i < cpool->tl; i++) {
        free_thread(*(cpool->ts+i));
      }
      cpool->tl = 0;
      cpool->ps = 0;
      break;
    case JUMP:
      add_thread(cpool, *((cpool->ts)+i), p, ml_len,
                 s_index-utf8_len_back(s+s_index-1), s,
                 char_index-1);
      break;
    case SPLIT:
      add_thread(cpool, *((cpool->ts)+i), p, ml_len,
                 s_index-utf8_len_back(s+s_index-1), s,
                 char_index-1);
      break;
    }
    if (next_thread) i++;
  }

  destroy_pool(cpool);
  destroy_pool(npool);
  return (result==SUCCESS) ? result_index : -1;
}

#ifdef TEST
#include "compile.h"
#include <time.h>

void push_prog_inst_1 (Prog p, int offset, OPCODE_t op, ARG_t arg);

void push_prog_inst_2 (Prog p, int offset, OPCODE_t op,
                       ARG_t arg1, ARG_t arg2);

int main(int argc, char **argv)
{
  char *re = "(haha)|(wankzn\n(hahaha)\njnfskd)|(yo)|(試試看)", *str = "abc試試看";
  /* char *pre_re = "[a-z]?", *str = MYALLOC(char, 60);
   * char *re = MYALLOC(char, 660);
   * for (int i = 0; i < 60; i++)
   *   *(str+i) = 'a'+(i%26);
   * for (int i = 0; i < 60; i++) {
   *   *(re+6*i) = *pre_re;
   *   *(re+6*i+1) = *(pre_re+1);
   *   *(re+6*i+2) = *(pre_re+2);
   *   *(re+6*i+3) = *(pre_re+3);
   *   *(re+6*i+4) = *(pre_re+4);
   *   *(re+6*i+5) = *(pre_re+5);
   *   *(re+6*60+5*i) = *pre_re;
   *   *(re+6*60+5*i+1) = *(pre_re+1);
   *   *(re+6*60+5*i+2) = *(pre_re+2);
   *   *(re+6*60+5*i+3) = *(pre_re+3);
   *   *(re+6*60+5*i+4) = *(pre_re+4);
   * } */

  struct timespec start, end;
  long secs = 0, nano = 0;

  clock_gettime(CLOCK_MONOTONIC_RAW, &start);

  ProgInfo info = preprocess_prog(re);
  /* print_info(info); */
  Prog p = compile(re, info);

  p = postprocess_prog(p, &info);

  /* Prog p2 = MYALLOC(Inst_t*,4);
   * ProgInfo info2 = (ProgInfo) { 0, 4, 0, 0, 0, NULL, 0, NULL };
   * 
   * push_prog_inst_2(p2, 0, SPLIT, 1, 2);
   * push_prog_inst_1(p2, 1, ANY, 0);
   * push_prog_inst_2(p2, 2, SPLIT, 3, 4);
   * *(p2+3) = MYALLOC(Inst_t, 1);
   * **(p2+3) = (Inst_t) { CLASS, MYALLOC(ARG_t, 7) };
   * *((**(p2+3)).args) = SINGLE;
   * *((**(p2+3)).args + 1) = 10;
   * *((**(p2+3)).args + 2) = 0;
   * *((**(p2+3)).args + 3) = SINGLE;
   * *((**(p2+3)).args + 4) = 13;
   * *((**(p2+3)).args + 5) = 0;
   * *((**(p2+3)).args + 6) = 0;  
   * 
   * p = concat_prog(p2, &info2, p, info); */

  clock_gettime(CLOCK_MONOTONIC_RAW, &end);

  secs = end.tv_sec - start.tv_sec;
  nano = end.tv_nsec - start.tv_nsec;

  for (; nano < 0; nano += 1000000000, secs--) {}

  printf("RE = %s\nSTR = %s\n", re, str);

  printf("It took %ld seconds and %ld nanoseconds to compile\n",
         secs, nano);

  if (!p) {
    fprintf(stderr, "Cannot compile the regular expression.\n");
    /* free(re); */
    /* free(str); */
    return 0;
  }

  /* printf("RE = %s\n", re); */
  /* printf("STR = ");
   * for (int i = 0; i < 5; i++) printf("%c", *(str+i));
   * printf(" 1000x%c ", *(str+5));
   * printf("%c\n", *(str+1005)); */
  /* printf("STR = %s\n", str); */
  
#ifdef EXTRA_TEST
  printf("Program is as follows.\n");
  print_prog(p, info.prog_depth);
#endif
  
  // Owner = main
  int *match_list = MYALLOC(int, 2*(info.parens+1));
  for (int i = 0; i < 2*(info.parens+1);i++)
    *(match_list+i) = -1;

  clock_gettime(CLOCK_MONOTONIC_RAW, &start);

  int match = run_prog(p, info, str, match_list);

  clock_gettime(CLOCK_MONOTONIC_RAW, &end);

  print_state(match);

  if (match+1) {
    printf("match = %d\n", match);
    printf("The match list:");
    for (int i = 0; i < 2*(info.parens+1); i++) {
      printf("%s%d", (i) ? ", " : " ",
             *(match_list+i));
    }
    printf("\n");
  }

  secs = end.tv_sec - start.tv_sec;
  nano = end.tv_nsec - start.tv_nsec;

  for (; nano < 0; nano += 1000000000, secs--) {}

  printf("It took %ld seconds and %ld nanoseconds to run\n",
         secs, nano);


  free(match_list);
  /* free(re); */
  /* free(str); */
  destroy_prog(p, info.prog_depth);
  destroy_info(info);
  return 0;
}
#endif
