#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "index.h"

int main (int argc, char **argv)
{
  long int keys_len = 0, spec_len = 0, total_keys_len;

  Node_t parens_node = EMPTY_NODE;
  Node_t parens_next = EMPTY_NODE;

  char *test_file = "test_file", *test_in_file = "test_in",
    *test_out = "test_out";
  
  if (argc > 1)
    test_file = *(argv+1);
  if (argc > 2)
    test_in_file = *(argv+2);
  if (argc > 3)
    test_out = *(argv+3);
  if (argc > 4) {
    fprintf(stderr, "Too many arguments. The arguments should have the form\n\
$(exe) [regular expression file name] [indexing document name] \
[output file name].\n");
    exit(1);
  }
    
  preprocess_spec(test_file, 1, &keys_len, &spec_len, &parens_node);

  long int *parens = MYALLOC(long int, keys_len+1);

#ifdef EXTRA_TEST
  printf("keys_len = %ld\n", keys_len);
  for (int i = 0; i < keys_len; i++) {
    printf("parens + %d = %ld\n", i, *(parens+i));
  }
#endif
  
   if (keys_len) {
     if (keys_len > 1) {
       for (int i = 0; i < keys_len; i++) {
         *(parens+keys_len-i-1) = (parens_node).value.i;
         if ((parens_node).next) {
           parens_next = *((parens_node).next);
           free((parens_node).next);
           (parens_node) = parens_next;
         }
       }
     } else {
       *(parens) = (parens_node).value.i;
     }
   }

  char **keys = MYALLOC(char*, keys_len+1);
  char *test = spec_to_re(test_file, 1, keys, keys_len,
                          spec_len, &total_keys_len);
  
  printf("The regular expression = %s\n", test);

  char *test_in = MYALLOC(char, READ_AMOUNT);
  
  read_entire_file(test_in_file, &test_in);

  printf("The document to index = %s\n", test_in);

  /* ProgInfo info = preprocess_prog(test);
   * Prog p = compile(test, info);
   * 
   * p = postprocess_prog(p, &info); */

  index_document_file(test_in, test, test_out,
                      parens, keys_len,
                      keys, total_keys_len);

  /* printf("Program is as follows.\n");
   * print_prog(p, info.prog_depth); */
  /* printf("test = ");
   * for (int i = 0; *(test+i); i++) {
   *   printf("%d ", *(test+i));
   * }
   * printf("\n"); */

  free(test);
  free(test_in);

#ifdef EXTRA_TEST
  for (int i = 0; i < keys_len; i++)
    printf("keys+%d = %s\n", i, *(keys+i));
#endif

  for (int i = 0; *(keys+i); i++)
    free(*(keys+i));

#ifdef EXTRA_TEST
  for (int i = 0; i < keys_len; i++)
    printf("parens+%d = %ld\n", i, *(parens+i));
#endif
  
  free(parens);
  free(keys);

  return 0;
}
