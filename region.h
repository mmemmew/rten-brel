#ifndef REGION_H
#define REGION_H

#include "index.h"

#define QUERY_LEAF(X) ((Query) { (Query_u) { .leaf = X }, 0, NULL, NULL })

#define QUERY_NODE(OP, LEFT, RIGHT) ((Query) { (Query_u) { .op = OP }, 1, LEFT, RIGHT })

#define QUERY_NODE_OP(OP) ((Query) { (Query_u) { .op = OP }, 2, NULL, NULL })

struct Region_s;

typedef struct Region_s Region;

typedef struct RSet_s {
  Region *rs;                   /* array of regions in start order */
  long size;                    /* The length of the array */
  long *stoe;                   /* The permutation that transforms the
                                   start position order to the end
                                   position order */
  long *etos;                   /* The inverse permutation of stoe */
} RSet;

/* Functions to work with region sets. */

long *RSet_to_array(RSet rs);

RSet array_to_RSet(long *array, long size, unsigned char order);

void print_RSet(RSet rs);

void destroy_RSet(RSet rs);

/* Set operations */
RSet RSet_union(RSet a, RSet b);
RSet RSet_intersection(RSet a, RSet b);
RSet RSet_difference(RSet a, RSet b);

/* Endomorphisms */
RSet RSet_outer(RSet rs);
RSet RSet_inner(RSet rs);
RSet RSet_hull(RSet rs);

/* Containement related */

RSet RSet_in(RSet a, RSet b);
RSet RSet_ni(RSet a, RSet b);
RSet RSet_notin(RSet a, RSet b);
RSet RSet_notni(RSet a, RSet b);

/* Followed by operator */

RSet RSet_follow(RSet a, RSet b);
RSet RSet_follow_es(RSet a, RSet b);
RSet RSet_follow_ee(RSet a, RSet b);
RSet RSet_follow_eb(RSet a, RSet b);

/* Quotes */

RSet RSet_quote(RSet a, RSet b);
RSet RSet_quote_es(RSet a, RSet b);
RSet RSet_quote_ee(RSet a, RSet b);
RSet RSet_quote_eb(RSet a, RSet b);

/* TODO: Implement variants of follow and quote excluding the start
   and the end positions respectively.*/

/* Extraction */

RSet RSet_extract(RSet a, RSet b);

/* Query data type */

typedef enum {
  sum,
  prod,
  minus,
  outer,
  inner,
  hull,
  in,
  ni,
  notin,
  notni,
  follow,
  follow_es,
  follow_ee,
  follow_eb,
  quote,
  quote_es,
  quote_ee,
  quote_eb,
  extract,
} QOP;

typedef union {
  QOP op;
  char *leaf;
} Query_u;

/* The query is a binary tree. */

typedef struct Query_s {
  Query_u value;                /* either a leaf or a node. */
  unsigned char type;           /* 0 means leaf, 1 means node, and 2
                                   means no branches: these are
                                   arbitrary choices. */
  struct Query_s *left;         /* If not used these will be NULL
                                   pointers. */
  struct Query_s *right;
} Query;

/* A helper function. */

int query_argument_number(const Query q);

/* Run queries on a tree. */

RSet run_query(const Query q, const index_result *source, const long len);

/* Free the pointers in a query. */

void destroy_query(Query q);

#endif
