;;; query.el --- Querying Lists with no mutual containement  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: extensions, matching, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is inspired by, but different from the implementation proposed
;; in, the paper "An algebra for structured text search and a
;; framework for its implementation" by Clarke, Cormack, and
;; Burkowski. In fact, the implementations of the algebraic operations
;; are adapted from the paper "Nested Text Region Algebras" by Jaakkola
;; and Kilpeläinen.

;; This is for building a "query algebra" that queries lists of
;; "intervals".

;; An interval is a list of integers A and B, such that A is less than
;; or equal to B.

;; To be precise, the functions that really query the intervals are
;; not implemented in this file. Instead they are implemented as C
;; functions in the file region.c. And they are called through dynamic
;; module functions in Emacs. Then the task of this file is to
;; translate the queries from the users to a form that is easy to
;; execute.

;;; Code:

;; TODO: Parse and translate a query to a tree of algebraic
;; operations.

(provide 'query)
;;; query.el ends here
