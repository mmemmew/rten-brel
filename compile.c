#include "compile.h"
#include "util.h"
#include <stdlib.h>             // For malloc
#include <stdio.h>              // For reporting errors and testing

/* Specific macros that should not be useful either for other parts of
   the project or for the users of this project. */

#define SET_OP_CASE(CASE, LEN)                  \
  case CASE:                                    \
  op = #CASE;                                   \
  length = LEN;                                 \
  break

#define PRINT_CASE_EXTRA(OP, EXTRA) case OP:    \
  printf("%s", #OP);                            \
  EXTRA                                         \
  break

#define REPLACE_IN_SECOND_LOOP(X, Y)            \
  if ((X) % 2 == 0) {                           \
    (X) = pc + (X)/2;                           \
  } else if ((X) < 0 && (X) % 8 == -3) {        \
    temp = *(saved_pc_stack-((X)+3)/8);         \
    (X) = *(pc_stack-((X)+3)/8);                \
    if (temp/2)                                 \
      *((**(p+temp/2)).args + (temp % 2)) = pc; \
  } else if ((X) < 0 && (X) % 8 == -1) {        \
    *(saved_pc_stack-((X)+1)/8) = 2*pc+Y;       \
  }

#define REPLACE_IN_THIRD_LOOP(X)                \
  if ((X) < 0 && (X) % 8 == -7) {               \
    temp = ((X) + 7) / 8;                       \
    *(last_stack-temp) = pc;                    \
    (X) = *(pc_stack-temp);                     \
  } else if ((X) < 0 && (X) % 8 == -5) {        \
    temp = ((X) + 5) / 8;                       \
    (X) = *(last_stack-temp) + 1;               \
    *(last_stack-temp) = pc;                    \
  }

#define EQUAL_MODULO_MAX(X, Y)                  \
  ((X) % (MAX_CLASS+1) == (Y) % (MAX_CLASS+1))

#define INTERSECT(X, Y, A, B)                   \
  (((X) >= (A) && (X) <= (B)) ||                \
   ((Y) >= (A) && (Y) <= (B)))

/* Parse a character class.

   Returns the ending pointer. */
__attribute__((__pure__))
static inline char *parse_class_length (char *s)
{
  /* A closed bracket right after the open bracket means a closed
     bracket. No empty character classes are allowed. */

  /* A caret right after the open bracket will escape the following
     closed bracket as well. */

  /* Open brackets are always escaped. */

  if (*s == ']') s++;
  else if (*s == '^' && *(s+1) == ']') s += 2;

  /* Then a closed bracket terminates the character class. */

  for (;*s;s++) if (*s == ']') return s;

  return s-1;
}

/* Parse an escaping construction.

   Return the terminating pointer. */
__attribute__((__pure__))
static inline char *parse_escape(char *s)
{
  if (*s == 'a') {
    if (*(s+1) == '{') for (;*s != '}';s++) {}
  }

  return s;
}

/* REVIEW: Record the number of character-consuming instructions as
   well. */

ProgInfo preprocess_prog(char *re)
{
  ProgInfo info = { 0, 0, 0, 0, 0, NULL, 0, NULL };
  int prog_length = 2, re_depth = 0, max_re_depth = 0;
  int parens = 0, length = 0;
  char *temp = NULL;
  long c = 0;
  int increment = 0;

  int num_class = 0;
  Node_t current = EMPTY_NODE;
  Node_t next = EMPTY_NODE;
  Node_t *node_pointer = NULL;

  int num_escape = 0;
  Node_t ecurrent = EMPTY_NODE;
  Node_t enext = EMPTY_NODE;
  Node_t *enode_pointer = NULL;

  for (;*re;re+=increment, length+=increment) {
    increment = utf8_len(*re);
    c = to_cp(re, increment);

    switch (c) {
    case '\\':
      temp = parse_escape(re+1);
      length += temp - re;
      prog_length++;
      PUSH_NODE(num_escape, enode_pointer, ecurrent, length, i);
      re = temp;
      break;
    case '[':
      temp = parse_class_length(re+1);
      length += temp - re;
      prog_length++;
      PUSH_NODE(num_class, node_pointer, current, 1+temp-re, i);
      re = temp;
      break;
    case '*':
    case '|':
      prog_length += 2;
      break;
    case '(':
      re_depth++;
      parens++;
      if (re_depth > max_re_depth)
        max_re_depth = re_depth;
      prog_length++;
      break;
    case ')':
      re_depth--;
    default:
      prog_length++;
      break;
    };
  }

  /* Now loop over the nodes to collect them back into a simple
     array. */

  info.cl = MYALLOC(int, num_class);

  COPY_NODE(num_class, current, info.cl, next, i);

  info.es = MYALLOC(int, num_escape);

  COPY_NODE(num_escape, ecurrent, info.es, enext, i);

  info.length = length;
  info.prog_depth = prog_length;
  info.re_depth = max_re_depth;
  info.parens = parens;
  info.num_class = num_class;
  info.num_escape = num_escape;
  return info;
}

void destroy_info (ProgInfo info)
{
  if (info.cl) free(info.cl);
  if (info.es) free(info.es);
}

__attribute__((__cold__, __unused__))
void print_info (ProgInfo info)
{
  printf("Program length: %d\n", info.prog_depth);
  printf("Regular expression length: %d\n", info.length);
  printf("Regular expression depth: %d\n", info.re_depth);
  printf("Number of pairs of parentheses: %d\n", info.parens);
  printf("Number of character classes: %d\n", info.num_class);
  printf("Character classes information:");
  for (int i = 0; i < info.num_class; i++) {
    printf("%s%d", (i) ? ", " : " ", *(info.cl+i));
  }
  printf("\n");
  printf("Number of escape constructs: %d\n", info.num_escape);
  printf("Escaping consturcts information:");
  for (int i = 0; i < info.num_escape; i++) {
    printf("%s%d", (i) ? ", " : " ", *(info.es+i));
  }
  printf("\n");
}

__attribute__((__cold__, __unused__))
void print_prog(Prog p, int depth)
{
  Inst_t instruction;
  char *op = NULL;
  char *default_format = "%ld ";
  char *char_format = "%s ";
  char *format = default_format;
  ARG_t *args = NULL;
  ARG_t *atemp = NULL;
  int line = 0;
  int length = 0;

  char utf8_temp[5];

  int j = 0;

  for (;line < depth;line++) {
    format = default_format;
    instruction = **(p+line);
    switch (instruction.op) {
      // CHAR is special
    case CHAR:
      op = "CHAR";
      length = 1;
      format = char_format;
      break;
      SET_OP_CASE(CLASS, -1);
      SET_OP_CASE(ASSERT, -2);
      SET_OP_CASE(JUMP, 1);
      SET_OP_CASE(SPLIT, 2);
      SET_OP_CASE(SAVE, 1);
      SET_OP_CASE(MATCH, 0);
      SET_OP_CASE(FAIL, 0);
      SET_OP_CASE(ANY, 1);
    default:
      break;                    // Unreachable
    }
    args = instruction.args;
    printf("%d: %s, ", line, op);
    if (length == -1) {
      for (int i = 0; *(args+3*i) && i < 2; i++) {
        printf("%s", (i) ? ", " : "");
        printf(format, *(args+3*i));
        printf(format, *(args+3*i+1));
        printf(format, *(args+3*i+2));
      }
      printf("\n");
    } else if (length == -2) {
      for (int i = 0; *(args+i); i++) {
        switch (*(args+i)) {
          PRINT_CASE_EXTRA(AND,);
          PRINT_CASE_EXTRA(OR,);
          PRINT_CASE_EXTRA(NOT,);
          PRINT_CASE_EXTRA(NEXT,);
          PRINT_CASE_EXTRA(BEFORE_CHAR,);
          PRINT_CASE_EXTRA(AFTER_CHAR,);
          PRINT_CASE_EXTRA
            (BEFORE_CLASS,
             j=0;
             i++;
             for (; *(args+i+3*j); j++) {
               printf(" ");
               printf(format, *(args+i+3*j));
               printf(format, *(args+i+3*j+1));
               printf(format, *(args+i+3*j+2));
             };
             i+=3*j;);
          PRINT_CASE_EXTRA
            (AFTER_CLASS,
             j = 0;
             i++;
             for (; *(args+i+3*j); j++) {
               printf(" ");
               printf(format, *(args+i+3*j));
               printf(format, *(args+i+3*j+1));
               printf(format, *(args+i+3*j+2));
             };
             i+=3*j;);
          PRINT_CASE_EXTRA(BETWEEN_CHAR,);
          PRINT_CASE_EXTRA
            (BETWEEN_CLASS,
             j = 0;
             i++;
             for (; *(args+i+3*j); j++) {
               printf(" ");
               printf(format, *(args+i+3*j));
               printf(format, *(args+i+3*j+1));
               printf(format, *(args+i+3*j+2));
             };
             i+=3*j;);
        }
      }
      printf("\n");
    } else if (length > 0) {
      for (int i = 0; i < length; i++) {
        atemp = (ARG_t*) (args+i);
        if (*(format+1) == 's') {
          switch (*atemp) {
          case '\n':
            printf("%s ", "\\n");
            break;
          case ' ':
            printf("SPC ");
            break;
          case '\f':
            printf("%s ", "\\f");
            break;
          case '\t':
            printf("%s ", "\\t");
            break;
          default:
            to_utf8((unsigned int)*(args+i), utf8_temp);
            printf(format, utf8_temp);
            break;
          }
        } else {
          printf(format, *(args+i));
        }
      }
      printf("\n");
    } else {
      printf("\n");
    }
  }
}

__attribute__((__cold__))
void destroy_prog(Prog p, int length)
{
  for (int i = 0;i < length; i++) {
    free((**(p+i)).args);
    free(*(p+i));
  }
  free(p);
}

/* Push an instruction with one argument. */
static inline void push_prog_inst_1 (Prog p, int offset, OPCODE_t op, ARG_t arg)
{
  *(p+offset) = MYALLOC(Inst_t, 1);
  **(p+offset) = (Inst_t) { op, MYALLOC(ARG_t, 1) };
  *((**(p+offset)).args) = arg;
}

/* Push an instruction with two arguments. */
static inline void push_prog_inst_2 (Prog p, int offset, OPCODE_t op,
                      ARG_t arg1, ARG_t arg2)
{
  *(p+offset) = MYALLOC(Inst_t, 1);
  **(p+offset) = (Inst_t) { op, MYALLOC(ARG_t, 2) };
  *((**(p+offset)).args) = arg1;
  *((**(p+offset)).args + 1) = arg2;
}

/* Compile the arguments of a class. */
static int compile_class_args(ARG_t *args, char *re, int length)
{
  /* Owner = compile_class_args */
  ARG_t *pre_args = MYALLOC(ARG_t, 3*length-5);
  int j = 0;                    /* as a counter */
  long c = 0;
  char negated = 0;
  ARG_t increment = 0, temp = 0;

  for (int i = 1; i < length-1; i+=increment, j++) {
    increment = utf8_len_back(re-i);
    c = to_cp(re-i-increment+1, increment);
    switch (c) {
    case '^':
      if (i == length-2) {
        negated = 1;
        break;
      }
    default:
      /* The last two characters are directly processed */
      /* The order of elements in pre_args is reversed, but it does not
         matter. */
      if (length - i > 3 && *(re-i-increment) == '-') {
        /* A character range */
        temp = utf8_len_back(re-i-increment-1);
        *(pre_args+3*j) = RANGE;
        *(pre_args+3*j+1) = (ARG_t) to_cp(re-i-increment-temp, temp);
        *(pre_args+3*j+2) = (ARG_t) c;
        i += 2;
      } else {
        *(pre_args+3*j) = SINGLE;
        *(pre_args+3*j+1) = (ARG_t) c;
        *(pre_args+3*j+2) = 0;
      }
      break;
    }
  }

  *(pre_args+3*j) = 0;              /* 0 means stop. */

  j = 0;                        /* reuse the counter */
  int temp_one = 0;
  temp = 0;                     /* reuse the counter */

  for (int i = 0; *(pre_args+3*i); i++) {
    /* subtract MAX_CLASS+1 means negated */
    if (negated) *(pre_args+3*i) -= MAX_CLASS+1;

    if (!j) {
      *(args+3*j) = *(pre_args+3*i);
      *(args+3*j+1) = *(pre_args+3*i+1);
      *(args+3*j+2) = *(pre_args+3*i+2);
      j++;
      continue;
    }

    if (EQUAL_MODULO_MAX(*(pre_args+3*i), SINGLE)) {
      if (EQUAL_MODULO_MAX(*(args+3*j-3), SINGLE)) {
        temp = *(args+3*j-2);
        if (ABS(*(pre_args+3*i+1) - temp) == 1) {
          *(args+3*j-3) += RANGE-SINGLE;
          *(args+3*j-2) = MIN(*(pre_args+3*i+1), temp);
          *(args+3*j-1) = MAX(*(pre_args+3*i+1), temp);
        } else if (*(pre_args+3*i+1) == temp) {
        } else {
          *(args+3*j) = *(pre_args+3*i);
          *(args+3*j+1) = *(pre_args+3*i+1);
          *(args+3*j+2) = 0;
          j++;
        }
      } else if (EQUAL_MODULO_MAX(*(args+3*j-3), RANGE)) {
        temp = *(args+3*j-2);
        temp_one = *(args+3*j-1);

        if (*(pre_args+3*i+1) == temp-1) {
          (*(args+3*j-2))--;
        } else if (*(pre_args+3*i+1) == temp_one+1) {
          (*(args+3*j-1))++;
        } else if (*(pre_args+3*i+1) >= temp &&
                   *(pre_args+3*i+1) <= temp_one) {
        } else {
          *(args+3*j) = *(pre_args+3*i);
          *(args+3*j+1) = *(pre_args+3*i+1);
          *(args+3*j+2) = 0;
          j++;
        }
      } else {
        *(args+3*j) = *(pre_args+3*i);
        *(args+3*j+1) = *(pre_args+3*i+1);
        *(args+3*j+2) = 0;
        j++;
      }
    } else if (EQUAL_MODULO_MAX(*(pre_args+3*i), RANGE)) {
      if (EQUAL_MODULO_MAX(*(args+3*j-3), SINGLE)) {
        temp = *(args+3*j-2);

        if (*(pre_args+3*i+1) == temp+1) {
          *(args+3*j-3) = *(pre_args+3*i);
          *(args+3*j-1) = *(pre_args+3*i+2);
        } else if (*(pre_args+3*i+2) == temp-1) {
          *(args+3*j-3) = *(pre_args+3*i);
          *(args+3*j-2) = *(pre_args+3*i+1);
          *(args+3*j-1) = temp;
        } else if (temp >= *(pre_args+3*i+1) &&
                   temp <= *(pre_args+3*i+2)) {
          *(args+3*j-3) = *(pre_args+3*i);
          *(args+3*j-2) = *(pre_args+3*i+1);
          *(args+3*j-1) = *(pre_args+3*i+2);
        } else {
          *(args+3*j) = *(pre_args+3*i);
          *(args+3*j+1) = *(pre_args+3*i+1);
          *(args+3*j+2) = 0;
          j++;
        }
      } else if (EQUAL_MODULO_MAX(*(args+3*j-3), RANGE)) {
        temp = *(args+3*j-2);
        temp_one = *(args+3*j-1);

        if (INTERSECT(temp, temp_one,
                      *(pre_args+3*i+1), *(pre_args+3*i+2))) {
          *(args+3*j-2) = MIN(temp, *(pre_args+3*i+1));
          *(args+3*j-1) = MAX(temp_one, *(pre_args+3*i+2));
        } else {
          *(args+3*j) = *(pre_args+3*i);
          *(args+3*j+1) = *(pre_args+3*i+1);
          *(args+3*j+2) = *(pre_args+3*i+2);
          j++;
        }
      } else {
        *(args+3*j) = *(pre_args+3*i);
        *(args+3*j+1) = *(pre_args+3*i+1);
        *(args+3*j+2) = *(pre_args+3*i+2);
        j++;
      }
    } else {
      *(args+3*j) = *(pre_args+3*i);
      *(args+3*j+1) = *(pre_args+3*i+1);
      *(args+3*j+2) = *(pre_args+3*i+2);
      j++;
    }
  }

  *(args+3*j) = 0;

  free(pre_args);
  return 3*j+1;
}

/* Push the instruction for the class that ends at the pointer RE. */
static void compile_class (Prog p, int offset, char *re, int length)
{
  if (!(*re)) return;

  /* char c = 0;
   * char negated = 0;             /\* used as a boolean *\/ */
  /* Only length - 2 items are there. Each item contributes 3 slots
     and one slot is reserved to mark the end. */
  if (length <= 2) return;  /* No empty classes are allowed. */
  /* Owner = p */
  ARG_t *final_args = MYALLOC(ARG_t, 3*length-5);

  compile_class_args(final_args, re, length);

  /* Now push to p. */
  *(p+offset) = MYALLOC(Inst_t, 1);
  **(p+offset) = (Inst_t) { CLASS, final_args };
}

/* We first loop from the end of the regular expression. This step
   will determine every instruction and put them in the correct
   position, but their arguments are wrong. */
static Prog compile_1 (char *regex, ProgInfo info)
{
  int i = info.length - 1;
  int pc = info.prog_depth - 1;
  // Owner = caller of compile_1
  Prog p = MYALLOC(Inst_t*, info.prog_depth);
  long c = 0, next = 0;
  unsigned char immediate = 0;

  int classes_counter = 0;

  int escape_counter = 0;
  int a_construct_counter = 0;

  int j = 0, temp = 0, increment;

  int error = 0;

  // stack needs at least one spot.
  int stack_max = info.re_depth+1;
  // Owner = compile_1
  int *stack = MYALLOC(int, stack_max);

  for (int k = 0; k < stack_max; k++)
    *(stack+k) = 0;

  int stack_depth = 0;

  push_prog_inst_1(p, pc, SAVE, 1);
  pc--;

  for (; i >= 0; i-=increment) {
    increment = utf8_len_back(regex+i);
    c = to_cp(regex+i-increment+1, increment);
    temp = utf8_len_back(regex+i-increment);
    next = (i) ? to_cp(regex+i-increment-temp, temp) : 0;
    /* We process escaping constructs specially. */

    if (info.num_escape > 0 &&
        escape_counter < info.num_escape &&
        i == *(info.es+info.num_escape-1-escape_counter)) {
      escape_counter++;
      if (c == '}') {

        /* To test for usual ASCII we don't have to use longs. */
        for (; (c=*(regex+i-j)) != '{'; j++)
          if (c == ',') a_construct_counter++;
        a_construct_counter++;

        /* We can use next here since it won't be used until the next
           iteration, when it will be reset. */

        next = i - j;

        *(p+pc) = MYALLOC(Inst_t, 1);
        **(p+pc) = (Inst_t) { ASSERT, MYALLOC(ARG_t, a_construct_counter) };

        a_construct_counter = 0; /* reuse the counter */
        j--;
        for (; j>0; j--) {
          switch (*(regex+i-j)) {
          case 'A':
          case 'a':
            switch (*(regex+i-j+1)) {
            case 'C':
            case 'c':
              *((**(p+pc)).args+a_construct_counter++) = AFTER_CHAR;
              for (; *(regex+i-j) != ',';) j--;
              temp = utf8_len(*(regex+i-j+1));
              *((**(p+pc)).args+a_construct_counter++)
                = to_cp(regex+i-j+1, temp);
              j -= temp+1;
              break;
            case 'L':
            case 'l':
              *((**(p+pc)).args+a_construct_counter++) = AFTER_CLASS;
              for (; *(regex+i-j) != ',';) j--;
              j--;
              temp = 1+parse_class_length(regex+i-j) - regex-i+j;
              a_construct_counter +=
                compile_class_args((**(p+pc)).args+a_construct_counter,
                                   regex+i-j, temp);
              j -= temp-1;
              break;
            case 'n':
            case 'N':
              break;
            default:
              break;
            }
            break;
          case 'B':
          case 'b':
            break;
          case 't':
          case 'T':
            break;
          case 'o':
          case 'O':
            break;
          case 'n':
          case 'N':
            break;
          default:
            break;
          }
        }

        /* *((**(p+pc)).args) = arg1;
         * *((**(p+pc)).args + 1) = arg2; */
      } else {
        switch (c) {
        case 'n':
          c = '\n';
          break;
        case 't':
          c = '\t';
          break;
        case 'f':
          c = '\f';
          break;
        default:
          break;
        }
        push_prog_inst_1(p, pc, CHAR, (ARG_t) c);
        pc--;
        i--;
        if (immediate) {
          push_prog_inst_2(p, pc, SPLIT, 2, 4);
          pc--;
          immediate = 0;
        }
      }
      continue;
    }

    // 1 means star
    // 2 means question
    // 3 means or
    switch (c) {
    case '^':
      *(p+pc) = MYALLOC(Inst_t, 1);
      **(p+pc) = (Inst_t) { ASSERT, MYALLOC(ARG_t, 7) };
      *((**(p+pc)).args) = AFTER_CLASS;
      *((**(p+pc)).args+1) = -1-MAX_CLASS+SINGLE;
      *((**(p+pc)).args+2) = '\n';
      *((**(p+pc)).args+3) = 0;
      *((**(p+pc)).args+4) = 0;
      *((**(p+pc)).args+5) = NOT;
      *((**(p+pc)).args+6) = 0;
      pc--;
      break;
    case '$':
      *(p+pc) = MYALLOC(Inst_t, 1);
      **(p+pc) = (Inst_t) { ASSERT, MYALLOC(ARG_t, 7) };
      *((**(p+pc)).args) = BEFORE_CLASS;
      *((**(p+pc)).args+1) = -1-MAX_CLASS+SINGLE;
      *((**(p+pc)).args+2) = '\n';
      *((**(p+pc)).args+3) = 0;
      *((**(p+pc)).args+4) = 0;
      *((**(p+pc)).args+5) = NOT;
      *((**(p+pc)).args+6) = 0;
      pc--;
      break;
    case ']':
      compile_class(p, pc, regex+i,
                    *(info.cl+info.num_class-1-classes_counter));
      pc--;
      classes_counter++;
      i -= *(info.cl+info.num_class-classes_counter)-1;
      if (immediate) {
        push_prog_inst_2(p, pc, SPLIT, 2, 4);
        pc--;
        immediate = 0;
      }
      break;
    case ')':
      push_prog_inst_1(p, pc, SAVE, 3);
      pc--;
      stack_depth++;
      break;
    case '(':
      if (*(stack+stack_depth) & 0b100) {
        /* or */
        *(stack+stack_depth) -= 0b100;
        push_prog_inst_2(p, pc, SPLIT,
                         2, -(8*stack_depth+5));
        stack_depth--;
        pc--;
      } else {
        stack_depth--;
      }

      push_prog_inst_1(p, pc, SAVE, 2);
      pc--;

      if (*(stack+stack_depth) & 0b10) {
        *(stack+stack_depth) -= 0b10;
        /* question */
        /* The same as star, except that there is no special
           instruction at the question position. */
        push_prog_inst_2(p, pc, SPLIT,
                         2, -(8*stack_depth+1));
        pc--;
      } else if (*(stack+stack_depth) & 1) {
        /* star */
        *(stack+stack_depth) -= 1;
        push_prog_inst_2(p, pc, SPLIT,
                         2, -(8*stack_depth+1));
        pc--;
      }
      break;
    case '*':
      if (next == ')') {
        push_prog_inst_2(p, pc, SPLIT,
                         -(8*stack_depth+3),
                         2);
        pc--;
        *(stack+stack_depth) |= 1; /* 1 means star */
      } else if (next) {
        push_prog_inst_2(p, pc, SPLIT, -2, 2);
        pc--;
        immediate = 1;
      } else {
        /* Invalid regular expression */
        error = 1;
      }
      break;
    case '?':
      if (next == ')') {
        *(stack+stack_depth) |= 0b10; /* 2 means question */
      } else if (next) {
        immediate = 1;
      } else {
        /* Invalid regular expression */
        error = 1;
      }
      break;
    case '+':
      if (next == ')') {
        push_prog_inst_2(p, pc, SPLIT,
                         -(8*stack_depth+3),
                         2);
        pc--;
      } else if (next) {
        push_prog_inst_2(p, pc, SPLIT, -2, 2);
        pc--;
      } else {
        error = 1;
      }
      break;
    case '|':
      if (*(stack+stack_depth) & 0b100) {
        /* previous or */
        push_prog_inst_2(p, pc, SPLIT,
                         2, -(8*stack_depth+5));
        pc--;
      }
      push_prog_inst_1(p, pc, JUMP, -1*(8*stack_depth+7));
      pc--;
      *(stack+stack_depth) |= 0b100; /* 3 means or */
      break;
    case '.':
      push_prog_inst_1(p, pc, ANY, 0);
      pc--;
      if (immediate) {
        push_prog_inst_2(p, pc, SPLIT, 2, 4);
        pc--;
        immediate = 0;
      }
      break;
    default:
      push_prog_inst_1(p, pc, CHAR, (ARG_t) c);
      pc--;
      if (immediate) {
        push_prog_inst_2(p, pc, SPLIT, 2, 4);
        pc--;
        immediate = 0;
      }
      break;
    }
    if (pc < -1) break;
  }

  if (*stack & 0b100) {
    /* or */
    *stack -= 0b100;
    push_prog_inst_2(p, pc, SPLIT, 2, -5);
    pc--;
  }

  push_prog_inst_1(p, pc, SAVE, 0);
  pc--;

  if (pc != -1 || error) {
    fprintf(stderr, "Wrong program!\n");
    fprintf(stderr, "PC: %d\n", pc);
    fprintf(stderr, "regex = %s\n", regex);
    free(stack);

    if (pc >= -1)
      print_prog(p+pc+1, info.prog_depth - pc - 1);
    else if (pc >= -2)
      print_prog(p+pc+2, info.prog_depth-pc-2);

    for (int i = 0;i < info.prog_depth - pc - 1; i++) {
      if (pc+1+i>=0) {
        free((**(p+pc+1+i)).args);
      }
        free(*(p+pc+1+i));
    }
    free(p);
    return NULL;
  }

  free(stack);
  return p;
}

/* Then we loop from the beginning.

   This step will determine every relative address. These are the
   addresses that are even, whether positive or negative. And after
   this step they will become absolute addresses, and hence positive,
   whether even or odd.

   Furthermore, the addresses that are negative and congruent to -3
   modulo 8 will be replaced with the addresses of the address of the
   open parenthesis of depth one higher than -1 * (X + 3) / 8, i.e. of
   depth (5 - X) / 8. The addresses that are negative and congruent to
   -1, -5, and -7 will be resolved in the next step. And in this
   process the negative addresses that are congruent to -1 modulo 8
   will also be fixed. This will be explained in the next paragraph.

   When an address with argument that is negative and congruent to -1
   modulo 8 is found, its program counter will be recorded. And an
   offset that is 0 or 1 will be recorded as well. That offset is the
   Y in the macro. Then when an address with argument that is negative
   and congruent to -3 is found, as in the previous paragraph, it will
   fetch the recorded program counter, and set its argument (with an
   offset) to its program counter. In this way the -1 modulo 8 and the
   -3 modulo 8 arguments form pairs.

   A special case is when there is no corresponding -3 address to pair
   with the -1 address. This can occur with the ? operation. To pair
   with such a -1 address, we also fetch and set program counters for
   closing parentheses.

   In addition, the numbering of parentheses will be determined as
   well. */
static void compile_2 (Prog p, ProgInfo info)
{
  int pc = 0;
  Inst_t inst;
  OPCODE_t op = 0;
  ARG_t *args = NULL;

  int current_paren = 0;
  int paren_depth = 0;
  // Owner = compile_2
  int *paren_stack = MYALLOC(int, info.re_depth);
  int *pc_stack = MYALLOC(int, info.re_depth);
  int *saved_pc_stack = MYALLOC(int, info.re_depth);

  for (int i = 0; i < info.re_depth; i++) {
    *(paren_stack+i) = 0;
    *(pc_stack+i) = 0;
    *(saved_pc_stack+i) = 0;
  }

  // Only used for replacement
  int temp = 0;

  for (; pc < info.prog_depth; pc++) {
    inst = **(p+pc);
    op = inst.op;

    switch (op) {
    case JUMP:
      args = inst.args;
      REPLACE_IN_SECOND_LOOP(*args, 0);
      break;
    case SPLIT:
      args = inst.args;
      REPLACE_IN_SECOND_LOOP(*args, 0);
      REPLACE_IN_SECOND_LOOP(*(args+1), 1);
      break;
    case SAVE:
      if (*(inst.args) == 2) {
        // Open paren
        *(inst.args) = 2*(++current_paren);
        *(pc_stack+paren_depth) = pc;
        *(paren_stack+(paren_depth++)) = current_paren;
      } else if (*(inst.args) == 3) {
        // Closing paren
        *(inst.args) = 2*(*(paren_stack+(--paren_depth))) + 1;
        temp = *(saved_pc_stack+paren_depth);
        /* *(saved_pc_stack+paren_depth) = 0; */
        if (temp)
          *((**(p+temp/2)).args + (temp % 2)) = pc + 1;
      }
    default:
      break;
    }
  }
  free(paren_stack);
  free(pc_stack);
  free(saved_pc_stack);
}

/* In the last loop we loop from the end again, and will resolve the
   addresses that are negative and congruent to -5 or -7 modulo 8.

   Those -7 addresses, say X, will be replaced by the address of the
   closing parenthesis of depth -1 * (X + 7) / 8.

   And those -5 addressesn say X, will be replaced by one plus the
   previous -5 address or the previous -7 address of the same depth,
   whichever came last. */

static void compile_3(Prog p, ProgInfo info)
{
  int pc = info.prog_depth - 1;
  Inst_t inst;
  OPCODE_t op = 0;
  ARG_t *args = NULL;

  int paren_depth = 0;
  // Owner = compile_3
  int *last_stack = MYALLOC(int, info.re_depth);
  int *pc_stack = MYALLOC(int, info.re_depth);

  int temp = 0;

  for (int i = 0; i < info.re_depth; i++) {
    *(last_stack+i) = 0;
    *(pc_stack+i) = 0;
  }

  for (; pc >= 0; pc--) {
    inst = **(p+pc);
    op = inst.op;

    switch (op) {
    case JUMP:
      args = inst.args;
      REPLACE_IN_THIRD_LOOP(*args);
      break;
    case SPLIT:
      args = inst.args;
      REPLACE_IN_THIRD_LOOP(*args);
      REPLACE_IN_THIRD_LOOP(*(args+1));
      break;
    case SAVE:
      if (*(inst.args) % 2 == 1) {
        // Closing paren
        *(pc_stack+paren_depth++) = pc;
      } else {
        // Open paren
        paren_depth--;
      }
    default:
      break;
    }
  }
  free(last_stack);
  free(pc_stack);
}

/* Put the three loops together. */
Prog compile (char *regex, ProgInfo info)
{
  
  long l = 0;

  for (;*(regex+l);) l += utf8_len(*(regex+l));

  if (l > info.length) {
    fprintf(stderr, "wrong regex!:\nre = %s\n", regex);
    return NULL;
  }

  // Owner = caller of compile
  Prog p = compile_1(regex, info);
  
  if (p) compile_2(p, info);
  if (p) compile_3(p, info);

  return p;
}

/* Merge two pointers. */
__attribute__((__always_inline__))
static inline int *concat_pointer (int *p1, int *p2, int p1l, int p2l)
{
  if (!p2l) {
    free(p2);
    return p1;
  }

  if (!p1l) {
    free(p1);
    return p2;
  }

  for (int i = 0; i < p2l; i++)
    *(p1+p1l+i) = *(p2+i);

  free(p2);

  return p1;
}

/* Merge two programs together and adjust the jumping, splitting, and
   saving instructions automatically.

   The resulting merged program will be P1, and INFO1 will be adjusted
   and returned accordingly. And P2 and INFO2 will be deleted
   automatically as well. */
Prog concat_prog (Prog p1, ProgInfo *info1, Prog p2, ProgInfo info2)
{
  int pc_offset = info1->prog_depth, save_offset = info1->parens;

  Prog p = MYALLOC(Inst_t*, info1->prog_depth+info2.prog_depth);
  for (int pc = 0; pc < pc_offset; pc++) {
    *(p+pc) = *(p1+pc);
  }

  for (int pc = 0; pc < info2.prog_depth; pc++)
    *(p+pc+pc_offset) = MYALLOC(Inst_t, 1);

  info1->prog_depth += info2.prog_depth;
  info1->re_depth = MAX(info1->re_depth, info2.re_depth);
  info1->parens += info2.parens;

  info1->cl = concat_pointer(info1->cl, info2.cl,
                             info1->num_class,
                             info2.num_class);

  info1->num_class += info2.num_class;
  info1->es = concat_pointer(info1->es, info2.es,
                             info1->num_escape,
                             info2.num_escape);
  info1->num_escape += info2.num_escape;

  info2.cl = info2.es = NULL;

  Inst_t current = (Inst_t) { CHAR, NULL };
  OPCODE_t op = CHAR;

  for (int pc = 0; pc < info2.prog_depth; pc++) {
    current = **(p2+pc);
    op = current.op;

    switch (op) {
    case JUMP:
      *(current.args) += pc_offset;
      break;
    case SPLIT:
      *(current.args) += pc_offset;
      *(current.args+1) += pc_offset;
      break;
    case SAVE:
      *(current.args) += save_offset;
      break;
    default:
      break;
    }
    **(p+pc_offset+pc) = current;
    free(*(p2+pc));
  }

  free(p2);

  destroy_info(info2);

  free(p1);

  return p;
}

/* Merge with a small program to ignore some characters of the string
   before a match is found. */
Prog postprocess_prog (Prog p, ProgInfo *info)
{
  Prog p2 = MYALLOC(Inst_t*,6);
  ProgInfo info2 = (ProgInfo) { 0, 6, 0, 0, 0, NULL, 0, NULL };

  push_prog_inst_2(p2, 0, SPLIT, 3, 1);
  push_prog_inst_1(p2, 1, ANY, 0);
  push_prog_inst_1(p2, 2, JUMP, 5);
  push_prog_inst_2(p2, 3, SPLIT, 5, 4);
  *(p2+4) = MYALLOC(Inst_t, 1);
  **(p2+4) = (Inst_t) { CLASS, MYALLOC(ARG_t, 7) };
  *((**(p2+4)).args) = SINGLE;
  *((**(p2+4)).args + 1) = 10;
  *((**(p2+4)).args + 2) = 0;
  *((**(p2+4)).args + 3) = SINGLE;
  *((**(p2+4)).args + 4) = 13;
  *((**(p2+4)).args + 5) = 0;
  *((**(p2+4)).args + 6) = 0;
  push_prog_inst_2(p2, 5, SPLIT, 6, 0);

  p = concat_prog(p2, &info2, p, *info);

  *info = info2;

  return p;
}

#ifdef TEST
#include <time.h>

__attribute__((__cold__, __unused__))
static char *repeat_string (char *str, int length, int count)
{
  // Owner = caller of repeat_string
  char *result = MYALLOC(char, length * count);
  for (int i = 0; i < count; i++)
    for (int j = 0; j < length; j++)
      *(result+i*length+j) = *(str+j);
  return result;
}

__attribute__((__cold__, __unused__, __always_inline__))
static inline int string_length(char *s)
{
  int i = 0; for (; *(s+i); i++) {} return i;
}

int main (int argc, char **argv)
{
  char *pre_re = (argc>1) ? *(argv+1) :
    "D*urand\n|(Awl+|ow|er|)*|李俊緯";
  // Owner = main
  char *re = NULL;

  /* ProgInfo info = preprocess_prog(pre_re);
   * printf("re = %s\n", pre_re);
   * printf("the length is %d\n", info.length);
   * printf("program length = %d\n", info.prog_depth);
   *
   * return 0; */

  if (argc > 2) {
    re = repeat_string(pre_re, string_length(pre_re),
                       atoi(*(argv+2)));
    printf("The regular expression is %d times of \"%s\"\n",
           atoi(*(argv+2)),
           pre_re);
  } else {
    re = repeat_string(pre_re, string_length(pre_re), 1);
    printf("The regular expression is \"%s\"\n", re);
  }

  struct timespec start, end;

  clock_gettime(CLOCK_MONOTONIC_RAW, &start);

  ProgInfo info = preprocess_prog(re);

  print_info(info);

  /* free(re);
   * destroy_info(info);
   * return 1; */

  Prog p = compile(re, info);

  clock_gettime(CLOCK_MONOTONIC_RAW, &end);

  if (p) {
    /* Don't print when the program is too long. */
    if (info.prog_depth <= 100) {
      p = postprocess_prog(p, &info);
      print_prog(p, info.prog_depth);
    } else
      printf("The program has %d instructions and is too long to print here.\n",
             info.prog_depth);
    destroy_prog(p, info.prog_depth);
    printf("It took %ld seconds and %ld nanoseconds\n",
           end.tv_sec - start.tv_sec,
           end.tv_nsec - start.tv_nsec);
  } else {
    fprintf(stderr, "The base regular expression is \"%s\"\n", pre_re);
    fprintf(stderr, "If the regular expression is surely correct, \n");
    fprintf(stderr, "then this error is because some features are not\n");
    fprintf(stderr, "supported yet.\n");
    fprintf(stderr, "Please wait until they are supported.\n");
  }

  destroy_info(info);

  free(re);
  return 0;
}

#endif
